﻿using MEHK.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.OrganizationManagement.Models
{
    public class OrganizationChart
    {

        public OrganizationChart(Unit CurrentUnit)
        {
            this._CurrentUnit = CurrentUnit;
            //    this._Children = Children;
            GetChildren();
        }
        public int CurrentUnitId { get { return CurrentUnit.UnitId; } }
        public Unit CurrentUnit { get { return _CurrentUnit; }  }
        public List<OrganizationChart> Children  { get; set; }


        private Unit _CurrentUnit;
        private List<OrganizationChart> _Children { get; set; }

        public bool UpdateCurrentUnit(Unit updatedUnit)
        {
            if (_CurrentUnit.UnitId != updatedUnit.UnitId)
                return false;
            _CurrentUnit = updatedUnit;
            return true;
        }
        public void GetChildren()
        {
            this._Children = new List<OrganizationChart>();
            var _ChildrenNodes = new Entities().Units.Where(x => x.ParentUnitId == _CurrentUnit.UnitId);
            foreach (var node in _ChildrenNodes)
            {
                OrganizationChart Child = new OrganizationChart(node);
                _Children.Add(Child);
            }
            return; 
        }
        //public bool UpdateChildrenUnit(Unit updatedUnit)
        //{
        //    foreach(var )
        //}

    }
}