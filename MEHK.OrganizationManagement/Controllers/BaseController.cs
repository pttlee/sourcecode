﻿using MEHK.BLL;
using MEHK.DAL.DTO;
using MEHK.DAL.Model;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
//using MEHK.Workflow.Data.Model;
//using K25Lib;
//using MEHK.Workflow.Data.Enum;

namespace MEHK.OrganizationManagement.Controllers
{
    public class BaseController : Controller
    {

        //private string K2DomainName = "";
        //private string K2Server = "";
        //private string k2UserID = "";
        //private string k2Password = "";
        //private string loginDomain = "";
        //private string k2WindowsDomain = "";
        string ControllerName = "";
        protected string ProcessName = "";

        public BaseController()
        {
           
            // this.ProcessName = CommonBLL.GetSystemParameter(AppKey.GenenalProcessName);
        }
        public int GetCurrentUserID
        {
            get
            {
                //OrganizationBLL OrganizationBLL = new OrganizationBLL();
                int UserID = 0;

                var userinfo = new OrganizationBLL().GetMemberByLoginName(User.Identity.Name);

                if (userinfo != null)
                {
                    UserID = userinfo.MemberId;
                }

                return UserID;
            }
        }
        public string CommonAPIBase
        {
            get {
                return System.Configuration.ConfigurationManager.AppSettings["CommonApiUrl"];
            }
        }
        public string OrganizationAPIBase
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["OrganizationApiUrl"];
            }
        }


        protected void SetBreadcrumbs(params string[] breadcrumbs)
        {
            List<string> listBreadcrumbs = new List<string>();

            listBreadcrumbs = breadcrumbs.ToList();

            ViewData["listBreadcrumbs"] = listBreadcrumbs;
        }

        //protected override void Initialize(RequestContext requestContext)
        //{
        //    base.Initialize(requestContext);

        //    ControllerName = requestContext.RouteData.Values["controller"].ToString();


        //    if (requestContext.HttpContext.User.Identity.IsAuthenticated)
        //    {
        //        try
        //        {

        //        }
        //        catch (Exception ex)
        //        {
        //            CreateErrorLog(ex);

        //        }
        //    }
        //}
        public UserInfo GetCurrentUserInfo
        {
            get
            {
                //OrganizationBLL OrganizationBLL = new OrganizationBLL();

                var userinfo = new OrganizationBLL().GetMemberByLoginName(User.Identity.Name);

                return userinfo;
            }
        }

        public string GetUserIdentityName(int UserID)
        {
            string result = "";

            //OrganizationBLL OrganizationBLL = new OrganizationBLL();

            var userinfo = new OrganizationBLL().GetMember(UserID);

            if (userinfo != null)
            {
                result =  @"\" + userinfo.SAMAccount;
            }

            return result;
        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            ControllerName = requestContext.RouteData.Values["controller"].ToString();

            string WebBaseURL = new CommonBLL().GetSystemParameter("WebBaseURL");
            var url = WebBaseURL + "Web/Error/NoUserFoundInSystem";

            if (requestContext.HttpContext.User.Identity.IsAuthenticated)
            {
                try
                {
                    ViewData["DisplayName"] = GetCurrentUserInfo.MemberName;
                    ViewData["CurrentUserID"] = GetCurrentUserID;

                    if (!GetCurrentUserInfo.IsActive.HasValue || !GetCurrentUserInfo.IsActive.Value)
                    {
                        requestContext.HttpContext.Response.Redirect(url);
                    }
                }
                catch (Exception ex)
                {
                    CreateErrorLog(ex);
                    requestContext.HttpContext.Response.Redirect(url);
                }
            }
        }
        public void CreateErrorLog(Exception ex)
        {
            CommonBLL CommonBLL = new CommonBLL();

            Log Log = new Log();
            Log.Date = DateTime.Now;
            Log.ErrorCode = "";
            Log.LogPriority = "";
            Log.LogType = "Error";
            Log.Message = ex.Message;
            Log.StackTrace = ex.ToString();
            Log.Remark = "";
            Log.Source = ControllerName;

            CommonBLL.CreateErrorLog(Log);
        }

        public void CreateDebugLog(string Message, string Name = "")
        {
            CommonBLL CommonBLL = new CommonBLL();

            Log Log = new Log();
            Log.Date = DateTime.Now;
            Log.ErrorCode = "";
            Log.LogPriority = "";
            Log.LogType = "Debug";
            Log.Message = Message;
            Log.StackTrace = Name;
            Log.Remark = "";
            Log.Source = ControllerName;

            CommonBLL.CreateErrorLog(Log);
        }
        public List<T> APICall<T>(string EndPoint, string Controller, Method MethodType, List<Parameter> Params,T Object) where T : class
        {

            var client = new RestClient(Controller);
            var restrequest = new RestRequest(EndPoint+"/", MethodType);
            foreach(Parameter pm in Params)
            {
                restrequest.AddParameter(pm.Name,pm.Value);
            }
           
            IRestResponse response = client.Execute(restrequest);
            var content = response.Content;
            return JsonConvert.DeserializeObject<List<T>>(content);

        }
       


    }
}