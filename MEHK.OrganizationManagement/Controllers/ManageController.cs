﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using MEHK.DAL.ViewModel;
using Kendo.Mvc.UI;
using MEHK.DAL.DTO;
using Kendo.Mvc.Extensions;
using RestSharp;
using System.Collections.Generic;

namespace MEHK.OrganizationManagement.Controllers
{
   
    public class ManageController : BaseController
    {
       
        public ManageController()
        {
        }
        public ActionResult Index()
        {
            ViewBag.CurrentUserID = GetCurrentUserID;
            ViewBag.AppID = 1;
            ViewBag.ProcessID = 1;
            SetBreadcrumbs("Organization Units");
            return View();
          
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult UnitMemberGridList([DataSourceRequest] DataSourceRequest request, int UnitId)
        {
            List<Parameter> pms = new List<Parameter>();
            pms.Add(new Parameter() { Name="UnitId", Value= UnitId });
            var UnitMembers = APICall("Members", OrganizationAPIBase, Method.GET,pms, new UserInfo());
                 return Json(UnitMembers.ToDataSourceResult(request, ModelState));
           
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UnitMemberGridListCreate(int MemberId, int UnitId,[DataSourceRequest] DataSourceRequest request )
        {

            List<Parameter> pms = new List<Parameter>();
            pms.Add(new Parameter() { Name = "UnitId", Value = UnitId });
            pms.Add(new Parameter() { Name = "MemberId", Value = MemberId });
            var UnitMembers = APICall("Members", OrganizationAPIBase, Method.POST, pms, new UserInfo());
            return Json(new[] { UnitMembers }.ToDataSourceResult(request, ModelState));
            //  return Json(new[] { v }.ToDataSourceResult(request, ModelState));

        }

        public ActionResult UnitMemberGridListUpdate( [DataSourceRequest] DataSourceRequest request, UserInfo v)
        {

            return Json(new[] { v }.ToDataSourceResult(request, ModelState));
         
        }

        public ActionResult UnitMemberGridListDestroy([DataSourceRequest] DataSourceRequest request , int MemberId, int UnitId)
        {

            List<Parameter> pms = new List<Parameter>();
            pms.Add(new Parameter() { Name = "MemberId", Value = MemberId });
             pms.Add(new Parameter() { Name = "UnitId", Value = UnitId });
            var UnitMembers = APICall("Members", OrganizationAPIBase, Method.DELETE, pms, new UserInfo());
            return Json(UnitMembers.ToDataSourceResult(request, ModelState));
        }
    }
}