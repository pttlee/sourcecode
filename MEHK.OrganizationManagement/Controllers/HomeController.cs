﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MEHK.OrganizationManagement.Models;

namespace MEHK.OrganizationManagement.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {

            //var client = new RestClient("http://192.168.1.227:8089/MEHK.CommonAPI/api/Organization");
            //var request = new RestRequest("OrganizationChart/", Method.GET);
            //IRestResponse response = client.Execute(request);
            //var content = response.Content;
            //List<OrganizationChart> orgchart = JsonConvert.DeserializeObject<List<OrganizationChart>>(content);
           // return View();
            return RedirectToAction("Index", "Manage");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}