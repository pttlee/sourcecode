﻿// listener is a function, optionally accepting an event and
// a function that prints the entire page
addPrintEventListener = function (listener) {

	// IE 5.5+ support and HTML5 standard
	if ("onbeforeprint" in window) {
		window.addEventListener('beforeprint', listener);
	}

		// Chrome 9+, Firefox 6+, IE 10+, Opera 12.1+, Safari 5.1+
	else if (window.matchMedia) {
		var mqList = window.matchMedia("print");

		mqList.addListener(function (mql) {
			if (mql.matches) listener();  // no standard event anyway
		});
	}

		// Your fallback method, only working for JS initiated printing
		// (but the easiest case because there is no need to cancel)
	else {
		(function (oldPrint) {
			window.print = function () {
				listener(undefined, oldPrint);
			}
		})(window.print);
	}
}

printContentFrameOnly = function (event) {
	if (event) event.preventDefault();  // not going to work
	//window.frames['webcontent'].focus();
	//window.frames['webcontent'].print();
}
addPrintEventListener(printContentFrameOnly);