﻿
var mode = "";


function onFormRequestEnd() {
    if (mode == "View") {
        DisableAll();
    }
}

function DisableAll() {

    mode = "View";

    $('form input[type=checkbox]').attr('disabled', 'true');


    // Disable all datepicker
    $(".k-calendar-container").each(function () {
        var elementId = this.id.split("_")[0];
        var tempDP = $("#" + elementId).data("kendoDatePicker");
        tempDP.enable(false);
    });

    // Now disable all DropDowns
    $(".k-list-container").each(function () {
        var elementId = this.id.split("-")[0];

        var tempDL = $("#" + elementId).data("kendoDropDownList");
        tempDL.enable(false);
    });

    $(".k-textbox").each(function () {

        if (this.id != 'CapexID' && this.id != 'BudgetID') {
            $(this).prop("disabled", true).addClass("k-state-disabled DisableControl");
        }
    });

    $(".k-formatted-value").each(function () {
        $(this).addClass("DisableControl");
    });


    $(".k-numerictextbox").each(function () {

        var numerictextbox = $(this).find("[data-role='numerictextbox']").data("kendoNumericTextBox");

        numerictextbox.enable();

        numerictextbox.enable(false);

    });

    $(".calBtn").each(function () {
        $(this).hide();
    });

    $("#openDialogBtn").hide();

}

function EnableAll() {
    mode = "Edit";

    $('form input[type=checkbox]').removeAttr("disabled");

    // Disable all datepicker
    $(".k-calendar-container").each(function () {
        var elementId = this.id.split("_")[0];
        var tempDP = $("#" + elementId).data("kendoDatePicker");
        tempDP.enable(true);
    });

    // Now disable all DropDowns
    $(".k-list-container").each(function () {
        var elementId = this.id.split("-")[0];
        var tempDL = $("#" + elementId).data("kendoDropDownList");
        tempDL.enable(true);
    });

    $(".k-textbox").each(function () {
        $(this).prop("disabled", false).removeClass("k-state-disabled DisableControl");
    });

    $(".k-formatted-value").each(function () {
        $(this).removeClass("DisableControl");
    });


    $(".k-numerictextbox").each(function () {

        var numerictextbox = $(this).find("[data-role='numerictextbox']").data("kendoNumericTextBox");

        numerictextbox.enable();

        numerictextbox.enable(true);

    });


    $(".calBtn").each(function () {
        $(this).show();
    });

    $("#openDialogBtn").show();

}




function onGridError(args) {

    var message = '';

    if (args.errors) {
        $.each(args.errors, function (key, value) {
            if ('errors' in value) {
                $.each(value.errors, function () {
                    message += this + "\n";
                });
            }
        });
    }
    else {
        message = "There are some errors:\n";
    }

    alert(message);
}


function GetFormData($form) {
	var unindexed_array = $form.serializeArray();
	var indexed_array = {};

	$.map(unindexed_array, function (n, i) {
		indexed_array[n['name']] = n['value'];
	});

	return indexed_array;
}


function GetConfirmTitle(event) {
    return ConfirmTitle.replace('{0}', $(event.target).text().toLowerCase());
}


function FormValidError(formID)
{
	 //mApp.scrollTo(formID);

     //swal({
     //    "title": "",
     //    "text": "There are some errors in your submission. Please correct them.",
     //    "type": "error",
     //    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
     //});
	 
	var fullTop = $(document).height();
	var targetTop = $('.field-validation-error:visible:first').offset().top;

	var goTo = targetTop + 300 > fullTop ? targetTop : targetTop - 300;
	var message = "There are some errors in your submission. Please correct them.";

	ShowErrorCore(goTo, message); 
	 
}



function ShowErrorCore(yValue, message) {

	$('html, body').animate({
		scrollTop: (yValue)
	}, 1000);

	swal({
		"title": "",
		"text": message,
		"type": "error",
		"confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
	},
	function () {

		$('html, body').animate({
			scrollTop: (yValue)
		}, 1);
	});
}



function DecimalToString(amount)
{
	return numeral(amount).format('(0,0.00)')	
}