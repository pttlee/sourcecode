﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using MEHK.DAL.DTO;
using MEHK.DAL.Model;

namespace MEHK.BLL
{
	public class BaseBLL
	{
		protected Entities GetDB
		{
			get
			{
				return new Entities();
			}
		}

		protected object GetPropertyAttributes<T>(T target, string propertyName, string attributeName) where T : class
		{
			//PropertyInfo prop = typeof(target).GetProperty(propertyName);
			PropertyInfo prop = target.GetType().GetProperty(propertyName);
			var val = GetPropertyAttributes(prop, attributeName);
			return val;
		}
		protected object GetPropertyAttributes(PropertyInfo prop, string attributeName)
		{
			// look for an attribute that takes one constructor argument
			foreach (CustomAttributeData attribData in prop.GetCustomAttributesData())
			{
				string typeName = attribData.Constructor.DeclaringType.Name;
				if (attribData.ConstructorArguments.Count == 1 &&
					(typeName == attributeName || typeName == attributeName + "Attribute"))
				{
					return attribData.ConstructorArguments[0].Value;
				}
			}
			return null;
		}


		protected object GetPropValue(object src, string propName)
		{
			return src.GetType().GetProperty(propName).GetValue(src, null);
		}
		protected BaseResult EntityGeneral<T>(T insertObject, EntityState mode) where T : class
		{
			dynamic id = new ExpandoObject();
			return EntityGeneral(insertObject, mode, null, ref id);
		}

		protected BaseResult EntityGeneral<T>(T insertObject, EntityState mode, string idName, ref dynamic id) where T : class
		{
			BaseResult result = new BaseResult();
			result.IsSuccess = true;
			using (var db = GetDB)
			{
				try
				{
					//db.Set<T>().Add(insertObject);
					db.Entry(insertObject).State = mode;

					// call SaveChanges method to save new Student into database
					db.SaveChanges();

					if (!String.IsNullOrEmpty(idName))
					{
						dynamic temp = insertObject;
						id = temp.GetType().GetProperty(idName).GetValue(temp, null);
					}
				}
				catch (Exception ex)
				{
					result.IsSuccess = false;
					result.Message = ex.Message;
                    Exception eex = new Exception(ex.Message + ";inner:" + ex.InnerException.Message);
                   
                    new CommonBLL().CreateErrorLog(eex, insertObject.GetType().ToString());
				}
				return result;
			}
		}


		protected void DelGeneral<T>(IQueryable<T> targetIQueryList) where T : class
		{
			var targetList = targetIQueryList.ToList();
			foreach (var item in targetList)
			{
				EntityGeneral(item, EntityState.Deleted);
			}
		}


		public string Right(string input)
		{
			int length = 60;
			if (length >= input.Length)
			{
				return input;
			}
			else
			{
				return input.Substring(input.Length - length);
			}
		}
		public string Right(string input, int length)
		{
			if (length >= input.Length)
			{
				return input;
			}
			else
			{
				return input.Substring(input.Length - length);
			}
		}



		private double RoundUp(double input)
		{
			double multiplier = Math.Pow(10, Convert.ToDouble(2));
			return Math.Ceiling(input * multiplier) / multiplier;
		}

		private double RoundUp(double input, int places)
		{
			double multiplier = Math.Pow(10, Convert.ToDouble(places));
			return Math.Ceiling(input * multiplier) / multiplier;
		}


		public double RoundRate(double input)
		{
			return Math.Round(input, 2);
		}


		protected int GetFinancialYearID()
		{
			int result = -1;
			var dt = DateTime.Now;

			var year = dt.Year;
			var month = dt.Month;

			year = month >= 4 ? year + 1 : year;
			var yearValue = year.ToString();

			using (var db = GetDB)
			{
				var temp = db.FinancialYears.Where(q => q.Name == yearValue).FirstOrDefault();

				if (temp != null)
				{
					result = temp.ID;
				}
			}
			return result;
		}
        
	}
}
