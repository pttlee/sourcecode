﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using MEHK.DAL.DTO;
using MEHK.DAL.Enum;
using MEHK.DAL.Model;
using MEHK.DAL.ViewModel;

namespace MEHK.BLL
{
    public partial class OrganizationBLL : BaseBLL
    {

        public Unit GetUnit(int UnitId)
        {
            return GetDB.Units.Where(x => !x.IsDeleted && x.UnitId == UnitId).FirstOrDefault();
        }
        public List<Unit> GetSubUnit(int UnitId)
        {
            return GetDB.Units.Where(x => !x.IsDeleted && x.ParentUnitId == UnitId).ToList();
        }
        public List<Unit> GetUnits(string UnitName)
        {
            return GetDB.Units.Where(x => !x.IsDeleted && (string.IsNullOrEmpty(UnitName) || x.UnitName.ToLower().Contains(UnitName.ToLower()))).ToList();
        }
        public UnitType GetUnitTypes(int id)
        {
            return GetDB.UnitTypes.Where(x => x.UnitTypeId == id && !x.IsDeleted).FirstOrDefault();
        }
        public List<UnitType> GetAllUnitTypes()
        {
            return GetDB.UnitTypes.Where(x => !x.IsDeleted).ToList();
        }
        public List<UnitType> GetUnitTypes(string Name)
        {
            return GetDB.UnitTypes.Where(x => x.Description.ToLower().Contains(Name.ToLower()) && !x.IsDeleted).ToList();
        }
        public Queue<Unit> GetUnitsWithAbove(int UnitId)
        {
            Queue<Unit> Result = new Queue<Unit>();
            Unit CurrentUnit = GetUnit(UnitId);
            Result.Enqueue(CurrentUnit);
            //lee 2019-10-15 
            //while (CurrentUnit.ParentUnitId.HasValue)
            //{
            //    CurrentUnit = GetUnit(CurrentUnit.ParentUnitId.Value);
            //    if (CurrentUnit != null)
            //        Result.Enqueue(CurrentUnit);
            //    else
            //        break;
            //}
            return Result;
        }
        public Unit GetDivisionUnit(int UnitId)
        {
            Unit Result = new Unit();
        
            Unit CurrentUnit = GetUnit(UnitId);
            if (CurrentUnit != null)
            {
                var unitType = GetUnitTypes(CurrentUnit.UnitTypeId);
                if(unitType !=null && unitType.Code== "Division")
                {
                    Result= CurrentUnit;
                }
                else
                {
                    while (CurrentUnit.ParentUnitId.HasValue)
                    {
                        CurrentUnit = GetUnit(CurrentUnit.ParentUnitId.Value);
                        var _unitType = GetUnitTypes(CurrentUnit.UnitTypeId);
                        if (_unitType != null && _unitType.Code == "Division")
                        {
                            Result= CurrentUnit;
                        }
                        else
                            break;
                    }
                }
            }
            //lee 2019-10-15 
           
            return Result;
        }
         
        public List<Unit> GetUnitsWithBelow(int UnitId, List<Unit> unitlist)
        {
            //Queue<Unit> Result = new Queue<Unit>();
            //Unit CurrentUnit = GetUnit(UnitId);
            //Result.Enqueue(CurrentUnit);
            var SubUnit = GetSubUnit(UnitId);
            //if (SubUnit.Count() > 0)
            //{
            //    foreach (var item in SubUnit)
            //    {
            //        Result.Enqueue(item);
            //    }

            //}
          
            if (SubUnit.Count() > 0)
            {
                foreach (var item in SubUnit)
                {
                    unitlist.Add(item);
                    unitlist= GetUnitsWithBelow(item.UnitId, unitlist);
                  
                   
                }
             
            }
            return unitlist;
        }
        public List<UserInfo> GetUnitMembers(int UnitID, bool includeSubUnitMember = false)
        {

            return includeSubUnitMember ? GetDB.UnitMembers.Where(x => !x.IsDeleted && x.UnitId == UnitID).ToList().Select(y => GetMember(y.MemberId, CreatedDate: y.CreatedDate)).ToList() :
                    GetDB.vMembers.Where(x => x.UnitId == UnitID).ToList().Select(y => GetMember(y.MemberId, CreatedDate: y.UnitMemberCreateDate)).ToList();
        }

        public List<Unit> GetAllUnitByMemberId(int MemberId)
        {
            return GetDB.UnitMembers.Where(x => x.MemberId == MemberId && !x.IsDeleted).ToList().Select(x => GetUnit(x.UnitId)).ToList();
        }
        public bool IsParentUnit(int Unitid)
        {
            return GetDB.Units.Where(x => !x.IsDeleted && (x.ParentUnitId.HasValue && x.ParentUnitId == Unitid)).Count() > 0;
        }
        public void UpdateUnit(UpdateUnitModel PutUnits, ref BaseResult bs, bool isDelete = false)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                //BaseResult result = new BaseResult();
                Unit _unit = GetDB.Units.Where(x => x.UnitId == PutUnits.UnitId).FirstOrDefault();
                if (_unit == null)
                {
                    bs.IsSuccess = false;
                    bs.Message += "Unit Not Exist";
                }

                if (!isDelete)
                {
                    _unit.UnitShortName = PutUnits.UnitName;
                    _unit.UnitName = PutUnits.Description;
                    _unit.ParentUnitId = PutUnits.ParentUnitId.HasValue ? PutUnits.ParentUnitId : -1;
                    _unit.UnitTypeId = PutUnits.UnitTypeId;
                }
                _unit.ModifiedDate = DateTime.Now;
                _unit.ModifiedBy = PutUnits.ModifiedBy;
                _unit.IsDeleted = isDelete;
                BaseResult result = EntityGeneral(_unit, EntityState.Modified);
                if (!result.IsSuccess)
                {
                    bs.IsSuccess = false;
                    bs.Message += result.Message;
                }
                var DeleteCC = GetDB.UnitCostCenters.Where(x => x.UnitId == PutUnits.UnitId).ToList();
                if (!isDelete)
                {
                    foreach (UnitCostCenter ucc in DeleteCC)
                    {
                        EntityGeneral(ucc, EntityState.Deleted);

                    }

                }
                if (PutUnits.CostCenters != null && PutUnits.CostCenters.Count > 0)
                {
                    foreach (int ccid in PutUnits.CostCenters.ToList())
                    {
                        UnitCostCenter ucc = new UnitCostCenter();
                        ucc.CostCenterId = ccid;
                        ucc.UnitId = _unit.UnitId;
                        ucc.CreatedBy = PutUnits.ModifiedBy;
                        ucc.CreatedDate = DateTime.Now;
                        BaseResult resultucc = EntityGeneral(ucc, EntityState.Added);
                        if (!result.IsSuccess)
                        {
                            bs.IsSuccess = false;
                            bs.Message += result.Message;
                        }
                    }
                }

                scope.Complete();
            }
            return;
            // GetDB.Entry(_unit).State(EntityState.Modified);
            //  return new BaseResult() { IsSuccess = true }; 
        }
        public void CreateUnit(UpdateUnitModel PutUnits, ref BaseResult bs)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                Unit _unit = new Unit();
                _unit.UnitShortName = PutUnits.UnitName;
                _unit.UnitName = PutUnits.Description;
                _unit.ParentUnitId = PutUnits.ParentUnitId.HasValue ? PutUnits.ParentUnitId : -1;
                _unit.UnitTypeId = PutUnits.UnitTypeId;
                _unit.ModifiedDate = DateTime.Now;
                _unit.ModifiedBy = PutUnits.ModifiedBy;
                _unit.CreatedBy = PutUnits.ModifiedBy;
                _unit.CreatedDate = DateTime.Now;
                BaseResult result = EntityGeneral(_unit, EntityState.Added);
                if (!result.IsSuccess)
                {
                    bs.IsSuccess = false;
                    bs.Message += result.Message;
                }
                foreach (int ccid in PutUnits.CostCenters.ToList())
                {
                    UnitCostCenter ucc = new UnitCostCenter();
                    ucc.CostCenterId = ccid;
                    ucc.UnitId = _unit.UnitId;
                    ucc.CreatedBy = PutUnits.ModifiedBy;
                    ucc.CreatedDate = DateTime.Now;
                    BaseResult resultucc = EntityGeneral(ucc, EntityState.Added);
                    if (!result.IsSuccess)
                    {
                        bs.IsSuccess = false;
                        bs.Message += result.Message;
                    }
                }

                scope.Complete();
            }

            return;
        }
        public int GetIdfromUnitMember(int MemberId, int UnitId)
        {
            using (var db = GetDB)
            {
                var UnitMember = db.UnitMembers.Where(x => x.MemberId == MemberId && x.UnitId == UnitId).FirstOrDefault();
                if (UnitMember != null)
                    return UnitMember.Id;
            }
            return -1;
        }


    }
}
