﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using MEHK.DAL.Enum;
using MEHK.DAL.ViewModel;

namespace MEHK.BLL
{
	public class TheLinkBaseAuthorize : AuthorizeAttribute
	{

		public string PageRight { get; set; }
		public string[] PageRights { get; set; }
		protected string AppName { get; set; }




		protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
		{
			base.HandleUnauthorizedRequest(filterContext);

			filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "General", action = "NoPermission", area = "" }));
		}

		//protected override bool AuthorizeCore(HttpContextBase httpContext)
		//{
		//	if (GetUserID(httpContext) == -1)
		//	{
		//		return false;
		//	}

		//	if (String.IsNullOrEmpty(PageRight) && PageRights == null)
		//	{
		//		return true;
		//	}

		//	bool result = false;

		//	var userRight = UserRight(httpContext);

		//	var pageRightsList = PageRights != null ? PageRights.ToList() : new List<string>();
		//	if (!String.IsNullOrEmpty(PageRight))
		//	{
		//		pageRightsList.Add(PageRight);
		//	}

		//	try
		//	{
		//		foreach (var pageRight in pageRightsList)
		//		{
		//			result = IsPropertyExist(userRight, pageRight);
		//			if (result)
		//			{
		//				break;
		//			}
		//		}
		//	}
		//	catch
		//	{
		//		result = false;
		//	}

		//	return result;
		//}

		public MEHK.BLL.OrganizationBLL OrganizationMain
		{
			get
			{
				return new MEHK.BLL.OrganizationBLL();
			}
		}


		public MEHK.BLL.CommonBLL CommomMain
		{
			get
			{
				return new MEHK.BLL.CommonBLL();
			}
		}

		public int GetUserID(HttpContextBase content)
		{

			int userId = -1;
			var tempUser = OrganizationMain.GetMembers(false,GetUserLoginName(content));
			if (tempUser != null)
			{
				var user = tempUser.FirstOrDefault();
                if (user != null)
                    userId = user.MemberId;
			}
			return userId;
		}

		public string GetUserLoginName(HttpContextBase httpContext)
		{
			string result = "";

			result = httpContext.User.Identity.Name;

			return result;
		}

		//protected dynamic UserRight(HttpContextBase content)
		//{
		//	dynamic userRight = new System.Dynamic.ExpandoObject();
		//	var userRightList = GetUserRight(content);

		//	foreach (var right in userRightList)
		//	{
		//		AddProperty(userRight, right.RightName, true);
		//	}

		//	return userRight;
		//}

		//protected List<RightViewModel> GetUserRight(HttpContextBase content)
		//{
		//	var appID = CommomMain.GetAppID(AppName);

		//	List<RightViewModel> result = OrganizationMain.GetUserRight(GetUserID(content), appID).ToList();
		//	return result;
		//}

		protected object GetPropValue(object src, string propName)
		{
			return src.GetType().GetProperty(propName).GetValue(src, null);
		}

		protected void AddProperty(System.Dynamic.ExpandoObject expando, string propertyName, object propertyValue)
		{
			// ExpandoObject supports IDictionary so we can extend it like this
			var expandoDict = expando as IDictionary<string, object>;
			if (expandoDict.ContainsKey(propertyName))
			{
				expandoDict[propertyName] = propertyValue;
			}
			else
			{
				expandoDict.Add(propertyName, propertyValue);
			}
		}
		protected bool IsPropertyExist(dynamic settings, string name)
		{
			if (settings is System.Dynamic.ExpandoObject)
			{
				return ((IDictionary<string, object>)settings).ContainsKey(name);
			}
			return settings.GetType().GetProperty(name) != null;
		}

	}
}
