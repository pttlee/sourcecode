﻿using CommonHelper;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using MEHK.DAL.DTO;
using MEHK.DAL.Enum;
using MEHK.DAL.Model;
using MEHK.DAL.ViewModel;
using System.Data.SqlClient;

namespace MEHK.BLL
{
    public class CommonBLL : BaseBLL
    {

        protected OrganizationBLL OrganizationMain
        {
            get
            {
                return new OrganizationBLL();
            }
        }
        protected MasterBLL MasterMain
        {
            get
            {
                return new MasterBLL();
            }
        }

        public string GetSystemParameter(string Key)
        {
            string result = "";

            try
            {
                //int financialYearID = base.GetFinancialYearID();

                using (var DB = GetDB)
                {
                    result = DB.SystemParameters.Where(n => n.AppKey == Key)
                        .Select(n => n.Value)
                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }



   
      
        public BaseResult UploadFiles(HttpRequest httpRequest, int? userID = null, string formID = null, string processCode = null, string remark = null, string attachmentType = null, string section = null)
        {
            BaseResult result = new BaseResult();
            var files = httpRequest.Files;

            if (httpRequest.Files.Count > 0)
            {
                //foreach (HttpPostedFile file in files) //HttpPostedFileBase, HttpPostedFile
                for (int i = 0; i < files.Count; i++)
                {
                    using (var DB = GetDB)
                    {
                        var process = MasterMain.GetProcess(processCode);
                        FileAttachment newFile = new FileAttachment();
                        newFile.ID = Guid.NewGuid();
                        newFile.ProcessID = process != null ? process.ProcessId : 404;
                        newFile.FormID = Convert.ToInt32(formID);

                        //newFile.TypeID = 1;
                        newFile.FileOwner = userID;
                        newFile.FileName = files[i].FileName;
                        newFile.FileSize = files[i].ContentLength;
                        newFile.UploadedDate = DateTime.Now;
                        var fileArr = CompressHelper.Compress(files[i].InputStream.ToByteArray());
                        newFile.FileData = fileArr;

                        newFile.Remark = remark;
                        newFile.FileSection = section;
                        newFile.FileCode = attachmentType;

                        result = EntityGeneral(newFile, System.Data.Entity.EntityState.Added);
                    }
                }
            }

            return result;
        }


        public byte[] DownloadFile(Guid fileID, string processCode = null)
        {
            byte[] result = null;
            using (var DB = GetDB)
            {
                var temp = DB.FileAttachments.Where(q => q.ID == fileID).FirstOrDefault();
                if (temp != null)
                {
                    result = CompressHelper.Decompress(temp.FileData);
                }
            }
            return result;
        }

        public byte[] DownloadFile(Guid fileID, ref string fileName, string processCode = null)
        {
            byte[] result = null;
            using (var DB = GetDB)
            {
                var temp = DB.FileAttachments.Where(q => q.ID == fileID).FirstOrDefault();
                if (temp != null)
                {
                    result = CompressHelper.Decompress(temp.FileData);

                    fileName = temp.FileName;
                }
            }
            return result;
        }


        public BaseResult DeleteFile(Guid fileID, string processCode = null)
        {
            BaseResult result = new BaseResult();
            using (var DB = GetDB)
            {
                var temp = DB.FileAttachments.Where(q => q.ID == fileID).FirstOrDefault();
                if (temp != null)
                {
                    result = EntityGeneral(temp, System.Data.Entity.EntityState.Deleted);
                }
            }
            return result;
        }

        public List<FileList> GetFileList(string processCode, string formID)
        {
            var process = MasterMain.GetProcess(processCode);
            int processID = process != null ? process.ProcessId : -1;
            var formIDValue = Convert.ToInt32(formID);
            using (var DB = GetDB)
            {
                var result = DB.FileAttachments.Where(q => q.ProcessID == processID && q.FormID == formIDValue).ToList().Select(q => new FileList(q)).ToList();
                return result;
            }
        }


        public ProcessList GetProcessList(string code)
        {
            using (var DB = GetDB)
            {
                var result = DB.ProcessLists.Where(q => q.Code == code).FirstOrDefault();
                return result;
            }
        }

        public  void CreateDebugLog(string Message)
        {
            Entities Entities = new Entities();
            Log Log = new Log();
            Log.Date = DateTime.Now;
            Log.ErrorCode = "";
            Log.LogPriority = "";
            Log.LogType = "Error";
            Log.Message = Message;
            Log.StackTrace = "";
            Log.Remark = "";
            Log.Source = "";

            Entities.Entry(Log).State = System.Data.Entity.EntityState.Added;

            Entities.SaveChanges();
        }

        public void CreateErrorLog(Exception ex, string SourceName, string Remark="")
        {
            Entities Entities = new Entities();
            Log Log = new Log();
            Log.Date = DateTime.Now;
            Log.ErrorCode = "";
            Log.LogPriority = "";
            Log.LogType = "Error";
            Log.Message = ex.Message;
            Log.StackTrace = ex.ToString();
            Log.Remark = Remark;
            Log.Source = SourceName;

            Entities.Entry(Log).State = System.Data.Entity.EntityState.Added;

            Entities.SaveChanges();
        }
        public void CreateErrorLog(Log Log)
        {
            Entities Entities = new Entities();
           

            Entities.Entry(Log).State = System.Data.Entity.EntityState.Added;

            Entities.SaveChanges();
        }

        public BaseResult AddTempFileInfo(Guid tempID, Guid fileID, string json)
        {
            using (var db = GetDB)
            {
                TempFileInfo addInfo = new TempFileInfo();
                addInfo.CreateDate = DateTime.Now;
                addInfo.TempID = tempID;
                addInfo.FileID = fileID;
                addInfo.Detail = json;
                return EntityGeneral(addInfo, System.Data.Entity.EntityState.Added);
            }
        }

        public List<TempFileInfo> GetTempFiles(Guid tempID)
        {
            using (var db = GetDB)
            {
                var result = db.TempFileInfoes.Where(q => q.TempID == tempID).ToList();
                return result;
            }
        }
        public void DelTempFiles(Guid tempID)
        {
            using (var db = GetDB)
            {
                var temp = db.TempFileInfoes.Where(q => q.TempID == tempID).ToList();

                foreach (var item in temp)
                {
                    try
                    {
                        db.TempFileInfoes.Remove(item);
                        db.SaveChanges();
                    }
                    catch
                    {
                    }
                }
                //return result;
            }
        }
        public void DelTempFile(Guid fileID, Guid tempID)
        {
            using (var db = GetDB)
            {
                var temp = db.TempFileInfoes.Where(q => q.TempID == tempID && q.FileID == fileID).ToList();

                foreach (var item in temp)
                {
                    try
                    {
                        db.TempFileInfoes.Remove(item);
                        db.SaveChanges();
                    }
                    catch
                    {
                    }
                }
                //return result;
            }
        }


        public List<FileInfoViewModel> GetUploadedFiles(int formID, string processCode)
        {
            List<FileInfoViewModel> list = new List<FileInfoViewModel>();

            using (var db = GetDB)
            {
                var ProcessID = MasterMain.GetProcess(processCode);

                list = db.FileAttachments.Where(n => n.FormID == formID && n.ProcessID == ProcessID.ProcessId).ToList()
                                        .Select(n => new FileInfoViewModel()
                                        {
                                            FileType = n.FileCode,
                                            DocumentName = n.FileName,
                                            FileID = n.ID,
                                            FileSize = n.FileSize,
                                            HasUpload = true,
                                            Remark = n.Remark,
                                            Title = n.FileTitle,
                                            //UploadedDate = n.UploadedDate.ToString("dd-MM-yyyy HH:mm"),
                                            UploadedDate = n.UploadedDate.ToString("yyyy/MM/dd HH:mm"),
                                            UploadedByUserID = n.FileOwner.Value,
                                            Section = n.FileSection,
                                            CatName = n.CatName,
                                            UploadAfterApproved = n.UploadAfterApproved == null ? false : n.UploadAfterApproved.Value

                                        }).ToList();

                foreach (var item in list)
                {
                    var UserDisplayName = OrganizationMain.GetMember(item.UploadedByUserID).MemberName;

                    item.UploadedBy = UserDisplayName;
                }
            }

            return list;
        }
        //string TempFolder = WebConfigurationManager.AppSettings["TempFolder"].ToString();

        public string TempFolder
        {
            get
            {
                return GetSystemParameter(AppKey.UploadTempFolder.ToString());
            }
        }

        public Guid UpdateFile(Guid TempFileID, string processCode, int userID)
        {
            Guid returnedGuid = new Guid();

            using (var db = GetDB)
            {
                var ProcessID = MasterMain.GetProcess(processCode);

                string TempFileFolder = TempFolder + TempFileID;
                DirectoryInfo directory = new DirectoryInfo(TempFileFolder);
                FileInfo[] files = directory.GetFiles();

                if (files.Length > 0)
                {
                    FileAttachment newFile = new FileAttachment();
                    newFile.ID = Guid.NewGuid();
                    newFile.ProcessID = ProcessID.ProcessId;
                    //newFile.FormID = Convert.ToInt32(ID.Value);
                    newFile.FormID = -1;
                    newFile.FileOwner = userID;
                    newFile.FileName = files[0].Name;
                    newFile.FileSize = files[0].Length;
                    newFile.UploadedDate = DateTime.Now;
                    byte[] bytes = System.IO.File.ReadAllBytes(files[0].FullName);
                    var fileArr = CompressHelper.Compress(bytes);
                    newFile.FileData = fileArr;
                    newFile.FileTitle = "";
                    newFile.Remark = "";
                    newFile.FileSection = "";
                    newFile.FileCode = "";
                    newFile.CatName = "";
                    newFile.UploadAfterApproved = false;
                    db.Entry(newFile).State = System.Data.Entity.EntityState.Added;

                    returnedGuid = newFile.ID;
                }



                db.SaveChanges();

            }

            return returnedGuid;
        }



        //string TempFolder = @"D:\Temporary\";
        public void UpdateFile(List<FileInfoViewModel> listFileViewModel, string processCode, int formID, int userID, bool UploadAfterApproved)
        {
            using (var db = GetDB)
            {
                var ProcessID = MasterMain.GetProcess(processCode);

                List<FileAttachment> UploadedFileAttachments = new List<FileAttachment>();

                if (UploadAfterApproved)
                {
                    UploadedFileAttachments = db.FileAttachments.Where(n => n.FormID == formID && n.ProcessID == ProcessID.ProcessId && n.UploadAfterApproved == true).ToList();
                }
                else
                {
                    UploadedFileAttachments = db.FileAttachments.Where(n => n.FormID == formID && n.ProcessID == ProcessID.ProcessId && (n.UploadAfterApproved == false || n.UploadAfterApproved == null)).ToList();
                }



                foreach (var item in listFileViewModel)
                {
                    if (!item.HasUpload)
                    {
                        string TempFileFolder = TempFolder + item.FileID;
                        DirectoryInfo directory = new DirectoryInfo(TempFileFolder);
                        FileInfo[] files = directory.GetFiles();

                        if (files.Length > 0)
                        {
                            FileAttachment newFile = new FileAttachment();
                            newFile.ID = Guid.NewGuid();
                            newFile.ProcessID = ProcessID.ProcessId;
                            //newFile.FormID = Convert.ToInt32(ID.Value);
                            newFile.FormID = formID;
                            newFile.FileOwner = userID;
                            newFile.FileName = files[0].Name;
                            newFile.FileSize = files[0].Length;
                            newFile.UploadedDate = DateTime.Now;
                            byte[] bytes = System.IO.File.ReadAllBytes(files[0].FullName);
                            var fileArr = CompressHelper.Compress(bytes);
                            newFile.FileData = fileArr;
                            newFile.FileTitle = item.Title;
                            newFile.Remark = item.Remark;
                            newFile.FileSection = item.Section;
                            newFile.FileCode = item.FileType;
                            newFile.CatName = item.CatName;
                            newFile.UploadAfterApproved = UploadAfterApproved;
                            db.Entry(newFile).State = System.Data.Entity.EntityState.Added;
                        }

                    }

                    db.SaveChanges();
                }

                foreach (var item in UploadedFileAttachments)
                {
                    bool exist = listFileViewModel.Any(n => n.FileID == item.ID);

                    if (!exist)
                    {
                        var DeleteFile = db.FileAttachments.Where(n => n.ID == item.ID).FirstOrDefault();

                        db.Entry(DeleteFile).State = System.Data.Entity.EntityState.Deleted;
                    }
                }

                db.SaveChanges();
            }
        }
        
        public PageMenu GetMenus(int userID, int appID)
        {
            //var v = GetDB.GetSiteMenubyUserID(userID, appID).ToList();

            var v = GetDB.Database
              .SqlQuery<SiteMenuModel>("GetSiteMenubyUserID @userID, @appID",
              new SqlParameter("@userID", userID),
               new SqlParameter("@appID", appID)
              ).ToList();
            PageMenu menu = null;
            PopulateMenu(ref menu, v);
            return menu;
        }
        public void PopulateMenu(ref PageMenu root, List<SiteMenuModel> menus)
        {
            if (root == null)
            {
                root = new PageMenu();
                root.MenuName = "";
                root.MenuDisplayName = "";
				root.PageNameDisplayName_CN = "";

				root.nID = 0;
                root.Pages = new List<PageMenu>();

                var tempMenus = menus.Where(n => n.ParentID == null).GroupBy(x => x.ID)
                                  .Select(g => g.First())
                                  .ToList();

                foreach (var tempMenu in tempMenus)
                {
                    var child = new PageMenu()
                    {
                        nID = tempMenu.ID,
                        MenuName = tempMenu.PageName,
                        MenuDisplayName = tempMenu.PageDisplayName,
						PageNameDisplayName_CN=tempMenu.PageNameDisplayName_CN,

						URL = tempMenu.URL
                    };

                    PopulateMenu(ref child, menus);

                    root.Pages.Add(child);

                }
            }
            else
            {
                var id = (int)root.nID;

                var tempMenus = menus.Where(n => n.ParentID == id).GroupBy(x => x.ID)
                  .Select(g => g.First())
                  .ToList();

                root.Pages = new List<PageMenu>();

                foreach (var tempMenu in tempMenus)
                {

                    var child = new PageMenu()
                    {
                        nID = tempMenu.ID,
                        MenuName = tempMenu.PageName,
                        MenuDisplayName = tempMenu.PageDisplayName,
						PageNameDisplayName_CN=tempMenu.PageNameDisplayName_CN,

						URL = tempMenu.URL
                    };

                    PopulateMenu(ref child, menus);
                    root.Pages.Add(child);
                }
            }
        }

        public List<RightViewModel> GetUserRight(string  SAMACCOUNT, int appID)
        {
            //var v = GetDB.GetSiteMenubyUserID(userID, appID).ToList();

            var v = GetDB.Database
              .SqlQuery<RightViewModel>("GetMemberRightbyMemberID @SAMACCOUNT, @AppID",
              new SqlParameter("@SAMACCOUNT", SAMACCOUNT),
               new SqlParameter("@AppID", appID)
              ).ToList();
       
            return v;
        }





    }
}
