﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEHK.DAL.DTO;
using MEHK.DAL.Enum;
using MEHK.DAL.Model;
using MEHK.DAL.ViewModel;

namespace MEHK.BLL
{
    public partial class OrganizationBLL : BaseBLL
    {

        public List<Role> GetRolesByMemberId(int MemberId)
        {
            using (var db = GetDB)
            {
                var roleIdList = db.RoleMembers.Where(x => !x.IsDeleted && x.MemberId == MemberId).Select(y => y.RoleId).ToList();
                var roleList = db.Roles.Where(x =>  roleIdList.Contains(x.RoletId)).ToList();
                return roleList;
            }
               
        }
        public List<UserInfo> GetMembersByRole(int RoleId)
        {
            using (var db = GetDB)
            {
                var UserInfoList = db.RoleMembers.Where(x => !x.IsDeleted && x.RoleId == RoleId).ToList().Select(y => GetMember(y.MemberId)).ToList();
                    
                
                return UserInfoList;
            }

        }
        public Role GetRole(int RoleId)
        {
            using (var db = GetDB)
            {
               
                var role = db.Roles.Where(x => x.RoletId== RoleId).FirstOrDefault();
                return role;
            }

        }
        public List<Role> GetRoles()
        {
            using (var db = GetDB)
            {

                var role = db.Roles.ToList();
                return role;
            }

        }


    }
}
