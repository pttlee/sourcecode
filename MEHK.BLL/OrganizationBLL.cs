﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEHK.DAL.DTO;
using MEHK.DAL.Enum;
using MEHK.DAL.Model;
using MEHK.DAL.ViewModel;
using System.Transactions;
using System.Net.Http;
using Newtonsoft.Json;
namespace MEHK.BLL
{
    public partial class OrganizationBLL : BaseBLL
    {

        public OrganizationBLL()
        {


        }
        public UserInfo GetMemberByLoginName(string loginName)
        {
            using (var db = GetDB)
            {
                var loginNameValue = loginName.Contains('\\') ? loginName.Split('\\')[1] : loginName;
                loginNameValue = loginNameValue.ToLower();
                var v = db.vMembers.Where(q => q.SAMAccount.ToLower() == loginNameValue && q.IsActive.HasValue && q.IsActive.Value).ToList().Select(q => new UserInfo(q)).FirstOrDefault();
                GetUserAdditionalValue(v);

                return v;
            }
        }
        public List<UserInfo> GetMembers(bool IncludeDeleted = false, string MemberName = "", string SAMAccount = "", string SAPAccount = "", string StaffID = "", bool DisplayDepartmentAndDivision = true, bool additional = true)
        {
            using (var db = GetDB)
            {
                List<vMember> result = new List<vMember>();
                if (string.IsNullOrEmpty(SAMAccount) && string.IsNullOrEmpty(SAPAccount) && string.IsNullOrEmpty(StaffID) && string.IsNullOrEmpty(MemberName))
                {
                    result = db.vMembers.Where(q => (IncludeDeleted || (q.IsActive.HasValue && q.IsActive.Value))
                    ).ToList();
                }
                else
                {
                    result = db.vMembers.Where(n =>
                           (string.IsNullOrEmpty(SAMAccount) || n.SAMAccount.ToLower() == SAMAccount.ToLower()) &&
                           (string.IsNullOrEmpty(SAPAccount) || n.SAPAccount.ToLower() == SAPAccount.ToLower()) &&
                             (string.IsNullOrEmpty(StaffID) || n.StaffId.ToLower() == StaffID.ToLower())
                            ).ToList();

                    result = result.Where(q => (IncludeDeleted || (q.IsActive.HasValue && q.IsActive.Value))
                         && (string.IsNullOrEmpty(MemberName) || q.MemberName.ToLower().Contains(MemberName.ToLower()))
                         ).ToList();
                }

                var resultUserInfo = result.Select(q => new UserInfo(q, DisplayDepartmentAndDivision)).ToList();
                if (additional)
                {
                    foreach (var item in resultUserInfo)
                    {
                        GetUserAdditionalValue(item);
                    }
                }
                return resultUserInfo;
            }
        }
        public UserInfo GetMember(int MemberId, bool IncludeDeleted = false, DateTime? CreatedDate = null)
        {
            using (var db = GetDB)
            {
                var result = db.vMembers.Where(q => (IncludeDeleted || (q.IsActive.HasValue && q.IsActive.Value))
                && q.MemberId == MemberId).ToList().Select(q => new UserInfo(q)).FirstOrDefault();
                if (result == null)
                    return null;
                GetUserAdditionalValue(result);
                if (CreatedDate.HasValue)
                    result.Created = CreatedDate;
                return result;
            }
        }
        public List<UserInfo> GetMemberList()
        {
            using (var db = GetDB)
            {
                var result = db.GetMemberListView().Select(e => new UserInfo
                {
                    MemberId = e.MemberId,
                    MemberName = e.MemberName,
                    LoginAccount = e.LoginAccount,
                    StaffId = e.StaffId,
                    SAMAccount = e.SAMAccount,
                    SAPAccount = e.SAPAccount,
                    Post = e.Post,
                    Phone = e.Phone,
                    EmailAddress = e.EmailAddress,
                    JobLevelId = e.JobLevelId,
                    JobLevel = e.JobLevel == null ? 0 : (int)e.JobLevel,
                    Remark = e.Remark
                }).ToList();
                if (result == null)
                {
                    return new List<UserInfo>();
                }
                return result;
            }
        }
        public BaseResult RemoveMemberRole(int MemberId, int RoleId)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (var db = GetDB)
                {
                    var rms = db.RoleMembers.Where(x => x.MemberId == MemberId && x.RoleId == RoleId && !x.IsDeleted).ToList();
                    foreach (var rm in rms)
                    {
                        rm.IsDeleted = false;
                        rm.ModifiedDate = DateTime.Now;
                        var result = EntityGeneral(rm, System.Data.Entity.EntityState.Modified);
                        if (!result.IsSuccess)
                        {
                            scope.Dispose();
                            return result;
                        }
                    }
                    scope.Complete();
                    return new BaseResult() { IsSuccess = true };
                }
            }

        }
        public BaseResult RemoveMemberUnit(int MemberId, int UnitId)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (var db = GetDB)
                {
                    var ums = db.UnitMembers.Where(x => x.MemberId == MemberId && x.UnitId == UnitId && !x.IsDeleted).ToList();
                    foreach (var um in ums)
                    {
                        um.IsDeleted = true;
                        um.ModifiedDate = DateTime.Now;
                        var result = EntityGeneral(um, System.Data.Entity.EntityState.Modified);
                        if (!result.IsSuccess)
                        {
                            scope.Dispose();
                            return result;
                        }
                    }
                    scope.Complete();
                    return new BaseResult() { IsSuccess = true };
                }
            }

        }
        public BaseResult RemoveMember(int MemberId)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (var db = GetDB)
                {
                    var ms = db.Members.Where(x => x.MemberId == MemberId && x.IsActive.HasValue && x.IsActive.Value).ToList();
                    foreach (var m in ms)
                    {
                        m.IsActive = false;
                        m.LastModifiedDate = DateTime.Now;
                        var result = EntityGeneral(m, System.Data.Entity.EntityState.Modified);
                        if (!result.IsSuccess)
                        {
                            scope.Dispose();
                            return result;
                        }
                    }
                    scope.Complete();
                    return new BaseResult() { IsSuccess = true };
                }
            }

        }
        public BaseResult AddMemberUnit(int MemberId, int UnitId, int seq = -1)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (var db = GetDB)
                {
                    if (seq < 0)
                    {
                        var ums = db.UnitMembers.Where(x => x.MemberId == MemberId && x.UnitId == UnitId && !x.IsDeleted
                        ).ToList();
                        UnitMember um = new UnitMember();
                        um.UnitId = UnitId;
                        um.MemberId = MemberId;
                        um.IsDeleted = false;
                        um.Seq = ums.Count() > 0 ? ums.Max(x => x.Seq) + 1 : 1;

                        var result = EntityGeneral(um, System.Data.Entity.EntityState.Added);
                        if (!result.IsSuccess)
                        {
                            scope.Dispose();
                            return result;
                        }
                    }
                    else
                    {
                        var ums = db.UnitMembers.Where(x => x.MemberId == MemberId && x.UnitId == UnitId && !x.IsDeleted
                        && x.Seq == seq
                        ).ToList();
                        if (ums.Count() > 0)
                            return new BaseResult() { IsSuccess = false };
                        UnitMember um = new UnitMember();
                        um.UnitId = UnitId;
                        um.MemberId = MemberId;
                        um.IsDeleted = false;
                        um.Seq = seq;
                        um.CreatedBy = "";
                        um.ModifiedBy = "";
                        um.CreatedDate = DateTime.Now;
                        um.ModifiedDate = DateTime.Now;
                        var result = EntityGeneral(um, System.Data.Entity.EntityState.Added);
                        if (!result.IsSuccess)
                        {
                            scope.Dispose();
                            return result;
                        }
                    }

                }
                scope.Complete();
                return new BaseResult() { IsSuccess = true };

            }

        }
        

        public UserInfo UpdateMember(UserInfo ufl)
        {
            // var a = ufl;
            // string json = JsonConvert.SerializeObject(ufl);
            Member m = new Member();
            m.MemberId = ufl.MemberId;

            m.MemberName = ufl.MemberName;
            m.LoginAccount = ufl.LoginAccount;
            m.StaffId = ufl.StaffId;
            m.SAMAccount = ufl.SAMAccount;
            m.SAPAccount = ufl.SAPAccount;
            m.Post = ufl.Post;
            m.Phone = ufl.Phone;
            m.EmailAddress = ufl.EmailAddress;
            m.JobLevelId = ufl.JobLevelId;
            m.JobRank = ufl.JobRank;
            m.Remark = ufl.Remark;
            m.IsActive = true;
            using (TransactionScope scope = new TransactionScope())
            {
                using (var db = GetDB)
                {
                    var result = EntityGeneral(m, System.Data.Entity.EntityState.Modified);
                    if (!result.IsSuccess)
                    {
                        scope.Dispose();
                        return null;
                    }
                    var existUnit = db.UnitMembers.Where(x => x.MemberId == ufl.MemberId).ToList();
                    foreach (UnitMember eu in existUnit)
                    {
                        result = EntityGeneral(eu, System.Data.Entity.EntityState.Deleted);
                        if (!result.IsSuccess)
                        {
                            scope.Dispose();
                            return null;
                        }
                    }
                    result = AddMemberUnit(ufl.MemberId, ufl.UnitId, 1);
                    if (!result.IsSuccess)
                    {
                        scope.Dispose();
                        return null;
                    }
                    if (ufl.AllUnits != null)
                    {

                        foreach (Unit unitid in ufl.AllUnits)
                        {
                            result = AddMemberUnit(ufl.MemberId, unitid.UnitId);
                            if (!result.IsSuccess)
                            {
                                scope.Dispose();
                                return null;
                            }
                        }
                    }
                }
                scope.Complete();
                return ufl;

            }

        }
        private void GetUserAdditionalValue(UserInfo userInfo)
        {
            if (userInfo != null)
            {

                userInfo.Unit = GetUnit(userInfo.UnitId);
                userInfo.Roles = GetRolesByMemberId(userInfo.MemberId);
                userInfo.AllUnits = GetAllUnitByMemberId(userInfo.MemberId);
                List<Unit> unitlist = new List<Unit>();
                unitlist.Add(GetUnit(userInfo.UnitId));
                userInfo.BelowUnits = GetUnitsWithBelow(userInfo.UnitId,new List<Unit>());
                userInfo.BelowUnits.Add(userInfo.Unit);
            }
        }


        public List<UserInfo> GetMemberWithoutUnit()
        {
            using (var db = GetDB)
            {
                var result = db.vMembers.Where(n => !n.UnitId.HasValue)
            .ToList().Select(q => new UserInfo(q)).ToList();


                return result;
            }
        }
        //public UserInfo AddMember(UserInfo model)
        //{
        //	using (var db = GetDB)
        //	{
        //		if (model.MemberId != 0)
        //		{
        //			db.Entry(model).State = System.Data.Entity.EntityState.Modified;
        //		}
        //		else
        //		{
        //			db.Entry(model).State = System.Data.Entity.EntityState.Added;
        //		}
        //		db.SaveChanges();
        //		return model;
        //	}

        //}
        public List<Group> Getgroup()
        {
            using (var db = GetDB)
            {
                List<Group> list = new List<Group>();
                list = db.Groups.Where(e => e.IsDeleted == false).ToList();
                return list;
            }

        }
        public UserInfo CreateMember(UserInfo ufl)
        {
            Member m = new Member();
            m.MemberId = ufl.MemberId;

            m.MemberName = ufl.MemberName;
            m.LoginAccount = ufl.LoginAccount;
            m.StaffId = ufl.StaffId;
            m.SAMAccount = ufl.SAMAccount;
            m.SAPAccount = ufl.SAPAccount;
            m.Phone = ufl.Phone;
            m.EmailAddress = ufl.EmailAddress;
            m.JobLevelId = ufl.JobLevelId;
            m.JobRank = ufl.JobRank;
            m.Remark = ufl.Remark;
            m.IsActive = true;
            //using (TransactionScope scope = new TransactionScope())
            //{
            //	using (var db = GetDB)
            //	{
            //		var result = EntityGeneral(m, System.Data.Entity.EntityState.Added);

            //		if (!result.IsSuccess)
            //		{
            //			scope.Dispose();
            //			return null;
            //		}
            //	}
            //	scope.Complete();
            //	return ufl;

            //}
            using (var db = GetDB)
            {
                m = db.Members.Add(m);
                db.SaveChanges();
            }
            ufl.MemberId = m.MemberId;
            return ufl;

        }
        public Group CreateGroup(Group model)
        {
            //using (TransactionScope scope = new TransactionScope())
            //{
            using (var db = GetDB)
            {
                //var result = EntityGeneral(model, System.Data.Entity.EntityState.Added);
                //if (!result.IsSuccess)
                //{
                //	scope.Dispose();
                //	return null;
                //}
                model = db.Groups.Add(model);
                db.SaveChanges();
            }
            //scope.Complete();
            return model;

            //}
        }
        public Group UpdateGroup(Group model)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (var db = GetDB)
                {
                    var result = EntityGeneral(model, System.Data.Entity.EntityState.Modified);
                    if (!result.IsSuccess)
                    {
                        scope.Dispose();
                        return null;
                    }
                }
                scope.Complete();
                return model;

            }
        }
        public List<GroupMemberViewModel> GetGroupMemberList(int nid)
        {
            List<GroupMemberViewModel> list = new List<GroupMemberViewModel>();
            using (var db = GetDB)
            {
                list = db.GetListBpxGroupMember(nid).Select(e => new GroupMemberViewModel
                {
                    MemberId = e.MemberId,
                    MemberName = e.MemberName,
                    bExist = e.bexist,
                    groupID = nid
                }).ToList();
            }
            return list;
        }
        public List<MemberGroupViewModel> GetMemberGroupList(int nid)
        {
            List<MemberGroupViewModel> list = new List<MemberGroupViewModel>();
            using (var db = GetDB)
            {
                list = db.GetListBpxMemberGroup(nid).Select(e => new MemberGroupViewModel
                {
                    ID = e.ID,
                    Description = e.Description,
                    bExist = e.bexist,
                    MemberId = nid
                }).ToList();
            }
            return list;
        }
        public List<GroupMemberViewModel> UpdateGroupMemberList(List<GroupMemberViewModel> list)
        {
            using (var db = GetDB)
            {
                foreach (GroupMemberViewModel item in list)
                {
                    List<GroupMember> data = db.GroupMembers.Where(e => e.GroupId == item.groupID && e.MemberId == item.MemberId).ToList();
                    GroupMember model = new GroupMember();
                    if (item.bExist.ToLower() == "false")
                    {
                        if (data.Count() > 0)
                        {
                            model = data[0];
                            model.IsDeleted = true;
                            db.Entry(model).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    else
                    {

                        if (data.Count() > 0)
                        {
                            model = data[0];
                            model.IsDeleted = false;
                            db.Entry(model).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            model.GroupId = item.groupID;
                            model.MemberId = item.MemberId;
                            model.IsDeleted = false;
                            model = db.GroupMembers.Add(model);
                            db.SaveChanges();
                        }
                    }
                }
            }
            return list;
        }
        public List<MemberGroupViewModel> UpdateMemberGroupList(List<MemberGroupViewModel> list)
        {
            using (var db = GetDB)
            {
                foreach (MemberGroupViewModel item in list)
                {
                    List<GroupMember> data = db.GroupMembers.Where(e => e.GroupId == item.ID && e.MemberId == item.MemberId).ToList();
                    GroupMember model = new GroupMember();
                    if (item.bExist.ToLower() == "false")
                    {
                        if (data.Count() > 0)
                        {
                            model = data[0];
                            model.IsDeleted = true;
                            db.Entry(model).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    else
                    {

                        if (data.Count() > 0)
                        {
                            model = data[0];
                            model.IsDeleted = false;
                            db.Entry(model).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            model.GroupId = item.ID;
                            model.MemberId = item.MemberId;
                            model.IsDeleted = false;
                            model = db.GroupMembers.Add(model);
                            db.SaveChanges();
                        }
                    }
                }
            }
            return list;
        }
        public List<GroupRightViewModel> GetGroupRightList(int nid)
        {
            List<GroupRightViewModel> list = new List<GroupRightViewModel>();
            using (var db = GetDB)
            {
                list = db.GetListBpxGroupRight(nid).Select(e => new GroupRightViewModel
                {
                    ID = e.ID,
                    RightName = e.RightName,
                    bExist = e.bexist,
                    groupID = nid
                }).ToList();
            }
            return list;
        }
        public List<RightGroupViewModel> GetRightGroupList(int nid)
        {
            List<RightGroupViewModel> list = new List<RightGroupViewModel>();
            using (var db = GetDB)
            {
                list = db.GetListBpxRightGroup(nid).Select(e => new RightGroupViewModel
                {
                    ID = e.ID,
                    GroupName = e.GroupName,
                    bExist = e.bexist,
                    RightID = nid
                }).ToList();
            }
            return list;
        }
        public List<GroupRightViewModel> UpdateGroupRightList(List<GroupRightViewModel> list)
        {
            using (var db = GetDB)
            {
                foreach (GroupRightViewModel item in list)
                {
                    List<GroupRight> data = db.GroupRights.Where(e => e.GroupID == item.groupID && e.RightID == item.ID).ToList();
                    GroupRight model = new GroupRight();
                    if (item.bExist.ToLower() == "false")
                    {
                        if (data.Count() > 0)
                        {
                            model = data[0];
                            model.IsDeleted = true;
                            db.Entry(model).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    else
                    {

                        if (data.Count() > 0)
                        {
                            model = data[0];
                            model.IsDeleted = false;
                            db.Entry(model).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            model.GroupID = item.groupID;
                            model.RightID = item.ID;
                            model.IsDeleted = false;
                            model = db.GroupRights.Add(model);
                            db.SaveChanges();
                        }
                    }
                }
            }
            return list;
        }
        public List<RightGroupViewModel> UpdateRightGroupList(List<RightGroupViewModel> list)
        {
            using (var db = GetDB)
            {
                foreach (RightGroupViewModel item in list)
                {
                    List<GroupRight> data = db.GroupRights.Where(e => e.GroupID == item.ID && e.RightID == item.RightID).ToList();
                    GroupRight model = new GroupRight();
                    if (item.bExist.ToLower() == "false")
                    {
                        if (data.Count() > 0)
                        {
                            model = data[0];
                            model.IsDeleted = true;
                            db.Entry(model).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    else
                    {

                        if (data.Count() > 0)
                        {
                            model = data[0];
                            model.IsDeleted = false;
                            db.Entry(model).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            model.GroupID = item.ID;
                            model.RightID = item.RightID;
                            model.IsDeleted = false;
                            model = db.GroupRights.Add(model);
                            db.SaveChanges();
                        }
                    }
                }
            }
            return list;
        }
        public List<GetRightView_Result> GetRight()
        {
            using (var db = GetDB)
            {
                List<GetRightView_Result> list = new List<GetRightView_Result>();
                list = db.GetRightView().Where(e => e.IsDeleted == false).ToList();
                return list;
            }

        }
        public Right CreateRight(Right model)
        {
            //using (TransactionScope scope = new TransactionScope())
            //{
            using (var db = GetDB)
            {
                //var result = EntityGeneral(model, System.Data.Entity.EntityState.Added);
                //if (!result.IsSuccess)
                //{
                //	scope.Dispose();
                //	return null;
                //}
                model = db.Rights.Add(model);
                db.SaveChanges();

            }
            //scope.Complete();
            return model;

            //}
        }
        public Right UpdateRight(Right model)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (var db = GetDB)
                {
                    var result = EntityGeneral(model, System.Data.Entity.EntityState.Modified);
                    if (!result.IsSuccess)
                    {
                        scope.Dispose();
                        return null;
                    }
                }
                scope.Complete();
                return model;

            }
        }
        public List<Application> GetApplication()
        {
            using (var db = GetDB)
            {
                List<Application> list = new List<Application>();
                list = db.Applications.Where(e => e.IsDeleted == false).ToList();
                return list;
            }

        }
        public List<GetApprovalMartixView_Result> GetApprovalMartix()
        {
            using (var db = GetDB)
            {
                List<GetApprovalMartixView_Result> list = new List<GetApprovalMartixView_Result>();
                list = db.GetApprovalMartixView().Where(e => e.IsDeleted == false).ToList();
                return list;
            }

        }
        public List<GetApprovalMartixDetailView_Result> GetApprovalMartixDetail()
        {
            using (var db = GetDB)
            {
                List<GetApprovalMartixDetailView_Result> list = new List<GetApprovalMartixDetailView_Result>();
                list = db.GetApprovalMartixDetailView().Where(e => e.IsDeleted == false).ToList();
                return list;
            }

        }
        public ApprovalMartix CreateApprovalMartix(ApprovalMartix model)
        {
            //using (TransactionScope scope = new TransactionScope())
            //{
            using (var db = GetDB)
            {
                //var result = EntityGeneral(model, System.Data.Entity.EntityState.Added);
                //if (!result.IsSuccess)
                //{
                //	scope.Dispose();
                //	return null;
                //}
                model = db.ApprovalMartixes.Add(model);
                db.SaveChanges();
            }
            //scope.Complete();
            return model;

            //}
        }
        public ApprovalMartixDetail CreateApprovalMartixDetail(ApprovalMartixDetail model)
        {
            //using (TransactionScope scope = new TransactionScope())
            //{
            using (var db = GetDB)
            {
                //var result = EntityGeneral(model, System.Data.Entity.EntityState.Added);
                //if (!result.IsSuccess)
                //{
                //	scope.Dispose();
                //	return null;
                //}
                model = db.ApprovalMartixDetails.Add(model);
                db.SaveChanges();
            }
            //scope.Complete();
            return model;

            //}
        }
        public ApprovalMartix UpdateApprovalMartix(ApprovalMartix model)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (var db = GetDB)
                {
                    var result = EntityGeneral(model, System.Data.Entity.EntityState.Modified);
                    if (!result.IsSuccess)
                    {
                        scope.Dispose();
                        return null;
                    }
                }
                scope.Complete();
                return model;

            }
        }
        public ApprovalMartixDetail UpdateApprovalMartixDetail(ApprovalMartixDetail model)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (var db = GetDB)
                {
                    var result = EntityGeneral(model, System.Data.Entity.EntityState.Modified);
                    if (!result.IsSuccess)
                    {
                        scope.Dispose();
                        return null;
                    }
                }
                scope.Complete();
                return model;

            }
        }
        public List<ProcessList> GetProcessList()
        {
            using (var db = GetDB)
            {
                List<ProcessList> list = new List<ProcessList>();
                list = db.ProcessLists.ToList();
                return list;
            }
        }
        public List<Unit> GetUnitList()
        {
            using (var db = GetDB)
            {
                List<Unit> list = new List<Unit>();
                list = db.Units.Where(e => e.IsDeleted == false).ToList();
                return list;
            }
        }
        public List<Role> GetRoleList()
        {
            using (var db = GetDB)
            {
                List<Role> list = new List<Role>();
                list = db.Roles.Where(e => e.IsDeleted == false).ToList();
                return list;
            }
        }
        public List<ApprovalMartixOption> GetApprovalMartixOption()
        {
            using (var db = GetDB)
            {
                List<ApprovalMartixOption> list = new List<ApprovalMartixOption>();
                list = db.ApprovalMartixOptions.Where(e => e.IsDeleted == false).ToList();
                return list;
            }
        }
        public List<JobLevel> GetJobLeveln()
        {
            using (var db = GetDB)
            {
                List<JobLevel> list = new List<JobLevel>();
                list = db.JobLevels.Where(e => e.IsDeleted == false).ToList();
                return list;
            }
        }
        public List<GetProcessOptionByApprovalMartixOption_Result> GetProcessOption()
        {
            using (var db = GetDB)
            {
                List<GetProcessOptionByApprovalMartixOption_Result> list = new List<GetProcessOptionByApprovalMartixOption_Result>();
                list = db.GetProcessOptionByApprovalMartixOption().ToList();
                return list;
            }
        }
        public List<ApprovalLevel> GetApprovalLevel()
        {
            using (var db = GetDB)
            {
                List<ApprovalLevel> list = new List<ApprovalLevel>();
                list = db.ApprovalLevels.ToList();
                return list;
            }
        }
        public List<ApprovalCriteriaType> GetApprovalCriteriaType()
        {
            using (var db = GetDB)
            {
                List<ApprovalCriteriaType> list = new List<ApprovalCriteriaType>();
                list = db.ApprovalCriteriaTypes.Where(e => e.IsDeleted == false).ToList();
                return list;
            }
        }

        public List<CostCenter> GetCostCenter()
        {
            using (var db = GetDB)
            {
                List<CostCenter> list = new List<CostCenter>();
                list = db.CostCenters.Where(e => e.IsDeleted == false).ToList();
                return list;
            }

        }
        public CostCenter CreateCostCenter(CostCenter model)
        {
            //using (TransactionScope scope = new TransactionScope())
            //{
            using (var db = GetDB)
            {
                //var result = EntityGeneral(model, System.Data.Entity.EntityState.Added);
                //if (!result.IsSuccess)
                //{
                //	scope.Dispose();
                //	return null;
                //}
                model = db.CostCenters.Add(model);
                db.SaveChanges();

            }
            //scope.Complete();
            return model;

            //}
        }
        public CostCenter UpdateCostCenter(CostCenter model)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (var db = GetDB)
                {
                    var result = EntityGeneral(model, System.Data.Entity.EntityState.Modified);
                    if (!result.IsSuccess)
                    {
                        scope.Dispose();
                        return null;
                    }
                }
                scope.Complete();
                return model;

            }
        }

        public List<UnitCostCenterViewModel> GetUnitCostCenterList(int nid)
        {
            List<UnitCostCenterViewModel> list = new List<UnitCostCenterViewModel>();
            using (var db = GetDB)
            {
                list = db.GetListBpxUnitCostCenter(nid).Select(e => new UnitCostCenterViewModel
                {
                    UnitId = e.UnitId,
                    UnitName = e.UnitName,
                    bExist = e.bexist,
                    CostCenterId = nid
                }).ToList();
            }
            return list;
        }
        //public List<UnitCostCenterViewModel> UpdateUnitCostCenterList(List<UnitCostCenterViewModel> list)
        //{
        //	using (var db = GetDB)
        //	{
        //		foreach (UnitCostCenterViewModel item in list)
        //		{
        //			List<UnitCostCenter> data = db.UnitCostCenters.Where(e => e.UnitId == item.UnitId && e.CostCenterId == item.CostCenterId).ToList();
        //			UnitCostCenter model = new UnitCostCenter();
        //			if (item.bExist.ToLower() == "false")
        //			{
        //				if (data.Count() > 0)
        //				{
        //					model = data[0];
        //					model. = true;
        //					db.Entry(model).State = EntityState.Modified;
        //					db.SaveChanges();
        //				}
        //			}
        //			else
        //			{

        //				if (data.Count() > 0)
        //				{
        //					model = data[0];
        //					model.IsDeleted = false;
        //					db.Entry(model).State = EntityState.Modified;
        //					db.SaveChanges();
        //				}
        //				else
        //				{
        //					model.GroupId = item.ID;
        //					model.MemberId = item.MemberId;
        //					model.IsDeleted = false;
        //					model = db.GroupMembers.Add(model);
        //					db.SaveChanges();
        //				}
        //			}
        //		}
        //	}
        //	return list;
        //}
        public List<Role> GetRole()
        {
            using (var db = GetDB)
            {
                List<Role> list = new List<Role>();
                list = db.Roles.Where(e => e.IsDeleted == false).ToList();
                return list;
            }

        }
        public Role CreateRole(Role model)
        {
            //using (TransactionScope scope = new TransactionScope())
            //{
            using (var db = GetDB)
            {
                //var result = EntityGeneral(model, System.Data.Entity.EntityState.Added);
                //if (!result.IsSuccess)
                //{
                //	scope.Dispose();
                //	return null;
                //}
                model = db.Roles.Add(model);
                db.SaveChanges();

            }
            //scope.Complete();
            return model;

            //}
        }
        public Role UpdateRole(Role model)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (var db = GetDB)
                {
                    var result = EntityGeneral(model, System.Data.Entity.EntityState.Modified);
                    if (!result.IsSuccess)
                    {
                        scope.Dispose();
                        return null;
                    }
                }
                scope.Complete();
                return model;

            }
        }
        public List<RoleMemberViewModel> GetRoleMemberList(int nid)
        {
            List<RoleMemberViewModel> list = new List<RoleMemberViewModel>();
            using (var db = GetDB)
            {
                list = db.GetListBpxRoleMember(nid).Select(e => new RoleMemberViewModel
                {
                    MemberId = e.MemberId,
                    MemberName = e.MemberName,
                    bExist = e.bexist,
                    RoleId = nid
                }).ToList();
            }
            return list;
        }
        public List<RoleMemberViewModel> UpdateRoleMemberList(List<RoleMemberViewModel> list)
        {
            using (var db = GetDB)
            {
                foreach (RoleMemberViewModel item in list)
                {
                    List<RoleMember> data = db.RoleMembers.Where(e => e.RoleId == item.RoleId && e.MemberId == item.MemberId).ToList();
                    RoleMember model = new RoleMember();
                    if (item.bExist.ToLower() == "false")
                    {
                        if (data.Count() > 0)
                        {
                            model = data[0];
                            model.IsDeleted = true;
                            db.Entry(model).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    else
                    {

                        if (data.Count() > 0)
                        {
                            model = data[0];
                            model.IsDeleted = false;
                            db.Entry(model).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            model.RoleId = item.RoleId;
                            model.MemberId = item.MemberId;
                            model.IsDeleted = false;
                            model = db.RoleMembers.Add(model);
                            db.SaveChanges();
                        }
                    }
                }
            }
            return list;
        }
        public List<GetSiteMenuView_Result> GetSiteMenu()
        {
            using (var db = GetDB)
            {
                List<GetSiteMenuView_Result> list = new List<GetSiteMenuView_Result>();
                list = db.GetSiteMenuView().Where(e => e.IsDeleted == false).ToList();
                return list;
            }

        }
        public SiteMenu CreateSiteMenu(SiteMenu model)
        {
            using (var db = GetDB)
            {

                model = db.SiteMenus.Add(model);
                db.SaveChanges();

            }
            return model;

        }
        public SiteMenu UpdateSiteMenu(SiteMenu model)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (var db = GetDB)
                {
                    var result = EntityGeneral(model, System.Data.Entity.EntityState.Modified);
                    if (!result.IsSuccess)
                    {
                        scope.Dispose();
                        return null;
                    }
                }
                scope.Complete();
                return model;

            }
        }
        public List<SiteMenuRightViewModel> GetListBpxSiteMenuRight(int nid)
        {
            List<SiteMenuRightViewModel> list = new List<SiteMenuRightViewModel>();
            using (var db = GetDB)
            {
                list = db.GetListBpxSiteMenuRight(nid).Select(e => new SiteMenuRightViewModel
                {
                    ID = e.ID,
                    RightName = e.RightName,
                    bExist = e.bexist,
                    SiteMenuID = nid
                }).ToList();
            }
            return list;
        }
        public List<SiteMenuRightViewModel> UpdateSiteMenuRightList(List<SiteMenuRightViewModel> list)
        {
            using (var db = GetDB)
            {
                foreach (SiteMenuRightViewModel item in list)
                {
                    List<SiteMenuRight> data = db.SiteMenuRights.Where(e => e.SiteMenuID == item.SiteMenuID && e.RightID == item.ID).ToList();
                    SiteMenuRight model = new SiteMenuRight();
                    if (item.bExist.ToLower() == "false")
                    {
                        if (data.Count() > 0)
                        {
                            model = data[0];
                            model.IsDeleted = true;
                            db.Entry(model).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    else
                    {

                        if (data.Count() > 0)
                        {
                            model = data[0];
                            model.IsDeleted = false;
                            db.Entry(model).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            model.RightID = item.ID;
                            model.SiteMenuID = item.SiteMenuID;
                            model.IsDeleted = false;
                            model = db.SiteMenuRights.Add(model);
                            db.SaveChanges();
                        }
                    }
                }
            }
            return list;
        }

        public List<SystemParameter> GetSystemParameterList()
        {
            List<SystemParameter> list = new List<SystemParameter>();
            using (var db = GetDB)
            {
                list = db.SystemParameters.ToList();
            }
            return list;
        }

        public SystemParameter UpdateSystemParameter(SystemParameter model)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (var db = GetDB)
                {
                    var result = EntityGeneral(model, System.Data.Entity.EntityState.Modified);
                    if (!result.IsSuccess)
                    {
                        scope.Dispose();
                        return null;
                    }
                }
                scope.Complete();
                return model;

            }
        }

        //public List<MemberRoleViewModel> GetMemberRoleList(int nid)
        //{
        //	List<RoleMemberViewModel> list = new List<RoleMemberViewModel>();
        //	using (var db = GetDB)
        //	{
        //		list = db.GetListBpxRoleMember(nid).Select(e => new RoleMemberViewModel
        //		{
        //			MemberId = e.MemberId,
        //			MemberName = e.MemberName,
        //			bExist = e.bexist,
        //			RoleId = nid
        //		}).ToList();
        //	}
        //	return list;
        //}
        //public List<RoleMemberViewModel> UpdateRoleMemberList(List<RoleMemberViewModel> list)
        //{
        //	using (var db = GetDB)
        //	{
        //		foreach (RoleMemberViewModel item in list)
        //		{
        //			List<RoleMember> data = db.RoleMembers.Where(e => e.RoleId == item.RoleId && e.MemberId == item.MemberId).ToList();
        //			RoleMember model = new RoleMember();
        //			if (item.bExist.ToLower() == "false")
        //			{
        //				if (data.Count() > 0)
        //				{
        //					model = data[0];
        //					model.IsDeleted = true;
        //					db.Entry(model).State = EntityState.Modified;
        //					db.SaveChanges();
        //				}
        //			}
        //			else
        //			{

        //				if (data.Count() > 0)
        //				{
        //					model = data[0];
        //					model.IsDeleted = false;
        //					db.Entry(model).State = EntityState.Modified;
        //					db.SaveChanges();
        //				}
        //				else
        //				{
        //					model.RoleId = item.RoleId;
        //					model.MemberId = item.MemberId;
        //					model.IsDeleted = false;
        //					model = db.RoleMembers.Add(model);
        //					db.SaveChanges();
        //				}
        //			}
        //		}
        //	}
        //	return list;
        //}
        public List<vDivision> GetvDivision()
        {
            List<vDivision> list = new List<vDivision>();
            using (var db = GetDB)
            {
                list = db.vDivisions.ToList();
            }
            return list;
        }
    }
}
