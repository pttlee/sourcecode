﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEHK.DAL.DTO;
using MEHK.DAL.Enum;
using MEHK.DAL.Model;
using MEHK.DAL.ViewModel;

namespace MEHK.BLL
{
    public partial class OrganizationBLL : BaseBLL
    {

        public List<CostCenter> GetCostCenters()
        {
            return GetDB.CostCenters.Where(x => !x.IsDeleted).ToList();
        }
        public CostCenter GetUnitCostCenter(int CostCenterId)
        {
            return GetDB.CostCenters.Where(x => !x.IsDeleted &&  x.CostCenterId == CostCenterId).FirstOrDefault();
        }

        public List<CostCenter> GetUnitCostCenters(int UnitId)
        {
            var UnitCostCenters = GetDB.UnitCostCenters.Where(x => x.UnitId == UnitId).ToList();
            if (UnitCostCenters.Count()>0)
                return UnitCostCenters.Select(y=> GetUnitCostCenter(y.CostCenterId)).ToList();
            return new List<CostCenter>();
        }




    }
}
