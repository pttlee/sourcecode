﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEHK.DAL.DTO;
using MEHK.DAL.Enum;
using MEHK.DAL.Model;
using MEHK.DAL.ViewModel;

namespace MEHK.BLL
{
    public partial class OrganizationBLL : BaseBLL
    {

        //public ApprovalMartixBLL()
        //{


        //}

        public List<ApprovalMartix> GetApprovalMartix(string ProcessCode, int MemberId, string ApprovalMartixOptionCode)
        {
            var ProcessList = GetDB.ProcessLists.ToList();
            if (!string.IsNullOrEmpty(ProcessCode))
                ProcessList = ProcessList.Where(x => x.Code == ProcessCode).ToList();
            var ProcessIdList = ProcessList.Select(x => x.ProcessId).ToList();
            //List<int> UnitIds = new List<int>();
            if (MemberId > 0)
            {
                var member = GetMember(MemberId);
                var UnitsQueue = GetUnitsWithAbove(member.UnitId);
                foreach (var queue in UnitsQueue)
                {
                    var UnitApprovalMartixes = GetUnitApprovalMartix(queue.UnitId).Where(x => ProcessIdList.Contains(x.ProcessId)).ToList();
                    var FilterByCode = UnitApprovalMartixes.Where(x => !string.IsNullOrEmpty(x.ApprovalMartixOptionCode) && x.ApprovalMartixOptionCode == ApprovalMartixOptionCode).ToList();
                    if (!string.IsNullOrEmpty(ApprovalMartixOptionCode) && FilterByCode.Count() > 0)
                        return FilterByCode;
                    else if (string.IsNullOrEmpty(ApprovalMartixOptionCode) && UnitApprovalMartixes != null && UnitApprovalMartixes.Count() > 0)
                        return UnitApprovalMartixes;
                    //else if (!string.IsNullOrEmpty(ApprovalMartixOptionCode)
                    //    return 
                }

            }
            var ApprovalMartixes = GetDB.ApprovalMartixes.Where(x => !x.IsDeleted && ProcessIdList.Contains(x.ProcessId)).ToList();
            if (!string.IsNullOrEmpty(ApprovalMartixOptionCode))
                ApprovalMartixes = ApprovalMartixes.Where(x => x.ApprovalMartixOptionCode == ApprovalMartixOptionCode).ToList();
            if (ApprovalMartixes == null || ApprovalMartixes.Count() == 0)
                return new List<ApprovalMartix>();
            return ApprovalMartixes;

        }

        public ApprovalMartix GetApprovalMartix(int ApprovalMartixId)
        {
            return GetDB.ApprovalMartixes.Where(x => !x.IsDeleted && x.ApprovalMartixId == ApprovalMartixId).FirstOrDefault();
        }
        public List<ApprovalMartixDetail> GetApprovalMartixDetails(int ApprovalMartixId = -1)
        {
            if (ApprovalMartixId < 0)
                return GetDB.ApprovalMartixDetails.ToList();
            return GetDB.ApprovalMartixDetails.Where(x => !x.IsDeleted && x.ApprovalMartixId == ApprovalMartixId).ToList();
        }
        public List<ApprovalLevel> GetApprovalLevels(int ApprovalLevelId = -1)
        {
            if (ApprovalLevelId < 0)
                return GetDB.ApprovalLevels.ToList();
            return GetDB.ApprovalLevels.Where(x => x.ApprovalLevelId == ApprovalLevelId).ToList();
        }
        public List<ApprovalMartix> GetUnitApprovalMartix(int UnitId = -1)
        {
            if (UnitId > 0)
                return GetDB.ApprovalMartixes.Where(x => !x.IsDeleted && (x.UnitId.HasValue && x.UnitId == UnitId)).ToList();
            else
                return GetDB.ApprovalMartixes.Where(x => !x.IsDeleted).ToList();
        }
        //public List<ApprovalMartix> GetUnitApprovalMartixByUnitId(int UnitId)
        //{
        //    return GetDB.ApprovalMartixes.Where(x => !x.IsDeleted && ( x.UnitId.HasValue && x.UnitId == UnitId)).ToList();
        //}
        public List<string> GetApprover(int MemberId, string ApprovalCriteriaType, string ApprovalCriteria)
        {


            List<string> result = new List<string>();
            int iApprovalCriteria = -999;
            int.TryParse(ApprovalCriteria, out iApprovalCriteria);
            if (iApprovalCriteria == -999)
                return result;
            using (var db = GetDB)
            {
                var SearchMethod = db.ApprovalCriteriaTypes.Where(x => !x.IsDeleted && x.ApprovalCriteriaType1 == ApprovalCriteriaType).FirstOrDefault();
                if (SearchMethod == null || string.IsNullOrEmpty(SearchMethod.CompareLogic))
                    return result;

                var ComparingResult = db.Database.SqlQuery<string>
                    (string.Format("exec {0} {1},{2}",
                   SearchMethod.CompareLogic, ApprovalCriteria, MemberId));
                if (ComparingResult != null && ComparingResult.Count() > 0)
                    result.AddRange(ComparingResult.ToList());

                //var Search = SearchMethod.KeyField.Split('.');
                //if (Search.Count()<3)
                //    return result;
                //string TableNamefromKeyField = Search[1];
                //string KeyField = Search[2];
                //string CompareField = string.IsNullOrEmpty(SearchMethod.CompareField)?SearchMethod.KeyField: SearchMethod.CompareField;
                //var Compare = CompareField.Split('.');
                //CompareField = Compare[2];

                //string ComparingResult = db.Database.SqlQuery<string>
                //    (string.Format("Select {0} from {1} where {2}={3} and ",
                //    CompareField, TableNamefromKeyField, KeyField,ApprovalCriteria))
                //            .FirstOrDefault();
            }

            return result;
        }

        public string GetADFullListName(List<string> approvers, ref List<ApproversDetails> approversDetails)
        {
            string FullListName = "";
            MEHK.BLL.CommonBLL WFCommonBLL = new MEHK.BLL.CommonBLL();
            var _loginDomain = WFCommonBLL.GetSystemParameter("loginDomain");
            foreach (var p in approvers)
            {
                FullListName = FullListName + _loginDomain + "\\" + p + ";";
                ApproversDetails d = new ApproversDetails();
                d.ADName = _loginDomain + "\\" + p;
                d.DisplayName = GetMemberDisplayName(p);
                approversDetails.Add(d);
            }
            return FullListName.Length > 0 ? FullListName.Substring(0, FullListName.Length - 1) : "";
        }

        public string GetMemberDisplayName(string ADName)
        {
            string DisplayName = "";
            using (var db = GetDB)
            {
                var result = db.vMembers.Where(q => (q.SAMAccount.ToLower() == ADName.ToLower() && (q.IsActive.HasValue && q.IsActive.Value))
                    ).FirstOrDefault();
                if (result != null)
                {
                    DisplayName = result.MemberName;
                }
            }
            return DisplayName;
        }
    }
}