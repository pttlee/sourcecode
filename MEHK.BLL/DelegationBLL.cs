﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEHK.DAL.DTO;
using MEHK.DAL.Enum;
using MEHK.DAL.Model;
using MEHK.DAL.ViewModel;

namespace MEHK.BLL
{
    public partial class OrganizationBLL : BaseBLL
    {

        public List<Deputy> GetDelegationList()
        {
            using (var db = GetDB)
            {
                var DeputiesList = db.Deputies.Where(x => !x.IsDeleted).ToList();
                
                return DeputiesList;
            }
               
        }
       


    }
}
