﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEHK.DAL.DTO;
using MEHK.DAL.Model;

namespace MEHK.BLL
{
	public class EmailBLL : BaseBLL
	{

		public EmailBLL()
		{


		}


		private OrganizationBLL OrganizationMain
		{
			get
			{
				return new OrganizationBLL();
			}
		}


		public List<EmailNotificationTask> GetEmailTasks()
		{
			using (var db = GetDB)
			{
				var result = db.EmailNotificationTasks.Include("ProcessActivityGroup").Where(q => !q.IsEmailed).ToList();
				return result;
			}
		}

		public List<EmailTemplate> GetEmailTemplateList()
		{
			using (var db = GetDB)
			{
				var result = db.EmailTemplates.ToList();
				return result;
			}
		}


		public List<ProcessActivityGroup> GetProcessActivityGroupList()
		{
			using (var db = GetDB)
			{
				var result = db.ProcessActivityGroups.ToList();
				return result;
			}
		}

		public EmailTemplate GetEmailTemplate(int id)
		{
			using (var db = GetDB)
			{
				var result = db.EmailTemplates.Where(q => q.ActivityGroupID == id).FirstOrDefault();
				return result;
			}
		}

		public BaseResult UpdateEmailTask(int id)
		{
			BaseResult result = new BaseResult();
			EmailNotificationTask updateTarget = null;
			using (var db = GetDB)
			{
				updateTarget = db.EmailNotificationTasks.Where(q => q.EmailNotificationTasksID == id).FirstOrDefault();
			}
			if (updateTarget != null)
			{
				updateTarget.IsEmailed = true;
				result = EntityGeneral(updateTarget, System.Data.Entity.EntityState.Modified);
			}
			return result;

		}

        public EmailNotificationTask GetEmailTask(int id)
        {
            using (var db = GetDB)
            {
                var result = db.EmailNotificationTasks.Where(q => q.EmailNotificationTasksID == id).FirstOrDefault();
                return result;
            }

        }



        public BaseResult AddEmailLog(EmailNotificationLogDTO dto)
		{
			using (var db = GetDB)
			{
				EmailNotificationLog addItme = new EmailNotificationLog(dto);

				var result = EntityGeneral(addItme, System.Data.Entity.EntityState.Added);
				return result;
			}
		}
	}
}
