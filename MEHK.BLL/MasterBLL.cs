﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEHK.DAL.Model;

namespace MEHK.BLL
{
	public class MasterBLL : BaseBLL
	{

		public ProcessList GetProcess(string code)
		{
			using (var DB = GetDB)
			{
				var result = DB.ProcessLists.Where(q => q.Code == code).FirstOrDefault();
				return result;
			}
		}
	}
}
