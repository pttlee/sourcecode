﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.ExceptionHandling;

namespace MEHK.CommonAPI
{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'UnhandledExceptionLogger'
	public class UnhandledExceptionLogger : ExceptionLogger
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'UnhandledExceptionLogger'
	{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'UnhandledExceptionLogger.Log(ExceptionLoggerContext)'
		public override void Log(ExceptionLoggerContext context)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'UnhandledExceptionLogger.Log(ExceptionLoggerContext)'
		{

			var log = context.Exception.ToString();
			//Write the exception to your logs
			MEHK.BLL.CommonBLL CommonMain = new MEHK.BLL.CommonBLL();
			CommonMain.CreateErrorLog(context.Exception, "CommonAPI");
		}

	}
}