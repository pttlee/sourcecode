﻿var beforeunloadListener;
var hasFirstSetBeforeUnloadEvent = false;




function InitListener() {

    if (!hasFirstSetBeforeUnloadEvent) {
        beforeunloadListener = function (e) {
            var confirmationMessage = 'Are you sure you want to leave this page?';

            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
            return confirmationMessage;                            //Webkit, Safari, Chrome
        }

        window.addEventListener("beforeunload", beforeunloadListener, false);
        hasFirstSetBeforeUnloadEvent = true;
    }

}

function RemoveWindowOnBeforeUnloadListener() {
    //$(window).off("beforeunload");

    //console.log('RemoveWindowOnBeforeUnloadListener');

    if (typeof beforeunloadListener != 'undefined') {

        window.removeEventListener("beforeunload", beforeunloadListener, false);
    }

}

function RemoveLastDirectoryPartOf(the_url) {
    var the_arr = the_url.split('/');
    the_arr.pop();
    return (the_arr.join('/'));
}

function findRightMenuElementByURL(Menu, URL) {
    var Element = [];

    for (var i = 0; i < Menu.Pages.length; i++) {
        var hasSubMenu = false;

        if (Menu.Pages[i].Pages.length > 0) {
            hasSubMenu = true;
        }

        if (Menu.Pages[i].URL) {
            //if (Menu.Pages[i].URL.includes(URL + '/')) {
            //    Element.push(Menu.Pages[i]);
            //}

            if (RemoveLastDirectoryPartOf(Menu.Pages[i].URL).replace('../..', '') == URL && !Menu.Pages[i].URL.includes('/Web/Home/')) {
                Element.push(Menu.Pages[i]);
            }
        }

        if (hasSubMenu) {
            Array.prototype.push.apply(Element, findRightMenuElementByURL(Menu.Pages[i], URL));
        }
    }

    return Element;
}

function getRightMenuElement(Menu) {
    var Element = '';

    /*for (var i = 0; i < Menu.length; i++) {
        Element += '<li class="m-nav__item">' +
                        '<div class="m-nav__link" style="padding:0 10px">' +
                            '<a href="' + Menu[i].URL + '" class="m-nav__link-text" style="text-decoration: none;">' +
                                Menu[i].MenuDisplayName +
                            '</a>';
							
									
        Element +=  '</div>';				
        Element +=  '</li>';
    }*/
	
	Element += '<div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark ">';
	
	Element += '<ul class="m-menu__nav  m-menu__nav--submenu-arrow ">';
	
	
	for (var i = 0; i < Menu.length; i++) {
		
		
	    if (Menu[i].Pages.length == 0) {
	        Element += '<li class="m-menu__item m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true">	<a href="' + Menu[i].URL + '" class="m-menu__link" title=""><span class="m-menu__link-text">' + Menu[i].MenuDisplayName + '</span></a></li>';

	    }
	    else {
	        Element += '<li class="m-menu__item m-menu__item--submenu m-menu__item--rel toggleRightMenu" m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true">' +
            '<a href="javascript:;" class="m-menu__link m-menu__toggle" title="">' +
            '<span class="m-menu__link-text">' + Menu[i].MenuDisplayName + '</span>' +
            '<i class="m-menu__hor-arrow la la-angle-down"></i>' +
    '<i class="m-menu__ver-arrow la la-angle-right"></i></a>' +
    '<div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left">' +
    '<span class="m-menu__arrow m-menu__arrow--adjust" style="left: 71px;"></span>' +
    '<ul class="m-menu__subnav">';


	        for (j = 0 ; j < Menu[i].Pages.length; j++) {
	            Element += '<li class="m-menu__item " aria-haspopup="true">';

	            Element += '<a href="' + Menu[i].Pages[j].URL + '" class="m-menu__link "><span class="m-menu__link-text">' + Menu[i].Pages[j].MenuDisplayName + '</span></a>';

	            Element += '</li>';
	        }

	        Element += '</ul></div></li>';

	    }
		
		

    }
	
	Element += '</ul>';
	
	Element += '</div>';

    return Element;
	
	
}

function getLeftMenuElement(Menu) {
    var Element = '';

    for (var i = 0; i < Menu.Pages.length; i++) {
        var hasSubMenu = false;

        if (Menu.Pages[i].Pages.length > 0) {
            hasSubMenu = true;
        }

        if (hasSubMenu) {
            Element += '<li class="m-menu__item " aria-haspopup="true" data-menu-submenu-toggle="click" data-menu-submenu-mode="accordion">';

            Element += '<a href="#" class="m-menu__link m-menu__toggle">' +
                            '<span class="m-menu__link-text">' +
                                Menu.Pages[i].MenuDisplayName +
                            '</span>' +
                            '<i class="m-menu__ver-arrow la la-angle-right"></i>' +
                       '</a>';
        }
        else {
            Element += '<li class="m-menu__item " aria-haspopup="true" data-redirect="true">';

            Element += '<a href="' + Menu.Pages[i].URL + '" class="m-menu__link">' +
                            '<span class="m-menu__link-text">' +
                                Menu.Pages[i].MenuDisplayName +
                            '</span>' +
                       '</a>';
        }

        if (hasSubMenu) {
            Element += '<div class="m-menu__submenu ">';
            Element += '<span class="m-menu__arrow"></span>';
            Element += '<ul class="m-menu__subnav">';
            Element += getLeftMenuElement(Menu.Pages[i]);
            Element += '</ul>';
            Element += '</div>';
        }

        Element += '</i>'
    }

    return Element;
}

function InitMenu(UserID, AppID) {
    var data = { userID: UserID, appID: AppID }

    $.getJSON('/MEHK.CommonAPI/api/Common/GetUserMenuPage', data).done(function (data) {
        $("#LeftMenu").append(getLeftMenuElement(data));
        var url = RemoveLastDirectoryPartOf(window.location.pathname);

        //if (url) {
        //	var rightmenuitems = findRightMenuElementByURL(data, url);

        //	if (rightmenuitems.length > 1) {
        //		$("#divRightMenu").show();
        //	}
        //	else {
        //		$("#divRightMenu").hide();
        //	}

        //	$("#RightMenu").append(getRightMenuElement(rightmenuitems));
        //}
    });
}


function InitMenuRight(UserID, ProcessID) {
    var data = { userID: UserID, processID: ProcessID }

    $.getJSON('/MEHK.CommonAPI/api/Common/GetUserMenuPageByProcessID', data).done(function (data) {

        var menuData = data.Pages;

        if (menuData.length > 0) {
            $("#divRightMenu").show();
            $("#RightMenu").append(getRightMenuElement(menuData[0].Pages));
			
			$(function(){
	
				$(".toggleRightMenu").click(function(e){
					$(this).toggleClass('m-menu__item--open-dropdown m-menu__item--hover');
					
					e.stopPropagation();
				});
				
				$(document).click(function(){
					
					$(".toggleRightMenu").removeClass('m-menu__item--open-dropdown m-menu__item--hover');
				});
				
			})
        }
        else {
            $("#divRightMenu").hide();
        }       
    });
}

String.prototype.amountFormat = function () {
    var n = parseFloat(this);
    return n.format(2);
};


Number.prototype.format = function (n, x) {
    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
};

function getHtmlNewLinesString(text) {
    if (text) {
        var regexp = new RegExp('\n', 'g');
        return text.replace(regexp, '<br>');
    }
    else if (text == null) {
        return "";
    }
    else {
        return text;
    }
}


function PrintPage() {

    var printButton = $('.printButton');
    if (printButton.length > 0) {
        printButton.hide();
    }

    self.print();

    var printButton = $('.printButton');
    if (printButton.length > 0) {
        setTimeout(function () { printButton.show(); }, 500);
    }
}