﻿var FileNameText = '';
var FileDescriptionText = 'Document Name';
var FileUploadTimeText = '';
var FileOperationText = 'Action';
var FileDeleteText = 'Delete';
var SectionText = '';
var CategoryText = '';

var BaseUrl = "";

function IninUpload(uploadSetting, selectFileCallBack, completeCallBack, uploadCallBack, successCallBack, errorCallBack) {


    InitListener();

    $(uploadSetting.UploadControl).kendoUpload({
        async: {
            withCredentials: false,
            saveUrl: BaseUrl + "/MEHK.CommonAPI/api/Common/AsyncPostFile?GID=" + uploadSetting.GID,
            autoUpload: true,
            maxAutoRetries: 4,
        },
        multiple: false,
        validation: {
            allowedExtensions: uploadSetting.allowedExtensions,
            maxFileSize: uploadSetting.maxFileSize
        },
        localization: {
        	invalidMaxFileSize: uploadSetting.InvalidMaxFileSizeMessge,
            //invalidFileExtension: InvalidFileExtension,
            //select: SelectFileText,
        },
        select: function (e) {
        	selectFileCallBack(e);
        	
        },
        complete: function (e) {

            completeCallBack(e);
            mApp.unblockPage();
        },
        success: function (e) {

            mApp.blockPage();

            if (e.operation == "upload") {

                $(".k-upload-files.k-reset").find("li").remove();
                $(uploadSetting.UploadedFilesTable).data('kendoGrid').dataSource.read();
                $(uploadSetting.UploadedFilesTable).data('kendoGrid').refresh();
             
               
            }

            successCallBack(e);

            mApp.unblockPage();
        },
        upload: function (e) {
            //var data = {
            //	refid: "1",
            //	process: "CAF",
            //	remark: "Remark",
            //	userID: 1,
            //	attachmentType: "Test"
            //}
            //e.data = data;
            uploadCallBack(e);

        },

        error: function (e) {
            mApp.unblockPage();
            errorCallBack(e);

        }
    });

    BindUploadedFilesGrid(uploadSetting);


}

function IninUploadInPDFFormat(uploadSetting, selectFileCallBack, completeCallBack, uploadCallBack, successCallBack, errorCallBack) {


    InitListener();

    $(uploadSetting.UploadControl).kendoUpload({
        async: {
            withCredentials: false,
            saveUrl: BaseUrl + "/MEHK.CommonAPI/api/Common/AsyncPostFileInPDFFormat?GID=" + uploadSetting.GID,
            autoUpload: true,
            maxAutoRetries: 4,
        },
        multiple: false,
        validation: {
            allowedExtensions: uploadSetting.allowedExtensions,
            maxFileSize: uploadSetting.maxFileSize
        },
        //localization: {
        //    invalidMaxFileSize: InvalidMaxFileSizeMessge,
        //    invalidFileExtension: InvalidFileExtension,
        //    select: SelectFileText,
        //},
        select: function (e) {
            selectFileCallBack(e);
        },
        complete: function (e) {

            completeCallBack(e);
            mApp.unblockPage();
        },
        success: function (e) {

            mApp.blockPage();

            if (e.operation == "upload") {

                $(".k-upload-files.k-reset").find("li").remove();
                $(uploadSetting.UploadedFilesTable).data('kendoGrid').dataSource.read();
                $(uploadSetting.UploadedFilesTable).data('kendoGrid').refresh();
            }

            successCallBack(e);

            mApp.unblockPage();
        },
        upload: function (e) {
            //var data = {
            //	refid: "1",
            //	process: "CAF",
            //	remark: "Remark",
            //	userID: 1,
            //	attachmentType: "Test"
            //}
            //e.data = data;
            uploadCallBack(e);

        },

        error: function (e) {
            mApp.unblockPage();
            errorCallBack(e);

        }
    });

    BindUploadedFilesGrid(uploadSetting);


}


function IninUploadButtonTemplate(EnableDeleteFile, Gid) {
    var ButtonTemplate = '';
    if (EnableDeleteFile.toLowerCase() == "true") {
        ButtonTemplate = "<a style='cursor: pointer;' class='flaticon-download' onclick='DownloadFile(\"#= FileID #\", \"#= HasUpload #\")'></a>&nbsp;&nbsp;&nbsp;&nbsp;<a style='cursor: pointer;' class='fa fa-trash-o' onclick=DeleteFile(this,'" + Gid + "','#: FileID #')></a>";
    }
    else {
        ButtonTemplate = "<a style='cursor: pointer;' class='flaticon-download' onclick='DownloadFile(\"#= FileID #\", \"#= HasUpload #\")'></a>";
    }
    return ButtonTemplate;
}

function InitUploadTableColumn(EnableDeleteFile, Gid,UserID, ColumnList, showAction) {

	var GridColumnDefinition = [
    {
        field: "Title",
        filterable: false,
        title: FileNameText,
        template: "<label style = 'word-break: break-all;' >#:Title#</label>",
        width: 200
    },
	{
        field: "DocumentName",
        filterable: false,
        title: FileDescriptionText,
        template: "<label style = 'word-break: break-all;' >#:DocumentName#</label>",
        width: 200
    }, 
	{
	    field: "Category",
	    filterable: false,
	    title: CategoryText,
	    template: "<label style = 'word-break: break-all;' >#:CatName#</label>",
	    width: 200
	},
	{
        field: "Remark",
		encoded: false,
        filterable: false,
        title: "Remarks",
        template: "<label style = 'word-break: break-all;' >#= getHtmlNewLinesString(Remark) #</label>",
        width: 200
    },
	{
		field: "UploadedBy",
		filterable: false,
		title: "Uploaded By",
		template: "<label style = 'word-break: break-all;' >#:UploadedBy#</label>",
		width: 200
	},
	{
		field: "UploadedDate",
		filterable: false,
		title: "Uploaded Date",
		template: "<label style = 'word-break: break-all;' >#:UploadedDate#</label>",
		width: 200
	}];

//    ,
//{
//    filterable: false,
//    title: FileOperationText,
//    template: IninUploadButtonTemplate(EnableDeleteFile, Gid, UserID),
//    width: 100
//}

    var GridColumnList = [];

    for (i = 0; i < ColumnList.length; i++) {

        for(j = 0; j < GridColumnDefinition.length; j++)
        {
            if (GridColumnDefinition[j].field == ColumnList[i])
            {
                GridColumnList.push(GridColumnDefinition[j]);
            }
        }
    }

    if (showAction)
    {
        GridColumnList.push({
            filterable: false,
            title: FileOperationText,
            template: IninUploadButtonTemplate(EnableDeleteFile, Gid, UserID),
            width: 100
        });
    }

    return GridColumnList;
}

function IninUploadTalbeColumn(EnableDeleteFile, Gid) {
    var ColumnList = [{
        field: "Title",
        filterable: false,
        title: FileNameText,
        template: "<label style = 'word-break: break-all;' >#:Title#</label>",
        width: 200
    }, {
        field: "DocumentName",
        filterable: false,
        title: FileDescriptionText,
        template: "<label style = 'word-break: break-all;' >#:DocumentName#</label>",
        width: 200
    }, {
        field: "Remark",
        filterable: false,
        title: "Remark",
        template: "<label style = 'word-break: break-all;' >#:Remark#</label>",
        width: 200
    },
		{
		    field: "UploadedBy",
		    filterable: false,
		    title: "Uploaded By",
		    template: "<label style = 'word-break: break-all;' >#:UploadedBy#</label>",
		    width: 200
		},
		{
		    field: "UploadedDate",
		    filterable: false,
		    title: "Uploaded Date",
		    template: "<label style = 'word-break: break-all;' >#:UploadedDate#</label>",
		    width: 200
		},
		{
		    filterable: false,
		    title: FileOperationText,
		    template: IninUploadButtonTemplate(EnableDeleteFile, Gid),
		    width: 100
		}];

    return ColumnList;
}


function IninUploadButtonTemplate(EnableDeleteFile, Gid, userID) {
    var ButtonTemplate = '';
    if (EnableDeleteFile.toLowerCase() == "true") {
        ButtonTemplate = "<a style='cursor: pointer;' class='flaticon-download' onclick='DownloadFile(\"#= FileID #\", \"#= HasUpload #\")'></a># if(data.UploadedByUserID== " + userID + " ) {  # &nbsp;&nbsp;&nbsp;&nbsp;<a style='cursor: pointer;' class='fa fa-trash-o' onclick=DeleteFile(this,'" + Gid + "','#: FileID #')></a> # } else{ #  #} #";
    }
    else {
        ButtonTemplate = "<a style='cursor: pointer;' class='flaticon-download' onclick='DownloadFile(\"#= FileID #\", \"#= HasUpload #\")'></a>";
    }
    return ButtonTemplate;
}

function IninUploadMobileTalbeColumn(EnableDeleteFile, Gid, UserID) {
    var ColumnList = [{
        field: "Title",
        filterable: false,
        title: FileNameText,
        template: "<label style = 'word-break: break-all;' >#:Title#</label>",
        width: 200
    }, {
        field: "DocumentName",
        filterable: false,
        title: FileDescriptionText,
        template: "<label style = 'word-break: break-all;' >#:DocumentName#</label>",
        width: 200
    }, {
        field: "Remark",
        encoded: false,
        filterable: false,
        title: "Remark",
        template: "<label style = 'word-break: break-all;' >#= getHtmlNewLinesString(Remark) #</label>",
        width: 200
    },
		{
		    field: "UploadedBy",
		    filterable: false,
		    title: "Uploaded By",
		    template: "<label style = 'word-break: break-all;' >#:UploadedBy#</label>",
		    width: 200
		},
		{
		    field: "UploadedDate",
		    filterable: false,
		    title: "Uploaded Date",
		    template: "<label style = 'word-break: break-all;' >#:UploadedDate#</label>",
		    width: 200
		},
		{
		    filterable: false,
		    title: FileOperationText,
		    template: IninUploadButtonTemplate(EnableDeleteFile, Gid, UserID),
		    width: 100
		}];

    return ColumnList;
}

function IninUploadTalbeColumn(EnableDeleteFile, Gid, userID) {
    var ColumnList = [{
        field: "Title",
        filterable: false,
        title: FileNameText,
        template: "<label style = 'word-break: break-all;' >#:Title#</label>",
        width: 150
    }, {
        field: "Section",
        filterable: false,
        title: SectionText,
        template: "<label style = 'word-break: break-all;' >#:Section#</label>",
        width: 100
    },
	{
	    field: "Document Name",
	    filterable: false,
	    title: FileDescriptionText,
	    template: "<label style = 'word-break: break-all;' >#:DocumentName#</label>",
	    width: 150
	},
	{
	    field: "Category",
	    filterable: false,
	    title: CategoryText,
	    template: "<label style = 'word-break: break-all;' >#:CatName#</label>",
	    width: 100
	},
		{
		    field: "UploadedDate",
		    filterable: false,
		    title: "Uploaded Date",
		    template: "<label style = 'word-break: break-all;' >#:UploadedDate#</label>",
		    width: 150
		},
		{
		    field: "UploadedBy",
		    filterable: false,
		    title: "Uploaded By",
		    template: "<label style = 'word-break: break-all;' >#:UploadedBy#</label>",
		    width: 150
		},

		{
		    field: "Remark",
		    filterable: false,
		    title: "Remark",
		    template: "<label style = 'word-break: break-all;' >#:Remark#</label>",
		    width: 200
		},
		{
		    filterable: false,
		    title: FileOperationText,
		    template: IninUploadButtonTemplate(EnableDeleteFile, Gid, userID),
		    width: 100
		}];

    return ColumnList;
}


function InitFields() {
    var setfields =
	{
	    fields: {
	        FileID: { type: "string" },
	        Title: { type: "string" },
	        DocumentName: { type: "string" },
	        CatName: { type: "string" },
	        FileType: { type: "string" },
	        Section: { type: "string" },
	        Remark: { type: "string" },
	        Path: { type: "string" },
	        AppID: { type: "string" },
	        EnableDelete: { type: "string" },
	    }
	};
    return setfields;
}


function BindUploadedFilesGrid(uploadSetting) {

    var GID = uploadSetting.GID;

    var ColumnList = uploadSetting.TableColumnList;

    $(uploadSetting.UploadedFilesTable).kendoGrid({
        dataSource: {
            transport: {
                read: {
                    url: BaseUrl + "/MEHK.CommonAPI/api/Common/UploadedFileRead",
                    dataType: "json",
                    data: function () { return { GID: GID, FileSessionName: "UploadedFiles" } },
                    cache: false
                }
            },
            schema: {
                model: uploadSetting.TableField
            },

        },
        scrollable: false,
        columns: ColumnList,
        dataBound: function (e) {

            if (this.dataSource.total() === 0) {
                $(uploadSetting.TableArea).hide();

            }
            else {
                $(uploadSetting.TableArea).show();
            }
        }
    });
}



function DownloadFile(FileID, HasUpload) {
    RemoveWindowOnBeforeUnloadListener();
    window.location.href = BaseUrl + "/MEHK.CommonAPI/api/Common/Download?FileID=" + FileID + "&HasUpload=" + HasUpload;

    setTimeout(function () {

        if (typeof beforeunloadListener != 'undefined') {
            window.addEventListener("beforeunload", beforeunloadListener, false);
        }
    }, 1000);
}

function DeleteFile(e, FileID, GID) {

    var gv = $(e).closest(".k-grid");
    //console.log(gv);

    $.ajax({
        type: "get",
        url: BaseUrl + "/MEHK.CommonAPI/api/Common/Delete?FileID=" + FileID + "&GID=" + GID,

        cache: false,
        async: false,
        success: function (data) {
            gv.data('kendoGrid').dataSource.read();
            gv.data('kendoGrid').refresh();
        },
        error: function (msg) {
            //alert("ERROR " + msg);
            console.log(msg);
        }
    })
}



function InitFiles(formID, gID, processCode) {

    $.ajax({
        type: "get",
        url: BaseUrl + "/MEHK.CommonAPI/api/Common/InitFiles?id=" + formID + "&gID=" + gID + "&processCode=" + processCode,
        cache: false,
        async: false,
        success: function (data) {
        },
        error: function (msg) {
            //alert("ERROR " + msg);
            console.log(msg);
        }
    })
}

function InitUploadAfterApprovedFiles(formID, gID, processCode) {

    $.ajax({
        type: "get",
        url: BaseUrl + "/MEHK.CommonAPI/api/Common/InitUploadAfterApprovedFiles?id=" + formID + "&gID=" + gID + "&processCode=" + processCode,
        cache: false,
        async: false,
        success: function (data) {
        },
        error: function (msg) {
            //alert("ERROR " + msg);
            console.log(msg);
        }
    })
}



function SubmitChanges(formID, gID, userID, processCode) {

    $.ajax({
        type: "get",
        url: BaseUrl + "/MEHK.CommonAPI/api/Common/UpdateFiles?id=" + formID + "&gID=" + gID + "&userID=" + userID + "&processCode=" + processCode,
        cache: false,
        async: false,
        success: function (data) {

        },
        error: function (msg) {
            //alert("ERROR " + msg);
            console.log(msg);
        }
    })
}

function UpdateUploadAfterApprovedFilesSubmitChanges(formID, gID, userID, processCode) {

    $.ajax({
        type: "get",
        url: BaseUrl + "/MEHK.CommonAPI/api/Common/UpdateUploadAfterApprovedFiles?id=" + formID + "&gID=" + gID + "&userID=" + userID + "&processCode=" + processCode,
        cache: false,
        async: false,
        success: function (data) {

        },
        error: function (msg) {
            //alert("ERROR " + msg);
            console.log(msg);
        }
    })
}
