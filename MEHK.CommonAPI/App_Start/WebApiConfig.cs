﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Routing;

namespace MEHK.CommonAPI
{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'WebApiConfig'
	public static class WebApiConfig
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'WebApiConfig'
	{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'WebApiConfig.Register(HttpConfiguration)'
		public static void Register(HttpConfiguration config)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'WebApiConfig.Register(HttpConfiguration)'
		{
            var corsAttr = new EnableCorsAttribute("*", "*", "*");

            config.EnableCors(corsAttr);
            // Web API configuration and services

            config.Services.Replace(typeof(System.Web.Http.ExceptionHandling.IExceptionLogger), new UnhandledExceptionLogger());

			// Web API routes
			config.MapHttpAttributeRoutes();

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);
            HttpMethod[] methf = {HttpMethod.Put,HttpMethod.Get,HttpMethod.Post };

            config.Routes.MapHttpRoute(
				name: "DefaultApi",
               // constraints: new { httpMethod = new HttpMethodConstraint(methf) },
                routeTemplate: "api/{controller}/{action}/{id}",
				defaults: new { id = RouteParameter.Optional }
			);

            
        }
	}
}
