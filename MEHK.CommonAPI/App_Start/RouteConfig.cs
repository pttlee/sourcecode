﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace MEHK.CommonAPI
{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'RouteConfig'
	public class RouteConfig
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'RouteConfig'
	{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'RouteConfig.RegisterRoutes(RouteCollection)'
		public static void RegisterRoutes(RouteCollection routes)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'RouteConfig.RegisterRoutes(RouteCollection)'
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			//// Web API Stateless Route Configurations
			//routes.MapHttpRoute(
			//	name: "DefaultApi",
			//	routeTemplate: "api/{controller}/{id}",
			//	defaults: new { id = RouteParameter.Optional }
			//).RouteHandler = new SessionStateRouteHandler();


			routes.MapRoute(
				name: "Default",
				url: "{controller}/{action}/",
				defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
			);
           
        }
	}
}
