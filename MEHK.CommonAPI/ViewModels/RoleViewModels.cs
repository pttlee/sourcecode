﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.CommonAPI.ViewModels
{
	public class RoleViewModels
	{
		public int RoletId { get; set; }
		public string RoleName { get; set; }
		public string Remark { get; set; }
	}
}