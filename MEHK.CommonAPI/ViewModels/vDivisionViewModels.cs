﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.CommonAPI.ViewModels
{
   public class vDivisionViewModels
    {
        public int UnitId { get; set; }
        public string UnitName { get; set; }
    }
}
