﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.BLL.ViewModels
{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'MessageViewModel'
	public class MessageViewModel
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'MessageViewModel'
	{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'MessageViewModel.Success'
		public bool Success { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'MessageViewModel.Success'

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'MessageViewModel.Title'
		public string Title { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'MessageViewModel.Title'
	}
}