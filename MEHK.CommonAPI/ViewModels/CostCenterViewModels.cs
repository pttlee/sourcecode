﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.CommonAPI.ViewModels
{
	public class CostCenterViewModels
	{
		public int CostCenterId { get; set; }
		public string Code { get; set; }
		public string Description { get; set; }
		public string PIC { get; set; }
	}
}