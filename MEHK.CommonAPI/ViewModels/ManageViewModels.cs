﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.CommonAPI.ViewModels
{
  
    public class ManageViewModels
    {
        public int MemberId { get; set; }
        public string MemberName { get; set; }
        public string LoginAccount { get; set; }
        public string StaffId { get; set; }
        public string SAMAccount { get; set; }
        public string SAPAccount { get; set; }
        public string Post { get; set; }
        public string Phone { get; set; }
        public string EmailAddress { get; set; }
        public int JobLevelId { get; set; }
        public int? JobLevel { get; set; }
        public string JobRank { get; set; }
        public string Remark { get; set; }
        public int UnitId { get; set; }

    }
}