﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.CommonAPI.ViewModels
{
	public class ApprovalLevelViewModels
	{
		public int ApprovalLevelId { get; set; }
		public int Seq { get; set; }
		public string ApprovalLevelCode { get; set; }
		public string ApprovalLevelName { get; set; }
	}
}