﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.CommonAPI.ViewModels
{
   public class SystemParameterViewModels
    {
        public string AppKey { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public string GroupName { get; set; }
    }
}
