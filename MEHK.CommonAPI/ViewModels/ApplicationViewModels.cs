﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.CommonAPI.ViewModels
{
	public class ApplicationViewModels
	{
		public int ID { get; set; }
		public string AppName { get; set; }
		public string AppCode { get; set; }
		public string Description { get; set; }
	}
}