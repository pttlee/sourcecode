﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.CommonAPI.ViewModels
{
	public class UnitViewModels
	{
		public int UnitId { get; set; }
		public string UnitShortName { get; set; }
		public string UnitName { get; set; }
		public int UnitTypeId { get; set; }
		public string Description { get; set; }
	}
}