﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.CommonAPI.ViewModels
{
	public class ApprovalMartixOptionViewModels
	{
		public string ApprovalMartixOptionCode { get; set; }
		public string ApprovalMartixOptionName { get; set; }
	}
}