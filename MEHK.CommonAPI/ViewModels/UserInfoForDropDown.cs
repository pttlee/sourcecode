﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.CommonAPI.ViewModels
{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'UserInfoForDropDown'
    public class UserInfoForDropDown
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'UserInfoForDropDown'
    {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'UserInfoForDropDown.MemberId'
        public int MemberId { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'UserInfoForDropDown.MemberId'
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'UserInfoForDropDown.DisplaynameAndStaffNum'
        public string DisplaynameAndStaffNum { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'UserInfoForDropDown.DisplaynameAndStaffNum'

    }
}