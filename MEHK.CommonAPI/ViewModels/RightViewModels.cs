﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.CommonAPI.ViewModels
{
	public class RightViewModels
	{
		//public int ID { get; set; }
		//public string RightName { get; set; }
		//public string Description { get; set; }
		//public int? AppID { get; set; }
		//public string AppName { get; set; }
		public int ID { get; set; }
		public string RightName { get; set; }
		public string Description { get; set; }
		public int AppID { get; set; }
		public string AppName { get; set; }
	}
}