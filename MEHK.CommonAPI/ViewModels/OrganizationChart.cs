﻿using MEHK.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.CommonAPI.ViewModels
{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'OrganizationChart'
    public class OrganizationChart
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'OrganizationChart'
    {

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'OrganizationChart.OrganizationChart(Unit)'
        public OrganizationChart(Unit CurrentUnit)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'OrganizationChart.OrganizationChart(Unit)'
        {
            this._CurrentUnit = CurrentUnit;
            //    this._Children = Children;
            GetChildren();
        }
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'OrganizationChart.CurrentUnitId'
        public int CurrentUnitId { get { return CurrentUnit.UnitId; } }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'OrganizationChart.CurrentUnitId'
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'OrganizationChart.CurrentUnit'
        public Unit CurrentUnit { get { return _CurrentUnit; }  }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'OrganizationChart.CurrentUnit'
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'OrganizationChart.Children'
        public List<OrganizationChart> Children  { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'OrganizationChart.Children'


        private Unit _CurrentUnit;
        private List<OrganizationChart> _Children { get; set; }

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'OrganizationChart.UpdateCurrentUnit(Unit)'
        public bool UpdateCurrentUnit(Unit updatedUnit)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'OrganizationChart.UpdateCurrentUnit(Unit)'
        {
            if (_CurrentUnit.UnitId != updatedUnit.UnitId)
                return false;
            _CurrentUnit = updatedUnit;
            return true;
        }
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'OrganizationChart.GetChildren()'
        public void GetChildren()
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'OrganizationChart.GetChildren()'
        {
            this._Children = new List<OrganizationChart>();
            var _ChildrenNodes = new Entities().Units.Where(x => x.ParentUnitId == _CurrentUnit.UnitId);
            foreach (var node in _ChildrenNodes)
            {
                OrganizationChart Child = new OrganizationChart(node);
                _Children.Add(Child);
            }
            return; 
        }
        //public bool UpdateChildrenUnit(Unit updatedUnit)
        //{
        //    foreach(var )
        //}

    }
}