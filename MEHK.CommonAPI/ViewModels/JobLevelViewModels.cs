﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.CommonAPI.ViewModels
{
	public class JobLevelViewModels
	{
		public int JobLevelId { get; set; }
		public int? RankingSequence { get; set; }
		public string Title { get; set; }
		public int? JobLevel { get; set; }
	}
}