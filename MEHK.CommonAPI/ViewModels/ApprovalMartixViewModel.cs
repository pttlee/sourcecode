﻿using MEHK.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MEHK.DAL.ViewModel;
namespace MEHK.CommonAPI.ViewModels
{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixViewModel'
    public class ApprovalMartixViewModel
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixViewModel'
    {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixViewModel.ApprovalMartixId'
        public int ApprovalMartixId { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixViewModel.ApprovalMartixId'
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixViewModel.ApprovalMartixName'
        public string ApprovalMartixName { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixViewModel.ApprovalMartixName'
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixViewModel.ProcessId'
        public int ProcessId { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixViewModel.ProcessId'
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixViewModel.UnitId'
        public int? UnitId { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixViewModel.UnitId'
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixViewModel.RoleId'
        public int? RoleId { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixViewModel.RoleId'
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixViewModel.CopyDetailFrom'
        public int? CopyDetailFrom { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixViewModel.CopyDetailFrom'
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixViewModel.ApprovalMartixOptionCode'
        public string ApprovalMartixOptionCode { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixViewModel.ApprovalMartixOptionCode'
        //public decimal FromAmount { get; set; }
        //public decimal ToAmount { get; set; }
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixViewModel.Details'
        public List<ApprovalMartixDetailViewModel> Details;
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixViewModel.Details'
    }
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixDetailViewModel'
    public class ApprovalMartixDetailViewModel
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixDetailViewModel'
    {
        // ApprovalMartixDetail
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixDetailViewModel.ApprovalMartixDetailId'
        public int ApprovalMartixDetailId;
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixDetailViewModel.ApprovalMartixDetailId'
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixDetailViewModel.ApprovalMartixId'
        public int ApprovalMartixId;
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixDetailViewModel.ApprovalMartixId'
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixDetailViewModel.ApprovalLevelId'
        public int ApprovalLevelId;
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixDetailViewModel.ApprovalLevelId'
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixDetailViewModel.ApprovalCriteriaType'
        public string ApprovalCriteriaType;
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixDetailViewModel.ApprovalCriteriaType'
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixDetailViewModel.ApprovalCriteria'
        public string ApprovalCriteria;
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixDetailViewModel.ApprovalCriteria'
        //[ApprovalLevel]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixDetailViewModel.Seq'
        public int Seq;
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixDetailViewModel.Seq'
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixDetailViewModel.ApprovalLevelCode'
        public string ApprovalLevelCode;
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixDetailViewModel.ApprovalLevelCode'
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixDetailViewModel.ApprovalLevelName'
        public string ApprovalLevelName;
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixDetailViewModel.ApprovalLevelName'
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixDetailViewModel.Approvers'
        public List<string> Approvers;
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'ApprovalMartixDetailViewModel.Approvers'
        public string ApproversFulList;

        public List<ApproversDetails> ApproverDetails;
    }
}

