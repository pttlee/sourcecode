﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MEHK.CommonAPI.ViewModels
{
	public class ApprovalMartixViewModels
	{
		public ApprovalMartixViewModels()
		{
			ApprovalMartixDetailList = new List<ApprovalMartixDetailModel>();
		}
		public int ApprovalMartixId { get; set; }
		public string ApprovalMartixName { get; set; }
		public int ProcessId { get; set; }
		public int UnitId { get; set; }
		public int? RoleId { get; set; }
		public decimal Priority { get; set; }
		public int? CopyDetailFrom { get; set; }
		public string ApprovalMartixOptionCode { get; set; }
		public string ProcessName { get; set; }
		public string UnitName { get; set; }
		public string RoleName { get; set; }
		public string ApprovalMartixOptionName { get; set; }
		public List<ApprovalMartixDetailModel> ApprovalMartixDetailList { get; set; }

		public string Supervisor1_CriteriaType { get; set; }
		public string Supervisor1_Criteria { get; set; }
		public string Supervisor2_CriteriaType { get; set; }
		public string Supervisor2_Criteria { get; set; }
		public string AssistantManger_CriteriaType { get; set; }
		public string AssistantManger_Criteria { get; set; }
		public string DepartmentManager_CriteriaType { get; set; }
		public string DepartmentManager_Criteria { get; set; }
		public string SeniorManager_CriteriaType { get; set; }
		public string SeniorManager_Criteria { get; set; }
		public string DepartmentGeneralManager_CriteriaType { get; set; }
		public string DepartmentGeneralManager_Criteria { get; set; }
		public string GeneralManager_CriteriaType { get; set; }
		public string GeneralManager_Criteria { get; set; }
		public string GPAD_CriteriaType { get; set; }
		public string GPAD_Criteria { get; set; }
		public string MD_CriteriaType { get; set; }
		public string MD_Criteria { get; set; }
		public string ACVerification_CriteriaType { get; set; }
		public string ACVerification_Criteria { get; set; }
		public string ACVerification2_CriteriaType { get; set; }
		public string ACVerification2_Criteria { get; set; }
		public string ACApproval_CriteriaType { get; set; }
		public string ACApproval_Criteria { get; set; }
		public string HRVerification_CriteriaType { get; set; }
		public string HRVerification_Criteria { get; set; }
		public string HRVerification2_CriteriaType { get; set; }
		public string HRVerification2_Criteria { get; set; }
		public string HRApprova_CriteriaType { get; set; }
		public string HRApprova_Criteria { get; set; }


	}
	public class ApprovalMartixDetailModel
	{
		// ApprovalMartixDetail
		public int ApprovalMartixDetailId { get; set; }
		public int ApprovalMartixId { get; set; }
		public int? ApprovalLevelId { get; set; }
		public string ApprovalCriteriaType { get; set; }
		public string ApprovalCriteria { get; set; }
		public string ApprovalLevelName { get; set; }
		public string ApprovalLevelCode { get; set; }
		public string Description { get; set; }

	}
}