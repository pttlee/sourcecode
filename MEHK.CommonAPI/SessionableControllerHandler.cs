﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.WebHost;
using System.Web.Routing;
using System.Web.SessionState;

namespace MEHK.CommonAPI
{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'SessionableControllerHandler'
	public class SessionableControllerHandler : HttpControllerHandler, IRequiresSessionState
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'SessionableControllerHandler'
	{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'SessionableControllerHandler.SessionableControllerHandler(RouteData)'
		public SessionableControllerHandler(RouteData routeData)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'SessionableControllerHandler.SessionableControllerHandler(RouteData)'
			: base(routeData)
		{ }
	}

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'SessionStateRouteHandler'
	public class SessionStateRouteHandler : IRouteHandler
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'SessionStateRouteHandler'
	{
		IHttpHandler IRouteHandler.GetHttpHandler(RequestContext requestContext)
		{
			return new SessionableControllerHandler(requestContext.RouteData);
		}
	}
}