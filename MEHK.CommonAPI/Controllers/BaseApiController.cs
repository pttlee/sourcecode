﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using MEHK.BLL;
using MEHK.DAL.Model;

namespace MEHK.CommonAPI.Controllers
{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'BaseApiController'
	public class BaseApiController : ApiController
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'BaseApiController'
	{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'BaseApiController.OrganizationMain'
		public OrganizationBLL OrganizationMain
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'BaseApiController.OrganizationMain'
		{
			get
			{
				return new OrganizationBLL();
			}
		}

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'BaseApiController.CommonMain'
		public CommonBLL CommonMain
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'BaseApiController.CommonMain'
		{
			get
			{
				return new CommonBLL();
			}
		}

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'BaseApiController.GetDB'
		protected Entities GetDB
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'BaseApiController.GetDB'
		{
			get
			{
				return new Entities();
			}
		}



		private T SafeExecutor<T>(Func<T> action)
		{
			try
			{
				return action();
			}
			catch (Exception ex)
			{
				CommonMain.CreateErrorLog(ex, "CommonAPI");
			}

			return default(T);
		}

	}
}