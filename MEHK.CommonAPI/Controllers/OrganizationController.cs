﻿using MEHK.CommonAPI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MEHK.DAL.DTO;
using MEHK.DAL.Model;
using MEHK.DAL.ViewModel;
using MEHK.BLL.ViewModels;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System.Web.Mvc;
using System.Net.Http;

namespace MEHK.CommonAPI.Controllers
{


    #region endpoint list
    //-GetMember
    //MemberID


    //-GetMember
    //SAMAccount
    //SAPAccount
    //StaffID
    //MemberName

    //-GetOrganizationChart

    //-GetUnit
    //UnitID

    //-GetUnits
    //UnitCode
    ////MemberID

    //-GetUnitMembers
    //UnitID

    ////-GetMemberUnits
    ////memberID
    ////isRelated
    //GetApprovalMartix
    //ProcessCode
    //UnitIds

    //-GetRole
    //RoleId

    //-GetRoles
    //UnitCode
    //MemberID

    //-GetRoleMembers
    //RoleId

    //-GetMemberRoles
    //memberID

    //-GetCostCenters

    //-GetUnitCostCenter
    //UnitID

    //-GetUserCostCenter
    //UserID

    //-GetWorkflowApprovers
    //UserID
    //WorkflowID
    //ApprovalMartixID

    //-GetApprovers
    //UserID
    //WorkflowID
    //ApprovalMartixID
    //joblevel
    //RoleID
    #endregion endpoint list



#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController'
    public class OrganizationController : BaseApiController
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController'
    {

        #region Members
        [System.Web.Http.Route("api/Organization/Members")]
        [System.Web.Http.HttpGet]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController.Members(int, int, string, string, string, string, bool)'
        public List<UserInfo> Members(int UnitId = -1, int RoleId = -1, string SAMAccount = "", string SAPAccount = "", string StaffID = "", string MemberName = "", bool haveUnit = true, bool additionalinfo = true)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController.Members(int, int, string, string, string, string, bool)'
        {
            if (additionalinfo)
            {
                if (haveUnit)
                {

                    if (UnitId > 0)
                    {
                        var UnitMembers = GetUnitMembers(UnitId);
                        return (UnitMembers);
                    }
                    if (RoleId > 0)
                    {
                        var RoleMembers = GetRoleMembers(RoleId);
                        return (RoleMembers);
                    }
                }
                else
                {
                    //return OrganizationMain.GetMemberWithoutUnit();
                    return OrganizationMain.GetMemberList();
                }
                return GetMembers(SAMAccount, SAPAccount, StaffID, MemberName);
            }
            else
                return GetMembers(SAMAccount, SAPAccount, StaffID, MemberName, false, true);

        }
        [System.Web.Http.Route("api/Organization/Members")]
        [System.Web.Http.HttpDelete]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController.DeleteMembers(UserInfo)'
        public List<UserInfo> DeleteMembers([FromBody]UserInfo ui)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController.DeleteMembers(UserInfo)'
        {

            List<UserInfo> lui = new List<UserInfo>();

            if (ui.UnitId > 0 && ui.MemberId > 0)
            {

                OrganizationMain.RemoveMemberUnit(ui.MemberId, ui.UnitId);
                lui.Add(ui);
                return lui;
            }
            if (ui.Roles.Count() > 0)
            {
                foreach (var role in ui.Roles)
                    OrganizationMain.RemoveMemberRole(role.RoletId, ui.MemberId);

                lui.Add(ui);
                return lui;
            }
            if (ui.MemberId > 0)
            {
                OrganizationMain.RemoveMember(ui.MemberId);
                lui.Add(ui);
                return lui;
            }
            return lui;
        }
        [System.Web.Http.Route("api/Organization/Members")]
        [System.Web.Http.HttpPost]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController.PostMembers(UserInfo)'
        public List<UserInfo> PostMembers([FromBody]UserInfo ui)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController.PostMembers(UserInfo)'
        {
            // var ui = JsonConvert.DeserializeObject<UserInfo>(body);
            HttpRequest httpRequest = HttpContext.Current.Request;

            //Guid GID = new Guid(httpRequest.Form["GID"]);
            //string sUnitId = httpRequest.Form["UnitId"];
            //string sMemberId = httpRequest.Form["MemberId"];
            List<UserInfo> lui = new List<UserInfo>();

            if (ui.UnitId > 0 && ui.MemberId > 0)
            {

                if (OrganizationMain.AddMemberUnit(ui.MemberId, ui.UnitId, 1).IsSuccess)
                {
                    UserInfo _ui = GetMember(ui.MemberId);
                    //List<UserInfo> lui = new List<UserInfo>();
                    lui.Add(_ui);
                    return lui;
                }
            }
            return lui;
        }
        [System.Web.Http.Route("api/Organization/Members")]
        [System.Web.Http.HttpPut]
        public List<ManageViewModels> PutMembers([FromBody]ManageViewModels vms)
        {
            List<UserInfo> lui = new List<UserInfo>();
            UserInfo userInfo = new UserInfo();
            userInfo.MemberId = vms.MemberId;
            userInfo.MemberName = vms.MemberName;
            userInfo.LoginAccount = vms.LoginAccount;
            userInfo.StaffId = vms.StaffId;
            userInfo.SAMAccount = vms.SAMAccount;
            userInfo.SAPAccount = vms.SAPAccount;
            userInfo.Post = vms.Post;
            userInfo.Phone = vms.Phone;
            userInfo.EmailAddress = vms.EmailAddress;
            userInfo.JobLevelId = vms.JobLevelId;
            userInfo.JobRank = vms.JobRank;
            userInfo.Remark = vms.Remark;
            userInfo.UnitId = vms.UnitId;
            lui.Add(userInfo);

            List<UserInfo> errolui = new List<UserInfo>();
            foreach (var ui in lui)
            {
                if (ui.MemberId > 0)
                {

                    var result = OrganizationMain.UpdateMember(ui);
                    if (result == null)
                        errolui.Add(ui);
                }
                else
                {
                    errolui.Add(ui);
                }
            }
            if (errolui.Count() > 0)
            {
                return null;
            }
            else
            {
                int? JobLevel = OrganizationMain.GetJobLeveln().Where(e => e.JobLevelId == vms.JobLevelId).FirstOrDefault().JobLevel1;
                List<ManageViewModels> ml = new List<ManageViewModels>();
                vms.JobLevel = JobLevel == null ? 0 : (int)JobLevel;
                ml.Add(vms);
                return ml;
            }
        }

        private List<UserInfo> GetMembers(string SAMAccount = "", string SAPAccount = "", string StaffID = "", string MemberName = "", bool DisplayDepartmentAndDivision = true, bool additional = true)
        {

            var result = OrganizationMain.GetMembers(false, MemberName, SAMAccount, SAPAccount, StaffID, DisplayDepartmentAndDivision, additional).ToList();

            return result;
        }
        public List<UserInfo> GetMemberList()
        {
            return OrganizationMain.GetMemberList();
        }

        private UserInfo GetMember(int MemberId)
        {
            var result = OrganizationMain.GetMember(MemberId);
            return result;
        }

        private List<UserInfo> GetUnitMembers(int UnitId)
        {
            return OrganizationMain.GetUnitMembers(UnitId);
        }
        private List<UserInfo> GetRoleMembers(int RoleId)
        {
            return OrganizationMain.GetMembersByRole(RoleId);
        }
        #endregion Members
        #region OrganizationChart
        [System.Web.Http.HttpGet]
        private List<OrganizationChart> OrganizationChart()
        {
            return new Entities().Units.Where(x => !x.ParentUnitId.HasValue).ToList().Select
                (y => new OrganizationChart(y)).ToList();
        }
        [System.Web.Http.HttpGet]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController.OrganizationUnits(int?)'
        public List<TreeViewDto> OrganizationUnits(int? id)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController.OrganizationUnits(int?)'
        {
            //List<TreeViewDto> result = new List<TreeViewDto>();
            using (var db = GetDB)
            {
                var organizationUnits = db.Units.Where(q => id.HasValue ? q.ParentUnitId == id && !q.IsDeleted : q.ParentUnitId == null && !q.IsDeleted).ToList().Select
                    (x => new TreeViewDto()
                    {
                        id = x.UnitId,
                        Name = x.UnitName,
                        //hasChildren = IsParentUnit(x.UnitId)
                    }).ToList();
                return organizationUnits;
            }
            // return result;
        }
        [System.Web.Http.HttpGet]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController.UnitTypes(int, int)'
        public List<UnitType> UnitTypes(int id = -1, int UnitId = -1)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController.UnitTypes(int, int)'
        {
            if (id < 0 && UnitId < 0)
            {
                return OrganizationMain.GetAllUnitTypes();
            }
            else if (UnitId > 0)
            {
                var unit = OrganizationMain.GetUnit(UnitId);
                return UnitTypes(unit.UnitTypeId);
            }
            else

            {
                return new List<DAL.Model.UnitType>() { OrganizationMain.GetUnitTypes(id) };
            }
        }
        [System.Web.Http.HttpGet]
        private List<MemberUnit> MemberUnits(int id)
        {
            //List<TreeViewDto> result = new List<TreeViewDto>();
            using (var db = GetDB)
            {

                var MemberUnits = db.vMembers.Where(x => x.UnitId.HasValue && x.UnitId.Value == id).AsEnumerable().Select(
                    y => new MemberUnit()
                    {
                        //Id = OrganizationMain.GetIdfromUnitMember(y.MemberId,y.UnitId.Value),
                        MemberId = y.MemberId,
                        UnitId = id,
                        CreatedDate = y.Created,
                        MemberName = y.MemberName
                    }).ToList();
                return MemberUnits;
            }
            // return result;

        }
        [System.Web.Mvc.HttpGet]
        private ActionResult MemberUnits(int id, [DataSourceRequest] DataSourceRequest request)
        {
            //List<TreeViewDto> result = new List<TreeViewDto>();
            using (var db = GetDB)
            {

                var MemberUnits = db.vMembers.Where(x => x.UnitId.HasValue && x.UnitId.Value == id).AsEnumerable().Select(
                    y => new MemberUnit()
                    {
                        //Id = OrganizationMain.GetIdfromUnitMember(y.MemberId,y.UnitId.Value),
                        MemberId = y.MemberId,
                        UnitId = id,
                        CreatedDate = y.Created,
                        MemberName = y.MemberName
                    }).ToList();
                var Result = MemberUnits.ToDataSourceResult(request);
                System.Web.Mvc.JsonResult result = new JsonResult();
                result.Data = Result;
                result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                return result;
            }
            // return result;

        }

        #endregion OrganizationChart
        #region Unit
        [System.Web.Http.HttpGet]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController.Units(int, string, string)'
        public List<Unit> Units(int UnitId = -1, string UnitName = "", string currentTypeId = "")
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController.Units(int, string, string)'
        {


            if (UnitId > 0)
            {
                var Unit = GetUnit(UnitId);
                return new List<Unit>() { Unit };
            }
            if (!string.IsNullOrEmpty(currentTypeId))
            {
                var Unit = GetUnits().Where(x => x.UnitTypeId.ToString() == currentTypeId).ToList();
                return Unit;
            }
            return GetUnits(UnitName);

            //    result = result.Distinct().ToList();

            // return result;
        }
        [System.Web.Http.HttpPut]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController.Units(List<UpdateUnitModel>)'
        public JsonResult<bool> Units(List<UpdateUnitModel> PutUnits)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController.Units(List<UpdateUnitModel>)'
        {

            //bool Result = false;
            //var _UpdateUnitModel = this.DeserializeObject<IEnumerable<UpdateUnitModel>>("models");
            BaseResult bs = new BaseResult();
            bs.IsSuccess = false;
            try
            {

                if (PutUnits.FirstOrDefault() != null)
                {
                    bs.IsSuccess = true;
                    foreach (UpdateUnitModel update in PutUnits)
                    {
                        if (update.UnitId > 0)
                            OrganizationMain.UpdateUnit(update, ref bs);
                        else
                            OrganizationMain.CreateUnit(update, ref bs);
                        // CommonMain.CreateDebugLog(JsonConvert.SerializeObject(PostUnits));
                    }
                }

            }
            catch (Exception e)
            {
                bs.IsSuccess = false;
                bs.Message = e.Message;
                CommonMain.CreateErrorLog(e, "API:HttpPut:Units");
            }
            return Json(bs.IsSuccess);
        }
        [System.Web.Http.HttpDelete]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController.Units(List<int>)'
        public JsonResult<bool> Units(List<int> DeleteUnits)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController.Units(List<int>)'
        {

            //  bool Result = false;
            //var _UpdateUnitModel = this.DeserializeObject<IEnumerable<UpdateUnitModel>>("models");
            BaseResult bs = new BaseResult();
            bs.IsSuccess = false;
            try
            {

                if (DeleteUnits != null && DeleteUnits.Count() > 0)
                {
                    bs.IsSuccess = true;
                    foreach (int deleteid in DeleteUnits)
                    {
                        UpdateUnitModel _unit = new UpdateUnitModel();
                        _unit.UnitId = deleteid;
                        OrganizationMain.UpdateUnit(_unit, ref bs, true);
                    }
                }

            }
            catch (Exception e)
            {
                bs.IsSuccess = false;
                bs.Message = e.Message;
                CommonMain.CreateErrorLog(e, "API:HttpDelete:Units");
            }
            return Json(bs.IsSuccess);
        }
        private Unit GetUnit(int UnitId)
        {
            return OrganizationMain.GetUnit(UnitId);
        }
        private List<Unit> GetUnits(string UnitName = "")
        {
            return OrganizationMain.GetUnits(UnitName);
        }
        [System.Web.Http.HttpGet]
        public JsonResult<bool> DeletUnitsbyId(int UnitsId)
        {

            //  bool Result = false;
            //var _UpdateUnitModel = this.DeserializeObject<IEnumerable<UpdateUnitModel>>("models");
            BaseResult bs = new BaseResult();
            bs.IsSuccess = false;
            try
            {

                if (UnitsId > 0)
                {
                    bs.IsSuccess = true;
                   
                        UpdateUnitModel _unit = new UpdateUnitModel();
                        _unit.UnitId = UnitsId;
                        OrganizationMain.UpdateUnit(_unit, ref bs, true);
                }

            }
            catch (Exception e)
            {
                bs.IsSuccess = false;
                bs.Message = e.Message;
                CommonMain.CreateErrorLog(e, "API:HttpDelete:Units");
            }
            return Json(bs.IsSuccess);
        }
        #endregion Unit
        #region ApprovalMartixes
        //[HttpGet]
        //public List<ApprovalMartixViewModel> ApprovalMartixes(string ProcessCode = "" , int MemberId = -1, string ApprovalMartixOptionCode = "")
        //{

        //    List<ApprovalMartixViewModel> results = new List<ApprovalMartixViewModel>();
        //    var ApprovalMartixes = OrganizationMain.GetApprovalMartix(ProcessCode, MemberId, ApprovalMartixOptionCode);

        //    foreach (var ApprovalMartix in ApprovalMartixes)
        //    {
        //        ApprovalMartixViewModel result = new ApprovalMartixViewModel();
        //        result.ApprovalMartixId = ApprovalMartix.ApprovalMartixId;
        //        result.ApprovalMartixName = ApprovalMartix.ApprovalMartixName;
        //        result.ProcessId = ApprovalMartix.ProcessId;
        //        result.UnitId = ApprovalMartix.UnitId;
        //        result.RoleId = ApprovalMartix.RoleId;
        //        result.CopyDetailFrom = ApprovalMartix.CopyDetailFrom;
        //        result.ApprovalMartixOptionCode = string.IsNullOrEmpty(ApprovalMartix.ApprovalMartixOptionCode)?string.Empty:ApprovalMartix.ApprovalMartixOptionCode;
        //        //result.FromAmount = ApprovalMartix.FromAmount;
        //        //result.ToAmount = ApprovalMartix.ToAmount;
        //        //get detail
        //        result.Details = new List<ApprovalMartixDetailViewModel>();
        //        var details = OrganizationMain.GetApprovalMartixDetails(ApprovalMartix.ApprovalMartixId).ToList();
        //        foreach (var detail in details)
        //        {
        //            ApprovalMartixDetailViewModel vm = new ApprovalMartixDetailViewModel();
        //            var level = OrganizationMain.GetApprovalLevels(detail.ApprovalLevelId).FirstOrDefault();
        //            vm.ApprovalCriteria = detail.ApprovalCriteria;
        //            vm.ApprovalCriteriaType = detail.ApprovalCriteriaType;
        //            vm.ApprovalMartixDetailId = detail.ApprovalMartixDetailId;
        //            vm.ApprovalMartixId = detail.ApprovalMartixId;
        //            vm.ApprovalLevelId = detail.ApprovalLevelId;
        //            vm.ApprovalLevelName = level.ApprovalLevelName;
        //            vm.ApprovalLevelCode = level.ApprovalLevelCode;
        //            vm.Seq = level.Seq;
        //            result.Details.Add(vm);
        //        }
        //        if (result.CopyDetailFrom.HasValue)
        //        {
        //            var detailsCopied = OrganizationMain.GetApprovalMartixDetails(result.CopyDetailFrom.Value).ToList();
        //            foreach (var detail in detailsCopied)
        //            {
        //                ApprovalMartixDetailViewModel vm = new ApprovalMartixDetailViewModel();
        //                var level = OrganizationMain.GetApprovalLevels(detail.ApprovalLevelId).FirstOrDefault();
        //                vm.ApprovalCriteria = detail.ApprovalCriteria;
        //                vm.ApprovalCriteriaType = detail.ApprovalCriteriaType;
        //                vm.ApprovalMartixDetailId = detail.ApprovalMartixDetailId;
        //                vm.ApprovalMartixId = detail.ApprovalMartixId;
        //                vm.ApprovalLevelId = detail.ApprovalLevelId;
        //                vm.ApprovalLevelName = level.ApprovalLevelName;
        //                vm.ApprovalLevelCode = level.ApprovalLevelCode;
        //                vm.Seq = level.Seq;
        //                if (result.Details.Where(x=>x.ApprovalLevelId== detail.ApprovalLevelId).Count()<=0)
        //                    result.Details.Add(vm);
        //            }
        //        }
        //        results.Add(result);
        //    }
        //    return results;

        //}
        #endregion
        #region Role
        [System.Web.Http.HttpGet]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController.Roles(int, int)'
        public List<Role> Roles(int RoleId = -1, int MemberId = -1)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController.Roles(int, int)'
        {
            if (RoleId > 0) return new List<Role>() { GetRole(RoleId) };
            if (MemberId > 0) return GetMemberRole(MemberId);
            return GetRoles();
        }
        private List<Role> GetRoles()
        {
            return OrganizationMain.GetRoles();
        }
        private Role GetRole(int RoleId)
        {
            return GetRole(RoleId);
        }
        private List<Role> GetMemberRole(int MemberId)
        {
            return OrganizationMain.GetRolesByMemberId(MemberId);
        }
        #endregion Role
        #region CostCenter
        [System.Web.Http.HttpGet]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController.CostCenters(int, int)'
        public List<CostCenter> CostCenters(int UnitId = -1, int MemberId = -1)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController.CostCenters(int, int)'
        {
            List<CostCenter> result = GetCostCenters();
            if (UnitId > 0)
                result = GetUnitCostCenter(UnitId);
            if (MemberId > 0)
                result = GetUserCostCenters(MemberId);

            return result;
        }
        private List<CostCenter> GetCostCenters()
        {
            return OrganizationMain.GetCostCenters();
        }
        private List<CostCenter> GetUnitCostCenter(int UnitId)
        {
            return OrganizationMain.GetUnitCostCenters(UnitId);
        }
        private List<CostCenter> GetUserCostCenters(int MemberId)
        {
            int UnitId = OrganizationMain.GetMember(MemberId).UnitId;
            return OrganizationMain.GetUnitCostCenters(UnitId);
        }
        #endregion CostCenter

        [System.Web.Http.HttpGet]
        private ApprovalMartixViewModel Approvers(int MemberId = -1, string ProcessCode = "", int ApprovalMartixId = -1)
        {
            if (MemberId < 0 || ApprovalMartixId < 0 || string.IsNullOrEmpty(ProcessCode))
                return new ApprovalMartixViewModel();
            //return NotFound();

            var _ApprovalMartixes = OrganizationMain.GetApprovalMartix(ApprovalMartixId);

            ApprovalMartixViewModel result = new ApprovalMartixViewModel();
            result.ApprovalMartixId = _ApprovalMartixes.ApprovalMartixId;
            result.ApprovalMartixName = _ApprovalMartixes.ApprovalMartixName;
            result.ProcessId = _ApprovalMartixes.ProcessId;
            result.UnitId = _ApprovalMartixes.UnitId;
            result.RoleId = _ApprovalMartixes.RoleId;
            result.CopyDetailFrom = _ApprovalMartixes.CopyDetailFrom;
            //result.FromAmount = _ApprovalMartixes.FromAmount;
            //result.ToAmount = _ApprovalMartixes.ToAmount;
            result.ApprovalMartixOptionCode = string.IsNullOrEmpty(_ApprovalMartixes.ApprovalMartixOptionCode) ? string.Empty : _ApprovalMartixes.ApprovalMartixOptionCode;
            //get detail
            result.Details = new List<ApprovalMartixDetailViewModel>();
            var details = OrganizationMain.GetApprovalMartixDetails(_ApprovalMartixes.ApprovalMartixId).ToList();
            foreach (var detail in details)
            {
                ApprovalMartixDetailViewModel vm = new ApprovalMartixDetailViewModel();
                var level = OrganizationMain.GetApprovalLevels(detail.ApprovalLevelId).FirstOrDefault();
                vm.ApprovalCriteria = detail.ApprovalCriteria;
                vm.ApprovalCriteriaType = detail.ApprovalCriteriaType;
                vm.ApprovalMartixDetailId = detail.ApprovalMartixDetailId;
                vm.ApprovalMartixId = detail.ApprovalMartixId;
                vm.ApprovalLevelId = detail.ApprovalLevelId;
                vm.ApprovalLevelName = level.ApprovalLevelName;
                vm.ApprovalLevelCode = level.ApprovalLevelCode;
                vm.Seq = level.Seq;
                vm.Approvers = OrganizationMain.GetApprover(MemberId, vm.ApprovalCriteriaType, vm.ApprovalCriteria);
                result.Details.Add(vm);
            }


            return result;



        }


        [System.Web.Http.HttpGet]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController.ApprovalMartixApprovers(int, string, string)'
        public ApprovalMartixViewModel ApprovalMartixApprovers(int MemberId = -1, string ProcessCode = "", string ApprovalMartixOptionCode = "")
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController.ApprovalMartixApprovers(int, string, string)'
        {
            if (MemberId < 0 || string.IsNullOrEmpty(ProcessCode))
                return new ApprovalMartixViewModel();
			//return NotFound();

			var member = GetMember(MemberId);
			MEHK.BLL.CommonBLL WFCommonBLL = new MEHK.BLL.CommonBLL();
			var _loginDomain = WFCommonBLL.GetSystemParameter("loginDomain");
			string memberAD = _loginDomain + "\\" + member.SAMAccount;

			var _ApprovalMartixes = OrganizationMain.GetApprovalMartix(ProcessCode, MemberId, ApprovalMartixOptionCode).FirstOrDefault();
            if (_ApprovalMartixes == null)
                return new ApprovalMartixViewModel();
            ApprovalMartixViewModel result = new ApprovalMartixViewModel();
            result.ApprovalMartixId = _ApprovalMartixes.ApprovalMartixId;
            result.ApprovalMartixName = _ApprovalMartixes.ApprovalMartixName;
            result.ProcessId = _ApprovalMartixes.ProcessId;
            result.UnitId = _ApprovalMartixes.UnitId;
            result.RoleId = _ApprovalMartixes.RoleId;
            result.CopyDetailFrom = _ApprovalMartixes.CopyDetailFrom;
            //result.FromAmount = _ApprovalMartixes.FromAmount;
            //result.ToAmount = _ApprovalMartixes.ToAmount;
            result.ApprovalMartixOptionCode = string.IsNullOrEmpty(_ApprovalMartixes.ApprovalMartixOptionCode) ? string.Empty : _ApprovalMartixes.ApprovalMartixOptionCode;
            //get detail
            result.Details = new List<ApprovalMartixDetailViewModel>();
            var details = OrganizationMain.GetApprovalMartixDetails(_ApprovalMartixes.ApprovalMartixId).ToList();
            foreach (var detail in details)
            {
                ApprovalMartixDetailViewModel vm = new ApprovalMartixDetailViewModel();
                var level = OrganizationMain.GetApprovalLevels(detail.ApprovalLevelId).FirstOrDefault();
                vm.ApprovalCriteria = detail.ApprovalCriteria;
                vm.ApprovalCriteriaType = detail.ApprovalCriteriaType;
                vm.ApprovalMartixDetailId = detail.ApprovalMartixDetailId;
                vm.ApprovalMartixId = detail.ApprovalMartixId;
                vm.ApprovalLevelId = detail.ApprovalLevelId;
                vm.ApprovalLevelName = level.ApprovalLevelName;
                vm.ApprovalLevelCode = level.ApprovalLevelCode;
                vm.Seq = level.Seq;
                List<ApproversDetails> approverDetails = new List<ApproversDetails>();

                vm.Approvers = OrganizationMain.GetApprover(MemberId, vm.ApprovalCriteriaType, vm.ApprovalCriteria);
                vm.ApproversFulList = OrganizationMain.GetADFullListName(vm.Approvers, ref approverDetails);

                vm.ApproverDetails = approverDetails.Where(e=>e.ADName!= memberAD).ToList();
                result.Details.Add(vm);
            }


            return result;



        }

        //public List<CostCenter> GetWorkflowApprovers1(int MemberId, int WorkflowId, int ApprovalMartixId, int joblevel, int RoleID)
        //{
        //    int UnitId = OrganizationMain.GetMember(MemberId).UnitId;
        //    return OrganizationMain.GetUnitCostCenters(UnitId);
        //}

        [System.Web.Http.HttpGet]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController.Delegations(int, int, int)'
        public List<Deputy> Delegations(int WorkflowId = -1, int MemberId = -1, int DelegateMemberId = -1)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'OrganizationController.Delegations(int, int, int)'
        {
            var fullList = OrganizationMain.GetDelegationList();
            if (WorkflowId > 0)
                fullList = fullList.Where(x => x.ProcessId == WorkflowId).ToList();
            if (MemberId > 0)
                fullList = fullList.Where(x => x.MemberId == MemberId).ToList();
            if (DelegateMemberId > 0)
                fullList = fullList.Where(x => x.DelegateeMemberId == DelegateMemberId).ToList();
            return fullList;
        }

        [System.Web.Http.HttpPut]
        public List<ManageViewModels> CreateMember([FromBody]ManageViewModels vms)
        {
            List<UserInfo> lui = new List<UserInfo>();
            UserInfo userInfo = new UserInfo();
            userInfo.MemberId = vms.MemberId;
            userInfo.MemberName = vms.MemberName;
            userInfo.LoginAccount = vms.LoginAccount;
            userInfo.StaffId = vms.StaffId;
            userInfo.SAMAccount = vms.SAMAccount;
            userInfo.SAPAccount = vms.SAPAccount;
            userInfo.Phone = vms.Phone;
            userInfo.EmailAddress = vms.EmailAddress;
            userInfo.JobLevelId = vms.JobLevelId;
            userInfo.JobRank = vms.JobRank;
            userInfo.Remark = vms.Remark;
            lui.Add(userInfo);
            List<UserInfo> errolui = new List<UserInfo>();
            foreach (var ui in lui)
            {
                if (ui.MemberId == 0)
                {

                    var result = OrganizationMain.CreateMember(ui);
                    if (result.MemberId == 0)
                    {
                        errolui.Add(ui);
                    }
                    else
                    {
                        vms.MemberId = result.MemberId;
                    }
                }
                else
                {
                    errolui.Add(ui);
                }
            }
            if (errolui.Count() > 0)
            {
                return null;
            }
            else
            {
                List<ManageViewModels> ml = new List<ManageViewModels>();
                ml.Add(vms);

                return ml;
            }
        }
        //[System.Web.Http.Route("api/Organization/GetGroup")]
        [System.Web.Http.HttpGet]
        public List<GroupViewModels> GetGroup(int Id = -1)
        {
            if (Id > 0)
            {

                return OrganizationMain.Getgroup().Where(e => e.ID == Id).Select(e => new GroupViewModels
                {
                    ID = e.ID,
                    GroupName = e.GroupName,
                    Description = e.Description
                }).ToList();
            }
            else
            {
                return OrganizationMain.Getgroup().Select(e => new GroupViewModels
                {
                    ID = e.ID,
                    GroupName = e.GroupName,
                    Description = e.Description
                }).ToList();
            }
        }
        [System.Web.Http.HttpPut]
        public List<GroupViewModels> CreateGroup([FromBody]GroupViewModels vms)
        {
            List<Group> lgr = new List<Group>();
            Group Group = new Group();
            Group.ID = vms.ID;
            Group.GroupName = vms.GroupName;
            Group.Description = vms.Description;
            Group.IsDeleted = false;
            Group.CreatedDate = DateTime.Now;
            lgr.Add(Group);
            List<Group> errolui = new List<Group>();
            foreach (var gr in lgr)
            {
                if (gr.ID == 0)
                {

                    var result = OrganizationMain.CreateGroup(gr);
                    if (result == null)
                    {
                        errolui.Add(gr);
                    }
                    else
                    {
                        vms.ID = result.ID;
                    }
                }
                else
                {
                    errolui.Add(gr);
                }
            }
            if (errolui.Count() > 0)
            {
                return null;
            }
            else
            {
                List<GroupViewModels> ml = new List<GroupViewModels>();
                ml.Add(vms);
                return ml;
            }
        }

        [System.Web.Http.HttpPut]
        public List<GroupViewModels> UpdateGroup([FromBody]GroupViewModels vms)
        {
            List<Group> lgr = new List<Group>();
            Group Group = new Group();
            Group.ID = vms.ID;
            Group.GroupName = vms.GroupName;
            Group.Description = vms.Description;
            Group.IsDeleted = false;
            Group.ModifiedDate = DateTime.Now;
            lgr.Add(Group);
            List<Group> errolui = new List<Group>();
            foreach (var gr in lgr)
            {
                if (gr.ID != 0)
                {

                    var result = OrganizationMain.UpdateGroup(gr);
                    if (result == null)
                        errolui.Add(gr);
                }
                else
                {
                    errolui.Add(gr);
                }
            }
            if (errolui.Count() > 0)
            {
                return null;
            }
            else
            {
                List<GroupViewModels> ml = new List<GroupViewModels>();
                ml = OrganizationMain.Getgroup().Where(e => e.ID == vms.ID).Select(e => new GroupViewModels
                {
                    ID = e.ID,
                    GroupName = e.GroupName,
                    Description = e.Description
                }).ToList();
                return ml;
            }
        }
        [System.Web.Http.HttpDelete]
        public List<GroupViewModels> DeleteGroup([FromBody]GroupViewModels vms)
        {
            List<Group> lgr = new List<Group>();
            Group Group = new Group();
            Group.ID = vms.ID;
            Group.GroupName = vms.GroupName;
            Group.Description = vms.Description;
            Group.IsDeleted = true;
            Group.ModifiedDate = DateTime.Now;
            lgr.Add(Group);
            List<Group> errolui = new List<Group>();
            foreach (var gr in lgr)
            {
                if (gr.ID != 0)
                {

                    var result = OrganizationMain.UpdateGroup(gr);
                    if (result == null)
                        errolui.Add(gr);
                }
                else
                {
                    errolui.Add(gr);
                }
            }
            if (errolui.Count() > 0)
            {
                return null;
            }
            else
            {
                List<GroupViewModels> ml = new List<GroupViewModels>();
                ml = OrganizationMain.Getgroup().Where(e => e.ID == vms.ID).Select(e => new GroupViewModels
                {
                    ID = e.ID,
                    GroupName = e.GroupName,
                    Description = e.Description
                }).ToList();
                return ml;
            }
        }
        [System.Web.Http.HttpGet]
        public List<GroupMemberViewModel> GetGroupMemberList(int nid)
        {
            return OrganizationMain.GetGroupMemberList(nid);
        }
        [System.Web.Http.HttpGet]
        public List<MemberGroupViewModel> GetMemberGroupList(int nid)
        {
            return OrganizationMain.GetMemberGroupList(nid);
        }
        [System.Web.Http.HttpGet]
        public List<GroupMemberViewModel> UpdateGroupMemberList(string models)
        {
            List<GroupMemberViewModel> ret = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GroupMemberViewModel>>(models);
            try
            {
                if (ret != null)
                {
                    ret = OrganizationMain.UpdateGroupMemberList(ret);
                }
            }
            catch (Exception e)
            {
            }
            return ret;
        }
        [System.Web.Http.HttpGet]
        public List<MemberGroupViewModel> UpdateMemberGroupList(string models)
        {
            List<MemberGroupViewModel> ret = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MemberGroupViewModel>>(models);
            try
            {
                if (ret != null)
                {
                    ret = OrganizationMain.UpdateMemberGroupList(ret);
                }
            }
            catch (Exception e)
            {
            }
            return ret;
        }
        [System.Web.Http.HttpGet]
        public List<GroupRightViewModel> GetGroupRightList(int nid)
        {
            return OrganizationMain.GetGroupRightList(nid);
        }
        [System.Web.Http.HttpGet]
        public List<RightGroupViewModel> GetRightGroupList(int nid)
        {
            return OrganizationMain.GetRightGroupList(nid);
        }
        [System.Web.Http.HttpGet]
        public List<GroupRightViewModel> UpdateGroupRightList(string models)
        {
            List<GroupRightViewModel> ret = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GroupRightViewModel>>(models);
            try
            {
                if (ret != null)
                {
                    ret = OrganizationMain.UpdateGroupRightList(ret);
                }
            }
            catch (Exception e)
            {
            }
            return ret;
        }
        [System.Web.Http.HttpGet]
        public List<RightGroupViewModel> UpdateRightGroupList(string models)
        {
            List<RightGroupViewModel> ret = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RightGroupViewModel>>(models);
            try
            {
                if (ret != null)
                {
                    ret = OrganizationMain.UpdateRightGroupList(ret);
                }
            }
            catch (Exception e)
            {
            }
            return ret;
        }
        [System.Web.Http.HttpGet]
        public List<RightViewModels> GetRight(int Id = -1)
        {
            if (Id > 0)
            {

                return OrganizationMain.GetRight().Where(e => e.ID == Id).Select(e => new RightViewModels
                {
                    ID = e.ID,
                    RightName = e.RightName,
                    Description = e.Description,
                    AppID = e.AppID,
                    AppName = e.AppName
                }).ToList();
            }
            else
            {
                return OrganizationMain.GetRight().Select(e => new RightViewModels
                {
                    ID = e.ID,
                    RightName = e.RightName,
                    Description = e.Description,
                    AppID = e.AppID,
                    AppName = e.AppName
                }).ToList();
            }
        }
		[System.Web.Http.HttpPut]
		public List<RightViewModels> CreateRight([FromBody]RightViewModels vms)
		{
			//CommonMain.CreateErrorLog(null, "CreateRight");
			List<RightViewModels> resultuf = new List<RightViewModels>();
			try
			{
				Right Right = new Right();
				Right.ID = vms.ID;
				Right.RightName = vms.RightName;
				Right.Description = vms.Description;
				Right.AppID = vms.AppID;
				Right.CreatedDate = DateTime.Now;
				Right.IsDeleted = false;
				if (Right.ID == 0)
				{

					var result = OrganizationMain.CreateRight(Right);
					if (result.ID != 0)
					{
						vms = OrganizationMain.GetRight().Where(e => e.ID == result.ID).Select(e => new RightViewModels
						{
							ID = e.ID,
							RightName = e.RightName,
							Description = e.Description,
							AppID = e.AppID,
							AppName = e.AppName
						}).FirstOrDefault();
						resultuf.Add(vms);
					}
				}
			}
			catch (Exception e)
			{
				CommonMain.CreateErrorLog(e, "CreateRight");
			}
			return resultuf;


		}

        [System.Web.Http.HttpPut]
        public List<RightViewModels> UpdateRight([FromBody]RightViewModels vms)
        {
            List<Right> lgr = new List<Right>();
            Right Right = new Right();
            Right.ID = vms.ID;
            Right.RightName = vms.RightName;
            Right.Description = vms.Description;
            Right.AppID = vms.AppID == null ? 0 : (int)vms.AppID;
            Right.ModifiedDate = DateTime.Now;
            lgr.Add(Right);
            List<Right> errolui = new List<Right>();
            foreach (var gr in lgr)
            {
                if (gr.ID != 0)
                {

                    var result = OrganizationMain.UpdateRight(gr);
                    if (result == null)

                        errolui.Add(gr);
                }
                else
                {
                    errolui.Add(gr);
                }
            }
            if (errolui.Count() > 0)
            {
                return null;
            }
            else
            {
                List<RightViewModels> ml = new List<RightViewModels>();
                vms = OrganizationMain.GetRight().Where(e => e.ID == vms.ID).Select(e => new RightViewModels
                {
                    ID = e.ID,
                    RightName = e.RightName,
                    Description = e.Description,
                    AppID = e.AppID,
                    AppName = e.AppName
                }).FirstOrDefault();

                ml.Add(vms);
                return ml;
            }
        }
        [System.Web.Http.HttpDelete]
        public List<RightViewModels> DeleteRight([FromBody]RightViewModels vms)
        {
            List<Right> lgr = new List<Right>();
            Right Right = new Right();
            Right.ID = vms.ID;
            Right.RightName = vms.RightName;
            Right.Description = vms.Description;
            Right.AppID = vms.AppID == null ? 0 : (int)vms.AppID;
            Right.ModifiedDate = DateTime.Now;
            Right.IsDeleted = true;
            lgr.Add(Right);
            List<Right> errolui = new List<Right>();
            foreach (var gr in lgr)
            {
                if (gr.ID != 0)
                {

                    var result = OrganizationMain.UpdateRight(gr);
                    if (result == null)
                        errolui.Add(gr);
                }
                else
                {
                    errolui.Add(gr);
                }
            }
            if (errolui.Count() > 0)
            {
                return null;
            }
            else
            {
                List<RightViewModels> ml = new List<RightViewModels>();
                ml.Add(vms);
                return ml;
            }
        }




        [System.Web.Http.HttpGet]
        public List<ApplicationViewModels> GetApplication(int Id = -1)
        {
            if (Id > 0)
            {

                return OrganizationMain.GetApplication().Where(e => e.ID == Id).Select(e => new ApplicationViewModels
                {
                    ID = e.ID,
                    AppCode = e.AppCode,
                    AppName = e.AppName,
                    Description = e.Description
                }).ToList();
            }
            else
            {
                return OrganizationMain.GetApplication().Select(e => new ApplicationViewModels
                {
                    ID = e.ID,
                    AppCode = e.AppCode,
                    AppName = e.AppName,
                    Description = e.Description
                }).ToList();
            }
        }
        public List<ApprovalMartixDetailModel> GetApprovalMartixDetail(int Id)
        {
            return OrganizationMain.GetApprovalMartixDetail().Where(r => r.ApprovalMartixId == Id).Select(e => new ApprovalMartixDetailModel
            {
                ApprovalMartixDetailId = e.ApprovalMartixDetailId,
                ApprovalMartixId = e.ApprovalMartixId,
                ApprovalLevelId = e.ApprovalLevelId,
                ApprovalCriteriaType = e.ApprovalCriteriaType,
                ApprovalCriteria = e.ApprovalCriteria,
                ApprovalLevelName = e.ApprovalLevelName,
                ApprovalLevelCode = e.ApprovalLevelCode,
                Description = e.Description,

            }).ToList();
        }


        [System.Web.Http.HttpGet]
        public List<ApprovalMartixViewModels> GetApprovalMartix(int Id = -1)
        {
            List<ApprovalMartixViewModels> list = new List<ApprovalMartixViewModels>();
            list = OrganizationMain.GetApprovalMartix().Select(e => new ApprovalMartixViewModels
            {
                ApprovalMartixId = e.ApprovalMartixId,
                ApprovalMartixName = e.ApprovalMartixName,
                ProcessId = e.ProcessId,
                ProcessName = e.ProcessName,
                UnitId = e.UnitId == null ? 0 : (int)e.UnitId,
                UnitName = e.UnitName,
                RoleId = e.RoleId,
                RoleName = e.RoleName,
                Priority = e.Priority,
                //CopyDetailFrom=e.CopyDetailFrom,
                ApprovalMartixOptionCode = e.ApprovalMartixOptionCode,
                ApprovalMartixOptionName = e.ApprovalMartixOptionName,
                ApprovalMartixDetailList = GetApprovalMartixDetail(e.ApprovalMartixId),
                //Supervisor1_Criteria = GetApprovalMartixDetail(e.ApprovalMartixId)[0].ApprovalCriteria,
                //Supervisor1_CriteriaType = GetApprovalMartixDetail(e.ApprovalMartixId)[0].ApprovalCriteriaType,
                //Supervisor2_Criteria = GetApprovalMartixDetail(e.ApprovalMartixId)[1].ApprovalCriteria,
                //Supervisor2_CriteriaType = GetApprovalMartixDetail(e.ApprovalMartixId)[1].ApprovalCriteriaType,
                //AssistantManger_Criteria = GetApprovalMartixDetail(e.ApprovalMartixId)[2].ApprovalCriteria,
                //AssistantManger_CriteriaType = GetApprovalMartixDetail(e.ApprovalMartixId)[2].ApprovalCriteriaType,
                //DepartmentManager_Criteria = GetApprovalMartixDetail(e.ApprovalMartixId)[3].ApprovalCriteria,
                //DepartmentManager_CriteriaType = GetApprovalMartixDetail(e.ApprovalMartixId)[3].ApprovalCriteriaType,
                //SeniorManager_Criteria = GetApprovalMartixDetail(e.ApprovalMartixId)[4].ApprovalCriteria,
                //SeniorManager_CriteriaType = GetApprovalMartixDetail(e.ApprovalMartixId)[4].ApprovalCriteriaType,
                //DepartmentGeneralManager_Criteria = GetApprovalMartixDetail(e.ApprovalMartixId)[5].ApprovalCriteria,
                //DepartmentGeneralManager_CriteriaType = GetApprovalMartixDetail(e.ApprovalMartixId)[5].ApprovalCriteriaType,
                //GeneralManager_Criteria = GetApprovalMartixDetail(e.ApprovalMartixId)[6].ApprovalCriteria,
                //GeneralManager_CriteriaType = GetApprovalMartixDetail(e.ApprovalMartixId)[6].ApprovalCriteriaType,
                //GPAD_Criteria = GetApprovalMartixDetail(e.ApprovalMartixId)[7].ApprovalCriteria,
                //GPAD_CriteriaType = GetApprovalMartixDetail(e.ApprovalMartixId)[7].ApprovalCriteriaType,
                //MD_Criteria = GetApprovalMartixDetail(e.ApprovalMartixId)[8].ApprovalCriteria,
                //MD_CriteriaType = GetApprovalMartixDetail(e.ApprovalMartixId)[8].ApprovalCriteriaType,
                //ACVerification_Criteria = GetApprovalMartixDetail(e.ApprovalMartixId)[9].ApprovalCriteria,
                //ACVerification_CriteriaType = GetApprovalMartixDetail(e.ApprovalMartixId)[9].ApprovalCriteriaType,
                //ACVerification2_Criteria = GetApprovalMartixDetail(e.ApprovalMartixId)[10].ApprovalCriteria,
                //ACVerification2_CriteriaType = GetApprovalMartixDetail(e.ApprovalMartixId)[10].ApprovalCriteriaType,
                //ACApproval_Criteria = GetApprovalMartixDetail(e.ApprovalMartixId)[11].ApprovalCriteria,
                //ACApproval_CriteriaType = GetApprovalMartixDetail(e.ApprovalMartixId)[11].ApprovalCriteriaType,
                //HRVerification_Criteria = GetApprovalMartixDetail(e.ApprovalMartixId)[12].ApprovalCriteria,
                //HRVerification_CriteriaType = GetApprovalMartixDetail(e.ApprovalMartixId)[12].ApprovalCriteriaType,
                //HRVerification2_Criteria = GetApprovalMartixDetail(e.ApprovalMartixId)[13].ApprovalCriteria,
                //HRVerification2_CriteriaType = GetApprovalMartixDetail(e.ApprovalMartixId)[13].ApprovalCriteriaType,
                //HRApprova_Criteria = GetApprovalMartixDetail(e.ApprovalMartixId)[14].ApprovalCriteria,
                //HRApprova_CriteriaType = GetApprovalMartixDetail(e.ApprovalMartixId)[14].ApprovalCriteriaType,

            }).ToList();

            if (Id > 0)
            {

                return list.Where(e => e.ApprovalMartixId == Id).ToList();
            }
            else
            {
                return list;
            }
        }
        [System.Web.Http.HttpPut]
        public List<ApprovalMartixViewModels> CreateApprovalMartix([FromBody]ApprovalMartixViewModels vms)
        {

            ApprovalMartix ApprovalMartix = new ApprovalMartix();
            ApprovalMartix.ApprovalMartixId = vms.ApprovalMartixId;
            ApprovalMartix.ApprovalMartixName = vms.ApprovalMartixName;
            ApprovalMartix.ProcessId = vms.ProcessId;
            ApprovalMartix.UnitId = vms.UnitId;
            ApprovalMartix.RoleId = vms.RoleId;
            ApprovalMartix.Priority = vms.Priority;
            ApprovalMartix.CopyDetailFrom = vms.CopyDetailFrom;
            ApprovalMartix.ApprovalMartixOptionCode = vms.ApprovalMartixOptionCode;
            ApprovalMartix.CreatedDate = DateTime.Now;
            ApprovalMartix.CreatedBy = "sa";
            ApprovalMartix.IsDeleted = false;

            List<ApprovalMartix> errolui = new List<ApprovalMartix>();

            if (ApprovalMartix.ApprovalMartixId == 0)
            {

                ApprovalMartix = OrganizationMain.CreateApprovalMartix(ApprovalMartix);

            }
            else
            {
                errolui.Add(ApprovalMartix);
            }

            if (errolui.Count() > 0)
            {
                return null;
            }
            else
            {
                List<ApprovalMartixDetailModel> list = new List<ApprovalMartixDetailModel>();

                list = CreateApprovalMartixDetail(vms.ApprovalMartixDetailList, ApprovalMartix.ApprovalMartixId);

                List<ApprovalMartixViewModels> ml = new List<ApprovalMartixViewModels>();
                vms = OrganizationMain.GetApprovalMartix().Where(e => e.ApprovalMartixId == ApprovalMartix.ApprovalMartixId).Select(e => new ApprovalMartixViewModels
                {
                    ApprovalMartixId = e.ApprovalMartixId,
                    ApprovalMartixName = e.ApprovalMartixName,
                    ProcessId = e.ProcessId,
                    ProcessName = e.ProcessName,
                    UnitId = e.UnitId == null ? 0 : (int)e.UnitId,
                    UnitName = e.UnitName,
                    RoleId = e.RoleId,
                    RoleName = e.RoleName,
                    Priority = e.Priority,
                    //CopyDetailFrom=e.CopyDetailFrom,
                    ApprovalMartixOptionCode = e.ApprovalMartixOptionCode,
                    ApprovalMartixOptionName = e.ApprovalMartixOptionName,
                    ApprovalMartixDetailList = list
                }).FirstOrDefault();
                ml.Add(vms);
                return ml;
            }
        }
        public List<ApprovalMartixDetailModel> CreateApprovalMartixDetail(List<ApprovalMartixDetailModel> list, int ApprovalMartixId)
        {
            List<ApprovalMartixDetail> DBList = new List<ApprovalMartixDetail>();
            foreach (var item in list)
            {
                if (item.ApprovalCriteriaType != null && item.ApprovalCriteriaType != "" && item.ApprovalCriteria != null && item.ApprovalCriteria != "")
                {
                    ApprovalMartixDetail model = new ApprovalMartixDetail();
                    model.ApprovalMartixDetailId = item.ApprovalMartixDetailId;
                    model.ApprovalMartixId = ApprovalMartixId;
                    model.ApprovalLevelId = item.ApprovalLevelId == null ? 0 : (int)item.ApprovalLevelId;
                    model.ApprovalCriteriaType = item.ApprovalCriteriaType;
                    model.ApprovalCriteria = item.ApprovalCriteria;
                    model.IsDeleted = false;
                    model.CreatedBy = "sa";
                    model.CreatedDate = DateTime.Now;
                    model = OrganizationMain.CreateApprovalMartixDetail(model);
                    DBList.Add(model);
                }
            }
            return DBList.Select(e => new ApprovalMartixDetailModel
            {
                ApprovalMartixDetailId = e.ApprovalMartixDetailId,
                ApprovalMartixId = e.ApprovalMartixId,
                ApprovalLevelId = e.ApprovalLevelId,
                ApprovalCriteriaType = e.ApprovalCriteriaType,
                ApprovalCriteria = e.ApprovalCriteria
            }).ToList();
        }
        [System.Web.Http.HttpPut]
        public List<ApprovalMartixViewModels> UpdateApprovalMartix([FromBody]ApprovalMartixViewModels vms)
        {
            List<ApprovalMartix> lgr = new List<ApprovalMartix>();
            ApprovalMartix ApprovalMartix = new ApprovalMartix();
            ApprovalMartix.ApprovalMartixId = vms.ApprovalMartixId;
            ApprovalMartix.ApprovalMartixName = vms.ApprovalMartixName;
            ApprovalMartix.ProcessId = vms.ProcessId;
            ApprovalMartix.UnitId = vms.UnitId;
            ApprovalMartix.RoleId = vms.RoleId;
            ApprovalMartix.Priority = vms.Priority;
            ApprovalMartix.CopyDetailFrom = vms.CopyDetailFrom;
            ApprovalMartix.ApprovalMartixOptionCode = vms.ApprovalMartixOptionCode;
            ApprovalMartix.IsDeleted = false;
            ApprovalMartix.CreatedBy = "sa";
            ApprovalMartix.CreatedDate = DateTime.Now;
            lgr.Add(ApprovalMartix);
            List<ApprovalMartix> errolui = new List<ApprovalMartix>();
            foreach (var gr in lgr)
            {
                if (gr.ApprovalMartixId != 0)
                {

                    var result = OrganizationMain.UpdateApprovalMartix(gr);
                    if (result == null)
                        errolui.Add(gr);
                }
                else
                {
                    errolui.Add(gr);
                }
            }
            if (errolui.Count() > 0)
            {
                return errolui.Select(e => new ApprovalMartixViewModels
                {
                    ApprovalMartixId = e.ApprovalMartixId,
                    ApprovalMartixName = e.ApprovalMartixName,
                    ProcessId = e.ProcessId
                }).ToList();
            }
            else
            {
                List<ApprovalMartixDetailModel> list = new List<ApprovalMartixDetailModel>();

                list = UpdateApprovalMartixDetail(vms.ApprovalMartixDetailList, false, vms.ApprovalMartixId);
                List<ApprovalMartixViewModels> ml = new List<ApprovalMartixViewModels>();
                vms = OrganizationMain.GetApprovalMartix().Where(e => e.ApprovalMartixId == vms.ApprovalMartixId).Select(e => new ApprovalMartixViewModels
                {
                    ApprovalMartixId = e.ApprovalMartixId,
                    ApprovalMartixName = e.ApprovalMartixName,
                    ProcessId = e.ProcessId,
                    ProcessName = e.ProcessName,
                    UnitId = e.UnitId == null ? 0 : (int)e.UnitId,
                    UnitName = e.UnitName,
                    RoleId = e.RoleId,
                    RoleName = e.RoleName,
                    Priority = e.Priority,
                    //CopyDetailFrom=e.CopyDetailFrom,
                    ApprovalMartixOptionCode = e.ApprovalMartixOptionCode,
                    ApprovalMartixOptionName = e.ApprovalMartixOptionName,
                    ApprovalMartixDetailList = list
                }).FirstOrDefault();
                ml.Add(vms);
                return ml;
            }
        }
        public List<ApprovalMartixDetailModel> UpdateApprovalMartixDetail(List<ApprovalMartixDetailModel> list, bool IsDeleted, int ApprovalMartixId)
        {
            List<ApprovalMartixDetail> DBList = new List<ApprovalMartixDetail>();

            List<ApprovalMartixDetail> deleteList = GetApprovalMartixDetail(ApprovalMartixId).Select(e => new ApprovalMartixDetail
            {
                ApprovalMartixDetailId = e.ApprovalMartixDetailId,
                ApprovalMartixId = e.ApprovalMartixId,
                ApprovalLevelId = e.ApprovalLevelId == null ? 0 : (int)e.ApprovalLevelId,
                ApprovalCriteriaType = e.ApprovalCriteriaType,
                ApprovalCriteria = e.ApprovalCriteria,
                IsDeleted = true,
                CreatedBy = "sa",
                CreatedDate = DateTime.Now,
            }).ToList();
            foreach (var item in deleteList)
            {
                OrganizationMain.UpdateApprovalMartixDetail(item);
            }
            if (!IsDeleted)
            {
                foreach (var item in list)
                {
                    if (item.ApprovalCriteriaType != null && item.ApprovalCriteriaType != "" && item.ApprovalCriteria != null && item.ApprovalCriteria != "")
                    {
                        ApprovalMartixDetail model = new ApprovalMartixDetail();
                        model.ApprovalMartixDetailId = item.ApprovalMartixDetailId;
                        model.ApprovalMartixId = item.ApprovalMartixId;
                        model.ApprovalLevelId = item.ApprovalLevelId == null ? 0 : (int)item.ApprovalLevelId;
                        model.ApprovalCriteriaType = item.ApprovalCriteriaType;
                        model.ApprovalCriteria = item.ApprovalCriteria;
                        model.IsDeleted = IsDeleted;
                        model.CreatedBy = "sa";
                        model.CreatedDate = DateTime.Now;
                        model = OrganizationMain.CreateApprovalMartixDetail(model);
                        DBList.Add(model);
                    }

                }
            }
            return DBList.Select(e => new ApprovalMartixDetailModel
            {
                ApprovalMartixDetailId = e.ApprovalMartixDetailId,
                ApprovalMartixId = e.ApprovalMartixId,
                ApprovalLevelId = e.ApprovalLevelId,
                ApprovalCriteriaType = e.ApprovalCriteriaType,
                ApprovalCriteria = e.ApprovalCriteria
            }).ToList();
        }
        [System.Web.Http.HttpDelete]
        public List<ApprovalMartixViewModels> DeleteApprovalMartix([FromBody]ApprovalMartixViewModels vms)
        {
            List<ApprovalMartix> lgr = new List<ApprovalMartix>();
            ApprovalMartix ApprovalMartix = new ApprovalMartix();
            ApprovalMartix.ApprovalMartixId = vms.ApprovalMartixId;
            ApprovalMartix.ApprovalMartixName = vms.ApprovalMartixName;
            ApprovalMartix.ProcessId = vms.ProcessId;
            ApprovalMartix.UnitId = vms.UnitId;
            ApprovalMartix.RoleId = vms.RoleId;
            ApprovalMartix.Priority = vms.Priority;
            ApprovalMartix.CopyDetailFrom = vms.CopyDetailFrom;
            ApprovalMartix.ApprovalMartixOptionCode = vms.ApprovalMartixOptionCode;
            ApprovalMartix.IsDeleted = true;
            ApprovalMartix.CreatedBy = "sa";
            ApprovalMartix.CreatedDate = DateTime.Now;
            lgr.Add(ApprovalMartix);
            List<ApprovalMartix> errolui = new List<ApprovalMartix>();
            foreach (var gr in lgr)
            {
                if (gr.ApprovalMartixId != 0)
                {

                    var result = OrganizationMain.UpdateApprovalMartix(gr);
                    if (result == null)
                        errolui.Add(gr);
                }
                else
                {
                    errolui.Add(gr);
                }
            }
            if (errolui.Count() > 0)
            {
                return null;
            }
            else
            {
                UpdateApprovalMartixDetail(vms.ApprovalMartixDetailList, true, vms.ApprovalMartixId);
                List<ApprovalMartixViewModels> ml = new List<ApprovalMartixViewModels>();
                ml.Add(vms);
                return ml;
            }
        }
        [System.Web.Http.HttpGet]
        public List<ProcessListViewModels> GetProcessList(int? Id = -1)
        {
            if (Id > 0)
            {

                return OrganizationMain.GetProcessList().Where(e => e.ProcessId == Id).Select(e => new ProcessListViewModels
                {
                    ProcessId = e.ProcessId,
                    Code = e.Code,
                    ProcessName = e.ProcessName,
					K2ProcessName=e.K2ProcessName
				}).ToList();
            }
            else
            {
                return OrganizationMain.GetProcessList().Select(e => new ProcessListViewModels
                {
                    ProcessId = e.ProcessId,
                    Code = e.Code,
                    ProcessName = e.ProcessName,
					K2ProcessName = e.K2ProcessName
				}).ToList();
            }
        }
        [System.Web.Http.HttpGet]
        public List<UnitViewModels> GetUnitList(int Id = -1)
        {
            if (Id > 0)
            {

                return OrganizationMain.GetUnitList().Where(e => e.UnitId == Id).Select(e => new UnitViewModels
                {
                    UnitId = e.UnitId,
                    UnitName = e.UnitName,

                }).ToList();
            }
            else
            {
                return OrganizationMain.GetUnitList().Select(e => new UnitViewModels
                {
                    UnitId = e.UnitId,
                    UnitName = e.UnitName,
                }).ToList();
            }
        }

        [System.Web.Http.HttpGet]
        public List<RoleViewModels> GetRoleList(int Id = -1)
        {
            if (Id > 0)
            {

                return OrganizationMain.GetRoleList().Where(e => e.RoletId == Id).Select(e => new RoleViewModels
                {
                    RoletId = e.RoletId,
                    RoleName = e.RoleName,
                    Remark = e.Remark
                }).ToList();
            }
            else
            {
                return OrganizationMain.GetRoleList().Select(e => new RoleViewModels
                {
                    RoletId = e.RoletId,
                    RoleName = e.RoleName,
                    Remark = e.Remark
                }).ToList();
            }
        }
        [System.Web.Http.HttpGet]
        public List<ApprovalMartixOptionViewModels> GetApprovalMartixOption(string Code)
        {
            if (Code != null && Code != "")
            {

                return OrganizationMain.GetApprovalMartixOption().Where(e => e.ApprovalMartixOptionCode == Code).Select(e => new ApprovalMartixOptionViewModels
                {
                    ApprovalMartixOptionCode = e.ApprovalMartixOptionCode,
                    ApprovalMartixOptionName = e.ApprovalMartixOptionName
                }).ToList();
            }
            else
            {
                return OrganizationMain.GetApprovalMartixOption().Select(e => new ApprovalMartixOptionViewModels
                {
                    ApprovalMartixOptionCode = e.ApprovalMartixOptionCode,
                    ApprovalMartixOptionName = e.ApprovalMartixOptionName
                }).ToList();
            }
        }
        [System.Web.Http.HttpGet]
        public List<JobLevelViewModels> GetJobLeveln(int Id = -1)
        {
            if (Id > 0)
            {

                return OrganizationMain.GetJobLeveln().Where(e => e.JobLevelId == Id).Select(e => new JobLevelViewModels
                {
                    JobLevelId = e.JobLevelId,
                    RankingSequence = e.RankingSequence,
                    Title = e.Title,
                    JobLevel = e.JobLevel1
                }).ToList();
            }
            else
            {
                return OrganizationMain.GetJobLeveln().Select(e => new JobLevelViewModels
                {
                    JobLevelId = e.JobLevelId,
                    RankingSequence = e.RankingSequence,
                    Title = e.Title,
                    JobLevel = e.JobLevel1
                }).ToList();
            }
        }
        [System.Web.Http.HttpGet]
        public List<ProcessOptionViewModels> GetProcessOption(int Id = -1)
        {
            if (Id > 0)
            {

                return OrganizationMain.GetProcessOption().Where(e => e.ProcessID == Id).Select(e => new ProcessOptionViewModels
                {
                    ProcessID = e.ProcessID,
                    ApprovalMartixOptionCode = e.ApprovalMartixOptionCode,
                    ApprovalMartixOptionName = e.ApprovalMartixOptionName
                }).ToList();
            }
            else
            {
                return OrganizationMain.GetProcessOption().Select(e => new ProcessOptionViewModels
                {
                    ProcessID = e.ProcessID,
                    ApprovalMartixOptionCode = e.ApprovalMartixOptionCode,
                    ApprovalMartixOptionName = e.ApprovalMartixOptionName
                }).ToList();
            }
        }
        [System.Web.Http.HttpGet]
        public List<ApprovalLevelViewModels> GetApprovalLevel(int Id = -1)
        {
            if (Id > 0)
            {

                return OrganizationMain.GetApprovalLevel().Where(e => e.ApprovalLevelId == Id).Select(e => new ApprovalLevelViewModels
                {
                    ApprovalLevelId = e.ApprovalLevelId,
                    Seq = e.Seq,
                    ApprovalLevelCode = e.ApprovalLevelCode,
                    ApprovalLevelName = e.ApprovalLevelName
                }).ToList();
            }
            else
            {
                return OrganizationMain.GetApprovalLevel().Select(e => new ApprovalLevelViewModels
                {
                    ApprovalLevelId = e.ApprovalLevelId,
                    Seq = e.Seq,
                    ApprovalLevelCode = e.ApprovalLevelCode,
                    ApprovalLevelName = e.ApprovalLevelName
                }).ToList();
            }
        }
        [System.Web.Http.HttpGet]
        public List<ApprovalCriteriaTypeViewModel> GetApprovalCriteriaType()
        {
            return OrganizationMain.GetApprovalCriteriaType().Select(e => new ApprovalCriteriaTypeViewModel
            {
                ApprovalCriteriaType1 = e.ApprovalCriteriaType1,
                Description = e.Description,
                CompareLogic = e.CompareLogic
            }).ToList();

        }
        [System.Web.Http.HttpGet]
        public List<CostCenterViewModels> GetCostCenter(int Id = -1)
        {
            if (Id > 0)
            {

                return OrganizationMain.GetCostCenter().Where(e => e.CostCenterId == Id).Select(e => new CostCenterViewModels
                {
                    CostCenterId = e.CostCenterId,
                    Code = e.Code,
                    Description = e.Description,
                    PIC = e.PIC,
                }).ToList();
            }
            else
            {
                return OrganizationMain.GetCostCenter().Select(e => new CostCenterViewModels
                {
                    CostCenterId = e.CostCenterId,
                    Code = e.Code,
                    Description = e.Description,
                    PIC = e.PIC,
                }).ToList();
            }
        }
        [System.Web.Http.HttpPut]
        public List<CostCenterViewModels> CreateCostCenter([FromBody]CostCenterViewModels vms)
        {
            List<CostCenterViewModels> resultuf = new List<CostCenterViewModels>();
            CostCenter CostCenter = new CostCenter();
            CostCenter.CostCenterId = vms.CostCenterId;
            CostCenter.Code = vms.Code;
            CostCenter.Description = vms.Description;
            CostCenter.PIC = vms.PIC;
            CostCenter.IsDeleted = false;
            if (CostCenter.CostCenterId == 0)
            {

                var result = OrganizationMain.CreateCostCenter(CostCenter);
                if (result.CostCenterId != 0)
                {
                    vms = OrganizationMain.GetCostCenter().Where(e => e.CostCenterId == result.CostCenterId).Select(e => new CostCenterViewModels
                    {
                        CostCenterId = e.CostCenterId,
                        Code = e.Code,
                        Description = e.Description,
                        PIC = e.PIC,
                    }).FirstOrDefault();
                    resultuf.Add(vms);
                }
            }
            return resultuf;


        }

        [System.Web.Http.HttpPut]
        public List<CostCenterViewModels> UpdateCostCenter([FromBody]CostCenterViewModels vms)
        {
            List<CostCenter> lgr = new List<CostCenter>();
            CostCenter CostCenter = new CostCenter();
            CostCenter.CostCenterId = vms.CostCenterId;
            CostCenter.Code = vms.Code;
            CostCenter.Description = vms.Description;
            CostCenter.PIC = vms.PIC;
            lgr.Add(CostCenter);
            List<CostCenter> errolui = new List<CostCenter>();
            foreach (var gr in lgr)
            {
                if (gr.CostCenterId != 0)
                {

                    var result = OrganizationMain.UpdateCostCenter(gr);
                    if (result == null)

                        errolui.Add(gr);
                }
                else
                {
                    errolui.Add(gr);
                }
            }
            if (errolui.Count() > 0)
            {
                return null;
            }
            else
            {
                List<CostCenterViewModels> ml = new List<CostCenterViewModels>();
                vms = OrganizationMain.GetCostCenter().Where(e => e.CostCenterId == vms.CostCenterId).Select(e => new CostCenterViewModels
                {
                    CostCenterId = e.CostCenterId,
                    Code = e.Code,
                    Description = e.Description,
                    PIC = e.PIC,
                }).FirstOrDefault();

                ml.Add(vms);
                return ml;
            }
        }
        [System.Web.Http.HttpDelete]
        public List<CostCenterViewModels> DeleteCostCenter([FromBody]CostCenterViewModels vms)
        {
            List<CostCenter> lgr = new List<CostCenter>();
            CostCenter CostCenter = new CostCenter();
            CostCenter.CostCenterId = vms.CostCenterId;
            CostCenter.Code = vms.Code;
            CostCenter.Description = vms.Description;
            CostCenter.PIC = vms.PIC;
            CostCenter.IsDeleted = true;
            lgr.Add(CostCenter);
            List<CostCenter> errolui = new List<CostCenter>();
            foreach (var gr in lgr)
            {
                if (gr.CostCenterId != 0)
                {

                    var result = OrganizationMain.UpdateCostCenter(gr);
                    if (result == null)
                        errolui.Add(gr);
                }
                else
                {
                    errolui.Add(gr);
                }
            }
            if (errolui.Count() > 0)
            {
                return null;
            }
            else
            {
                List<CostCenterViewModels> ml = new List<CostCenterViewModels>();
                ml.Add(vms);
                return ml;
            }
        }

        [System.Web.Http.HttpPut]
        public List<RoleViewModels> CreateRole([FromBody]RoleViewModels vms)
        {
            try
            {
                List<RoleViewModels> resultuf = new List<RoleViewModels>();
                Role Role = new Role();
                Role.RoletId = vms.RoletId;
                Role.RoleName = vms.RoleName;
                Role.Remark = vms.Remark;
                Role.CreatedBy = "sa";
                Role.CreatedDate = DateTime.Now;
                Role.IsDeleted = false;
                if (Role.RoletId == 0)
                {
                    var result = OrganizationMain.CreateRole(Role);
                    if (result.RoletId != 0)
                    {
                        vms = OrganizationMain.GetRoleList().Where(e => e.RoletId == result.RoletId).Select(e => new RoleViewModels
                        {
                            RoletId = e.RoletId,
                            RoleName = e.RoleName,
                            Remark = e.Remark
                        }).FirstOrDefault();
                        resultuf.Add(vms);
                    }
                }
                return resultuf;

            }
            catch (Exception e)
            {
                CommonMain.CreateErrorLog(e, "Organization");
            }
            return null;
        }

        [System.Web.Http.HttpPut]
        public List<RoleViewModels> UpdateRole([FromBody]RoleViewModels vms)
        {
            try
            {
                List<Role> lgr = new List<Role>();
                Role Role = new Role();
                Role.RoletId = vms.RoletId;
                Role.RoleName = vms.RoleName;
                Role.Remark = vms.Remark;
                Role.CreatedBy = "sa";
                Role.CreatedDate = DateTime.Now;
                Role.IsDeleted = false;
                lgr.Add(Role);
                List<Role> errolui = new List<Role>();
                foreach (var gr in lgr)
                {
                    if (gr.RoletId != 0)
                    {

                        var result = OrganizationMain.UpdateRole(Role);
                        if (result == null)

                            errolui.Add(Role);
                    }
                    else
                    {
                        errolui.Add(gr);
                    }
                }
                if (errolui.Count() > 0)
                {
                    return null;
                }
                else
                {
                    List<RoleViewModels> ml = new List<RoleViewModels>();
                    vms = OrganizationMain.GetRoleList().Where(e => e.RoletId == vms.RoletId).Select(e => new RoleViewModels
                    {
                        RoletId = e.RoletId,
                        RoleName = e.RoleName,
                        Remark = e.Remark
                    }).FirstOrDefault();

                    ml.Add(vms);
                    return ml;
                }
            }
            catch (Exception e)
            {
                CommonMain.CreateErrorLog(e, "Organization");
            }
            return null;
        }
        [System.Web.Http.HttpDelete]
        public List<RoleViewModels> DeleteRole([FromBody]RoleViewModels vms)
        {
            try
            {
                List<Role> lgr = new List<Role>();
                Role Role = new Role();
                Role.RoletId = vms.RoletId;
                Role.RoleName = vms.RoleName;
                Role.Remark = vms.Remark;
                Role.CreatedBy = "sa";
                Role.CreatedDate = DateTime.Now;
                Role.IsDeleted = true;
                lgr.Add(Role);
                List<Role> errolui = new List<Role>();
                foreach (var gr in lgr)
                {
                    if (gr.RoletId != 0)
                    {

                        var result = OrganizationMain.UpdateRole(gr);
                        if (result == null)
                            errolui.Add(gr);
                    }
                    else
                    {
                        errolui.Add(gr);
                    }
                }
                if (errolui.Count() > 0)
                {
                    return null;
                }
                else
                {
                    List<RoleViewModels> ml = new List<RoleViewModels>();
                    ml.Add(vms);
                    return ml;
                }
            }
            catch (Exception e)
            {
                CommonMain.CreateErrorLog(e, "Organization");
            }
            return null;
        }
        [System.Web.Http.HttpGet]
        public List<RoleMemberViewModel> GetRoleMemberList(int nid)
        {
            try
            {
                return OrganizationMain.GetRoleMemberList(nid);
            }
            catch (Exception e)
            {
                CommonMain.CreateErrorLog(e, "Organization");
            }
            return null;
        }
        [System.Web.Http.HttpGet]
        public List<RoleMemberViewModel> UpdateRoleMemberList(string models)
        {
            try
            {
                List<RoleMemberViewModel> ret = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RoleMemberViewModel>>(models);
                try
                {
                    if (ret != null)
                    {
                        ret = OrganizationMain.UpdateRoleMemberList(ret);
                    }
                }
                catch (Exception e)
                {
                }
                return ret;
            }
            catch (Exception e)
            {
                CommonMain.CreateErrorLog(e, "Organization");
            }
            return null;
        }
        [System.Web.Http.HttpGet]
        public List<SiteMenuViewModels> GetSiteMenu(int Id = -1)
        {
            try
            {
                if (Id > 0)
                {

                    return OrganizationMain.GetSiteMenu().Where(e => e.ID == Id).Select(e => new SiteMenuViewModels
                    {
                        ID = e.ID,
                        PageName = e.PageName,
                        PageDisplayName = e.PageDisplayName,
                        PageNameDisplayName_CN = e.PageNameDisplayName_CN,
                        URL = e.URL,
                        AppID = e.AppID,
                        AppName = e.AppName,
						ParentID=e.ParentID!=null?(int)e.ParentID:0,
						ParentName=e.ParentName
					}).ToList();
                }
                else
                {
                    return OrganizationMain.GetSiteMenu().Select(e => new SiteMenuViewModels
                    {
                        ID = e.ID,
                        PageName = e.PageName,
                        PageDisplayName = e.PageDisplayName,
                        PageNameDisplayName_CN = e.PageNameDisplayName_CN,
                        URL = e.URL,
                        AppID = e.AppID,
                        AppName = e.AppName,
						ParentID = e.ParentID != null ? (int)e.ParentID : 0,
						ParentName = e.ParentName
					}).ToList();
                }
            }
            catch (Exception e)
            {
                CommonMain.CreateErrorLog(e, "Organization");
            }
            return null;

        }
        [System.Web.Http.HttpPut]
        public List<SiteMenuViewModels> CreateSiteMenu([FromBody]SiteMenuViewModels vms)
        {

            List<SiteMenuViewModels> resultuf = new List<SiteMenuViewModels>();
            try
            {
                SiteMenu SiteMenu = new SiteMenu();
                SiteMenu.ID = vms.ID;
                SiteMenu.PageName = vms.PageName;
                SiteMenu.PageDisplayName = vms.PageDisplayName;
                SiteMenu.PageNameDisplayName_CN = vms.PageNameDisplayName_CN;
                SiteMenu.URL = vms.URL;
                SiteMenu.AppID = 1;
				SiteMenu.ParentID = vms.ParentID;
                SiteMenu.CreatedDate = DateTime.Now;
                SiteMenu.IsDeleted = false;
                if (SiteMenu.ID == 0)
                {
                    var result = OrganizationMain.CreateSiteMenu(SiteMenu);
                    if (result.ID != 0)
                    {
                        vms = OrganizationMain.GetSiteMenu().Where(e => e.ID == result.ID).Select(e => new SiteMenuViewModels
                        {
                            ID = e.ID,
                            PageName = e.PageName,
                            PageDisplayName = e.PageDisplayName,
                            PageNameDisplayName_CN = e.PageNameDisplayName_CN,
                            URL = e.URL,
                            AppID = e.AppID,
                            AppName = e.AppName,
							ParentID = e.ParentID != null ? (int)e.ParentID : 0,
							ParentName = e.ParentName
						}).FirstOrDefault();
                        resultuf.Add(vms);
                    }
                }
            }
            catch (Exception e)
            {
                CommonMain.CreateErrorLog(e, "Organization");
            }
            return resultuf;


        }

        [System.Web.Http.HttpPut]
        public List<SiteMenuViewModels> UpdateSiteMenu([FromBody]SiteMenuViewModels vms)
        {
            try
            {
                List<SiteMenu> lgr = new List<SiteMenu>();
                SiteMenu SiteMenu = new SiteMenu();
                SiteMenu.ID = vms.ID;
                SiteMenu.PageName = vms.PageName;
                SiteMenu.PageDisplayName = vms.PageDisplayName;
                SiteMenu.PageNameDisplayName_CN = vms.PageNameDisplayName_CN;
                SiteMenu.URL = vms.URL;
                SiteMenu.AppID = 1;
				SiteMenu.ParentID = vms.ParentID;
                SiteMenu.ModifiedDate = DateTime.Now;
                SiteMenu.IsDeleted = false;
                lgr.Add(SiteMenu);
                List<SiteMenu> errolui = new List<SiteMenu>();
                foreach (var gr in lgr)
                {
                    if (gr.ID != 0)
                    {

                        var result = OrganizationMain.UpdateSiteMenu(SiteMenu);
                        if (result == null)
                            errolui.Add(SiteMenu);
                    }
                    else
                    {
                        errolui.Add(gr);
                    }
                }
                if (errolui.Count() > 0)
                {
                    return null;
                }
                else
                {
                    List<SiteMenuViewModels> ml = new List<SiteMenuViewModels>();
                    vms = OrganizationMain.GetSiteMenu().Where(e => e.ID == vms.ID).Select(e => new SiteMenuViewModels
                    {
                        ID = e.ID,
                        PageName = e.PageName,
                        PageDisplayName = e.PageDisplayName,
                        PageNameDisplayName_CN = e.PageNameDisplayName_CN,
                        URL = e.URL,
                        AppID = e.AppID,
                        AppName = e.AppName,
						ParentID = e.ParentID != null ? (int)e.ParentID : 0,
						ParentName = e.ParentName
					}).FirstOrDefault();

                    ml.Add(vms);
                    return ml;
                }
            }
            catch (Exception e)
            {
                CommonMain.CreateErrorLog(e, "Organization");
            }
            List<SiteMenuViewModels> rush = new List<SiteMenuViewModels>();
            rush.Add(vms);
            return rush;
        }
        [System.Web.Http.HttpDelete]
        public List<SiteMenuViewModels> DeleteSiteMenu([FromBody]SiteMenuViewModels vms)
        {
            try
            {
                List<SiteMenu> lgr = new List<SiteMenu>();
                SiteMenu SiteMenu = new SiteMenu();
                SiteMenu.ID = vms.ID;
                SiteMenu.PageName = vms.PageName;
                SiteMenu.PageDisplayName = vms.PageDisplayName;
                SiteMenu.PageNameDisplayName_CN = vms.PageNameDisplayName_CN;
                SiteMenu.URL = vms.URL;
                SiteMenu.AppID = 1;
                SiteMenu.ModifiedDate = DateTime.Now;
				SiteMenu.ParentID = vms.ParentID;
                SiteMenu.IsDeleted = true;
                lgr.Add(SiteMenu);
                List<SiteMenu> errolui = new List<SiteMenu>();
                foreach (var gr in lgr)
                {
                    if (gr.ID != 0)
                    {

                        var result = OrganizationMain.UpdateSiteMenu(gr);
                        if (result == null)
                            errolui.Add(gr);
                    }
                    else
                    {
                        errolui.Add(gr);
                    }
                }
                if (errolui.Count() > 0)
                {
                    return null;
                }
                else
                {
                    List<SiteMenuViewModels> ml = new List<SiteMenuViewModels>();
                    ml.Add(vms);
                    return ml;
                }
            }
            catch (Exception e)
            {
                CommonMain.CreateErrorLog(e, "Organization");
            }
            List<SiteMenuViewModels> rush = new List<SiteMenuViewModels>();
            rush.Add(vms);
            return rush;
        }
        [System.Web.Http.HttpGet]
        public List<SiteMenuRightViewModel> GetSiteMenuRightList(int nid)
        {
            try
            {
                return OrganizationMain.GetListBpxSiteMenuRight(nid);
            }
            catch (Exception e)
            {
                CommonMain.CreateErrorLog(e, "Organization");
            }
            return null;
        }
        [System.Web.Http.HttpGet]
        public List<SiteMenuRightViewModel> UpdateSiteMenuRightList(string models)
        {
            List<SiteMenuRightViewModel> ret = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SiteMenuRightViewModel>>(models);
            try
            {
                if (ret != null)
                {
                    ret = OrganizationMain.UpdateSiteMenuRightList(ret);
                }
            }
            catch (Exception e)
            {
                CommonMain.CreateErrorLog(e, "Organization");
            }
            return ret;
        }

        [System.Web.Http.HttpGet]
        public List<SystemParameterViewModels> GetListSystemParameter(int? nid = 0)
        {
            try
            {
                return OrganizationMain.GetSystemParameterList().Select(e => new SystemParameterViewModels
                {
                    AppKey = e.AppKey,
                    Description = e.Description,
                    GroupName = e.GroupName,
                    Value = e.Value
                }).ToList();

            }
            catch (Exception e)
            {
                CommonMain.CreateErrorLog(e, "Organization");
            }
            return null;
        }

        [System.Web.Http.HttpPut]
        public List<SystemParameterViewModels> UpdateSystemParameter([FromBody]SystemParameterViewModels vms)
        {
            try
            {
                List<SystemParameter> lgr = new List<SystemParameter>();
                SystemParameter systemParameter = new SystemParameter();
                systemParameter.AppKey = vms.AppKey;
                systemParameter.Value = vms.Value;
                systemParameter.GroupName = vms.GroupName;
                systemParameter.Description = vms.Description;
                lgr.Add(systemParameter);
                List<SystemParameter> errolui = new List<SystemParameter>();
                foreach (var gr in lgr)
                {
                    if (!string.IsNullOrEmpty(gr.AppKey))
                    {

                        var result = OrganizationMain.UpdateSystemParameter(systemParameter);
                        if (result == null)
                            errolui.Add(systemParameter);
                    }
                    else
                    {
                        errolui.Add(gr);
                    }
                }
                if (errolui.Count() > 0)
                {
                    return null;
                }
                else
                {
                    List<SystemParameterViewModels> ml = new List<SystemParameterViewModels>();
                    vms = OrganizationMain.GetSystemParameterList().Where(e => e.AppKey == vms.AppKey).Select(e => new SystemParameterViewModels
                    {
                        AppKey = e.AppKey,
                        Value = e.Value,
                        Description = e.Description,
                        GroupName = e.GroupName
                    }).FirstOrDefault();

                    ml.Add(vms);
                    return ml;
                }
            }
            catch (Exception e)
            {
                CommonMain.CreateErrorLog(e, "Organization");
            }
            List<SystemParameterViewModels> rush = new List<SystemParameterViewModels>();
            rush.Add(vms);
            return rush;
        }

        [System.Web.Http.HttpGet]
        public UserInfo GetMemberById(int Id)
        {
            return GetMember(Id);
        }

        [System.Web.Http.HttpGet]
        public List<vDivisionViewModels> GetvDivisionList()
        {
            try
            {
                return OrganizationMain.GetvDivision().Select(e => new vDivisionViewModels
                {
                     UnitId = e.UnitId,
                     UnitName = e.UnitName,
                }).ToList();
            }
            catch (Exception e)
            {
                CommonMain.CreateErrorLog(e, "Organization");
            }
            return null;
        }

        [System.Web.Http.HttpGet]
        public Unit GetAnnualBudgetByID(int UnitID)
        {
            return OrganizationMain.GetDivisionUnit(UnitID);
        }
    }
}