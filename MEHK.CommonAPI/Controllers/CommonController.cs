﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.DirectoryServices;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using MEHK.DAL.DTO;
using MEHK.DAL.Enum;
using MEHK.DAL.ViewModel;
using System.Data.SqlClient;
using MEHK.DAL.Model;

namespace MEHK.CommonAPI.Controllers
{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'CommonController'
    public class CommonController : BaseApiController
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'CommonController'
    {

        private string TempFolder
        {
            get
            {
                return CommonMain.GetSystemParameter(AppKey.UploadTempFolder.ToString());
            }
        }


        //[HttpGet]
        //public HttpResponseMessage DownloadFile(Guid guid, string process)
        //{
        //    HttpResponseMessage result = new HttpResponseMessage();
        //    var file = CommonMain.DownloadFile(guid, process);
        //    if (file != null)
        //    {
        //        result = new HttpResponseMessage(HttpStatusCode.OK);
        //        return Request.CreateResponse(HttpStatusCode.OK, "Success");
        //    }
        //    else
        //    {
        //        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Unable to get the attachment");
        //    }

        //    return result;
        //}

#pragma warning disable CS1587 // XML comment is not placed on a valid language element
        /// <summary>
        /// Delete file
        /// </summary>
        /// <param name="guid">File Guid</param>
        /// <param name="process">Workflow Code</param>
        /// <returns></returns>
        //public HttpResponseMessage DeleteFile(Guid guid, string process)
        //{
        //    HttpResponseMessage result = new HttpResponseMessage();
        //    var actionResult = CommonMain.DeleteFile(guid, process);
        //    if (actionResult.IsSuccess)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.OK, actionResult.Message);
        //    }
        //    else
        //    {
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError, actionResult.Message);
        //    }

        //    return result;
        //}


#pragma warning disable CS1587 // XML comment is not placed on a valid language element
        /// <summary>
        /// Get File List By Process Code and Form ID
        /// </summary>
        /// <param name="processCode">Process Code</param>
        /// <param name="formID">Form ID</param>
        /// <returns></returns>
        //[HttpGet]
        //public List<FileList> FileLists(string processCode, string formID)
        //{
        //    return CommonMain.GetFileList(processCode, formID);
        //}


#pragma warning disable CS1572 // XML comment has a param tag for 'process', but there is no parameter by that name
        /// <summary>
        /// Get Attachment Type List
        /// </summary>
        /// <param name="process">Workflow Code</param>
        /// <returns></returns>
        //[HttpGet]
        //public List<CommonSettingsDTO> FileTypes(string process)
        //{
        //    return new List<CommonSettingsDTO>();
        //}


        [HttpGet]
#pragma warning restore CS1572 // XML comment has a param tag for 'process', but there is no parameter by that name
#pragma warning restore CS1587 // XML comment is not placed on a valid language element
#pragma warning restore CS1587 // XML comment is not placed on a valid language element
#pragma warning disable CS1573 // Parameter 'processCode' has no matching param tag in the XML comment for 'CommonController.InitFiles(int, Guid, string)' (but other parameters do)
#pragma warning disable CS1573 // Parameter 'gID' has no matching param tag in the XML comment for 'CommonController.InitFiles(int, Guid, string)' (but other parameters do)
#pragma warning disable CS1573 // Parameter 'id' has no matching param tag in the XML comment for 'CommonController.InitFiles(int, Guid, string)' (but other parameters do)
        public HttpResponseMessage InitFiles(int id, Guid gID, string processCode)
#pragma warning restore CS1573 // Parameter 'id' has no matching param tag in the XML comment for 'CommonController.InitFiles(int, Guid, string)' (but other parameters do)
#pragma warning restore CS1573 // Parameter 'gID' has no matching param tag in the XML comment for 'CommonController.InitFiles(int, Guid, string)' (but other parameters do)
#pragma warning restore CS1573 // Parameter 'processCode' has no matching param tag in the XML comment for 'CommonController.InitFiles(int, Guid, string)' (but other parameters do)
        {

            var fileList = CommonMain.GetUploadedFiles(id, processCode).Where(n => n.UploadAfterApproved == false);

            CommonMain.DelTempFiles(gID);

            foreach (var item in fileList)
            {
                var json = JsonConvert.SerializeObject(item);
                CommonMain.AddTempFileInfo(gID, item.FileID, json);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'CommonController.SystemParameter(string)'
        public string SystemParameter(string Key)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'CommonController.SystemParameter(string)'
        {



            return CommonMain.GetSystemParameter(Key);


        }

        //[HttpGet]
        //public HttpResponseMessage InitUploadAfterApprovedFiles(int id, Guid gID, string processCode)
        //{

        //    var fileList = CommonMain.GetUploadedFiles(id, processCode).Where(n => n.UploadAfterApproved == true);

        //    CommonMain.DelTempFiles(gID);

        //    foreach (var item in fileList)
        //    {
        //        var json = JsonConvert.SerializeObject(item);
        //        CommonMain.AddTempFileInfo(gID, item.FileID, json);
        //    }

        //    return Request.CreateResponse(HttpStatusCode.OK);
        //}
        [HttpPost]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'CommonController.Files(Guid)'
        public HttpResponseMessage Files(Guid GID)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'CommonController.Files(Guid)'
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }
            HttpRequest httpRequest = HttpContext.Current.Request;

            //Guid GID = new Guid(httpRequest.Form["GID"]);
            string title = httpRequest.Form["title"];
            string refid = httpRequest.Form["refid"];
            string process = httpRequest.Form["process"];
            string remark = httpRequest.Form["remark"];
            int userID = Convert.ToInt32(httpRequest.Form["userID"]);
            string attachmentType = httpRequest.Form["attachmentType"];
            string section = httpRequest.Form["section"];
            string catName = httpRequest.Form["catName"];

            CommonMain.CreateDebugLog("title:" + title + "," +
                "refid:" + refid + "," +
                "process:" + process + "," +
                "remark:" + remark + "," +
                "userID:" + userID + "," +
                "attachmentType:" + attachmentType + "," +
                 "section:" + section + "," +
                  "catName:" + catName);

            //List<FileInfoViewModel> listFileViewModel = null;
            //try
            //{
            //	listFileViewModel = HttpContext.Current.Session["UploadedFiles" + GID.ToString()] as List<FileInfoViewModel>;
            //}
            //catch
            //{
            //	listFileViewModel = new List<FileInfoViewModel>();
            //}

            var files = httpRequest.Files;
            if (files != null)
            {
                for (int i = 0; i < files.Count; i++)
                {
                    FileInfoViewModel FileViewModel = new FileInfoViewModel();

                    var fileName = Path.GetFileName(files[i].FileName);

                    FileViewModel.DocumentName = fileName;
                    FileViewModel.Title = title;
                    FileViewModel.Remark = remark;
                    FileViewModel.FileID = Guid.NewGuid();
                    FileViewModel.FileSize = files[i].ContentLength;
                    FileViewModel.HasUpload = false;
                    FileViewModel.UploadedByUserID = userID;
                    FileViewModel.UploadedBy = OrganizationMain.GetMember(userID).MemberName;
                    //FileViewModel.UploadedDate = DateTime.Now.ToString("dd-MM-yyyy");
                    //FileViewModel.UploadedDate = DateTime.Now.ToString("dd-MM-yyyy HH:mm");
                    FileViewModel.UploadedDate = DateTime.Now.ToString("yyyy/MM/dd HH:mm");
                    FileViewModel.FileType = attachmentType;
                    FileViewModel.Section = section;
                    FileViewModel.CatName = catName;

                    var TempPathFolder = TempFolder + FileViewModel.FileID;

                    if (!Directory.Exists(TempPathFolder))
                    {
                        Directory.CreateDirectory(TempPathFolder);
                    }

                    var TempFilePath = TempPathFolder + @"\" + fileName;

                    files[i].SaveAs(TempFilePath);

                    var json = JsonConvert.SerializeObject(FileViewModel);
                    CommonMain.AddTempFileInfo(GID, FileViewModel.FileID, json);

                    //listFileViewModel.Add(FileViewModel);

                }
            }
            return Request.CreateResponse(HttpStatusCode.OK);
            //string root = HttpContext.Current.Server.MapPath("~/App_Data");
            //var provider = new MultipartFormDataStreamProvider(root);

            //try
            //{
            //	StringBuilder sb = new StringBuilder(); // Holds the response body

            //	// Read the form data and return an async task.
            //	await Request.Content.ReadAsMultipartAsync(provider);

            //	// This illustrates how to get the form data.
            //	foreach (var key in provider.FormData.AllKeys)
            //	{
            //		foreach (var val in provider.FormData.GetValues(key))
            //		{
            //			sb.Append(string.Format("{0}: {1}\n", key, val));
            //		}
            //	}

            //	// This illustrates how to get the file names for uploaded files.
            //	foreach (var file in provider.FileData)
            //	{
            //		FileInfo fileInfo = new FileInfo(file.LocalFileName);
            //		sb.Append(string.Format("Uploaded file: {0} ({1} bytes)\n", fileInfo.Name, fileInfo.Length));
            //	}
            //	return new HttpResponseMessage()
            //	{
            //		Content = new StringContent("")
            //	};
            //}
            //catch (System.Exception e)
            //{
            //	return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            //}



        }

        //[HttpGet]
        //public HttpResponseMessage Updatefiles(int Formid, Guid gID, int userID, string processCode)
        //{

        //    List<FileInfoViewModel> FileViewModel = new List<FileInfoViewModel>();

        //    var getData = CommonMain.GetTempFiles(gID);
        //    foreach (var item in getData)
        //    {
        //        FileInfoViewModel temp = JsonConvert.DeserializeObject<FileInfoViewModel>(item.Detail);
        //        FileViewModel.Add(temp);
        //    }

        //    CommonMain.UpdateFile(FileViewModel, processCode, Formid, userID, false);
        //    return Request.CreateResponse(HttpStatusCode.OK);
        //}
        [HttpPut]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'CommonController.Files(int, Guid, int, string)'
        public HttpResponseMessage Files(int Formid, Guid gID, int userID, string processCode)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'CommonController.Files(int, Guid, int, string)'
        {

            List<FileInfoViewModel> FileViewModel = new List<FileInfoViewModel>();

            var getData = CommonMain.GetTempFiles(gID);
            foreach (var item in getData)
            {
                FileInfoViewModel temp = JsonConvert.DeserializeObject<FileInfoViewModel>(item.Detail);
                FileViewModel.Add(temp);
            }

            CommonMain.UpdateFile(FileViewModel, processCode, Formid, userID, false);
            return Request.CreateResponse(HttpStatusCode.OK);
        }


        [HttpDelete]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'CommonController.Files(Guid, Guid)'
        public HttpResponseMessage Files(Guid FileID, Guid GID)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'CommonController.Files(Guid, Guid)'
        {
            CommonMain.DelTempFile(GID, FileID);

            return Request.CreateResponse(HttpStatusCode.OK);
        }
        [HttpGet]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'CommonController.Download(Guid, bool)'
        public HttpResponseMessage Download(Guid FileID, bool HasUpload)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'CommonController.Download(Guid, bool)'
        {
            HttpResponseMessage result = new HttpResponseMessage();
            string SavePath = "";
            byte[] file = null;
            var fileName = "";

            if (HasUpload)
            {
                file = CommonMain.DownloadFile(FileID, ref fileName);
            }
            else
            {
                SavePath = TempFolder;

                string FolderPath = SavePath + FileID.ToString();

                string[] filePaths = Directory.GetFiles(FolderPath);

                if (filePaths.Length > 0)
                {
                    fileName = Path.GetFileName(filePaths[0]);


                    file = File.ReadAllBytes(filePaths[0]);
                }
            }

            if (file != null)
            {
                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(file);
                result.Content.Headers.ContentDisposition =
                        new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                        {
                            FileName = fileName
                        };
                return result;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Unable to get the attachment");
            }

            //return result;
        }
        [HttpGet]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'CommonController.Files(Guid, bool, string, int)'
        public List<FileInfoViewModel> Files(Guid GID, bool isInite = false, string processCode = "", int formid = -1)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'CommonController.Files(Guid, bool, string, int)'
        {
            if (isInite)
            {
                var fileList = CommonMain.GetUploadedFiles(formid, processCode).Where(n => n.UploadAfterApproved == false);

                CommonMain.DelTempFiles(GID);

                foreach (var item in fileList)
                {
                    var json = JsonConvert.SerializeObject(item);
                    CommonMain.AddTempFileInfo(GID, item.FileID, json);
                }
                return new List<FileInfoViewModel>();

            }
            else
            {
                List<FileInfoViewModel> FileViewModel = new List<FileInfoViewModel>();

                var getData = CommonMain.GetTempFiles(GID);
                foreach (var item in getData)
                {
                    FileInfoViewModel temp = JsonConvert.DeserializeObject<FileInfoViewModel>(item.Detail);
                    FileViewModel.Add(temp);
                }

                return FileViewModel;
            }
        }

  
        //[HttpPost]
        //public bool Authenticate(dynamic data)
        //{
        //    bool authentic = false;
        //    try
        //    {
        //        string userName = "";
        //        int UserID = data["UserID"] != null ? int.Parse(data["UserID"].Value) : 0;
        //        var userinfo = OrganizationMain.GetUser(UserID);
        //        if (userinfo != null)
        //        {
        //            userName = userinfo.SAMAccountName;
        //        }
        //        string domain = "";
        //        MEHK.BLL.CommonBLL WFCommonBLL = new MEHK.BLL.CommonBLL();
        //        domain = WFCommonBLL.GetSystemParameter(AppKey.K2DomainName.ToString());
        //        string labpath = WFCommonBLL.GetSystemParameter("WindowLABPath");
        //        DirectoryEntry entry = new DirectoryEntry(labpath, userName, data["password"].Value);
        //        object nativeObject = entry.NativeObject;
        //        authentic = true;
        //    }
        //    catch (DirectoryServicesCOMException) { }
        //    return authentic;
        //}
        [HttpGet]
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member 'CommonController.Menus(int, int)'
        public PageMenu Menus(int userID, int appID)
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member 'CommonController.Menus(int, int)'
        {

            return CommonMain.GetMenus(userID, appID);
        }

      
        public List<RightViewModel> GetUserRight(string SAMACCOUNT, int AppID)
        {

            return CommonMain.GetUserRight(SAMACCOUNT, AppID);


        }
    }



}
