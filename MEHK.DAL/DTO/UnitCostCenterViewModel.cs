﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.DTO
{
	public class UnitCostCenterViewModel
	{
		public int UnitId { get; set; }
		public string UnitName { get; set; }
		public string bExist { get; set; }
		public int CostCenterId { get; set; }
	}
}
