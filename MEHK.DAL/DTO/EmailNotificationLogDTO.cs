﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.DTO
{
	public class EmailNotificationLogDTO
	{

		public EmailNotificationLogDTO()
		{
			LoggedOn = DateTime.Now;
		}

		public int EmailNotificationTasksID { get; set; }
		public Nullable<int> EmailNotificationProfileID { get; set; }
		public int EmailTemplateID { get; set; }
		public string Recipient { get; set; }
		public System.DateTime LoggedOn { get; private set; }
		public string ErrorMessage { get; set; }
	}
}
