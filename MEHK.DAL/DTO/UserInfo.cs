﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEHK.DAL.Model;

namespace MEHK.DAL.DTO
{
	public class UserInfo
	{
		public UserInfo()
		{

		}
		public UserInfo(vMember model,bool DisplayDepartmentAndDivision=false)
		{
            MemberId = model.MemberId;
            MemberName = model.MemberName;
            LoginAccount = model.LoginAccount;
            StaffId = model.StaffId;
            SAMAccount = model.SAMAccount;
            SAPAccount = model.SAPAccount;
            Post = model.Post;
            Phone = model.Phone;
            EmailAddress = model.EmailAddress;
            JobLevelId = model.JobLevelId;
            JobLevel = model.JobLevel==null?0:Convert.ToInt32(model.JobLevel);
            JobRank = model.JobRank;
            Remark = model.Remark;
            IsActive = model.IsActive;
            CreatedBy = model.CreatedBy;
            Created = model.Created;
            LastModifiedBy = model.LastModifiedBy;
            LastModifiedDate = model.LastModifiedDate;
            UnitId = model.UnitId.HasValue ? model.UnitId.Value:-1;
            _DisplayDepartmentDivision = DisplayDepartmentAndDivision;
            using (var db = new Entities())
            {
                _AllUnitTypes = db.UnitTypes.ToList();
            }
        }
        private bool _DisplayDepartmentDivision { get; set; }
        public int MemberId { get; set; }
        public string MemberName { get; set; }
        public string LoginAccount { get; set; }
        public string StaffId { get; set; }
        public string SAMAccount { get; set; }
        public string SAPAccount { get; set; }
        public string Post { get; set; }
        public string Phone { get; set; }
        public string EmailAddress { get; set; }
        public int JobLevelId { get; set; }
        public int JobLevel { get; set; }
        public string JobRank { get; set; }
        public string Remark { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> Created { get; set; }
        public string LastModifiedBy { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public int UnitId { get; set; }
        public List<Role> Roles { get; set; }
        public Unit Unit { get; set; }
        public List<Unit> AllUnits { get; set; }
        public List<Unit> BelowUnits { get; set; }
        private List<UnitType> _AllUnitTypes { get; set; }
        public string DisplaynameAndStaffNum
		{
			get
			{
				return $"{MemberName} ({StaffId})";
			}
		}
        public string Department
        {
            get
            {
                return _DisplayDepartmentDivision? GetUnitNameByCode(Enum.UnitCode.Department.ToString()):string.Empty;
            }
        }
        public string Division
        {
            get
            {
                return _DisplayDepartmentDivision ? GetUnitNameByCode(Enum.UnitCode.Division.ToString()) : string.Empty;
            }
        }
        public string GetUnitNameByCode(string Code)
        {
            using (var db = new Entities())
            {
                var Type = _AllUnitTypes.Where(x => x.Code == Code).FirstOrDefault();
                if (Type == null)
                    return string.Empty;
                if (Unit==null)
                    return string.Empty;
                if ( Unit.UnitTypeId == Type.UnitTypeId)
                    return Unit.UnitName;
                Unit _CurrentUnit = Unit;
                while (_CurrentUnit !=null && _CurrentUnit.ParentUnitId.HasValue)
                {
                    int _CurrentUnitId = _CurrentUnit.ParentUnitId.Value;
                    _CurrentUnit = GetUnit(_CurrentUnitId);
                    if (_CurrentUnit!= null && _CurrentUnit.UnitTypeId == Type.UnitTypeId)
                        return _CurrentUnit.UnitName;
                }
                return string.Empty;
            }

        }
        public Unit GetUnit(int UnitId)
        {
            using (var db = new Entities())
            {

                return db.Units.Where(x => x.UnitId == UnitId).FirstOrDefault();


            }

        }
        public string CreatedString
        {
            get
            {
                return Created.HasValue?Created.Value.ToString("dd/MM/yyyy hh:mmm:ss"):"";
            }
        }

    }
	
}
