﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.DTO
{
	public class ControlDTO
	{
		public int ID { get; set; }
		public string Name { get; set; }

		public string GroupKey { get; set; }
	}


	public class ApproverInfo : ControlDTO
	{
		public string JobClass { get; set; }
		public string Title { get; set; }
		public int Level { get; set; }
	}
}
