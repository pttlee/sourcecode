﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEHK.DAL.Model;

namespace MEHK.DAL.DTO
{
	public class EmailTaskInfo
	{
		public int ActionOwnerUserID { get; set; }
		public int ActivityGroupID { get; set; }
	//	public List<EmailNotificationTask> TaskList { get; set; }

    }


	public enum GroupType
	{
		Summary,
		Individual,

	}

	public class EmailTableInfo
	{
		public int formID { get; set; }
		public string Code { get; set; }

	}
}
