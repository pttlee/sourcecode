﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEHK.DAL.Model;

namespace MEHK.DAL.DTO
{
	public class OrganizationInfo
	{
		public OrganizationInfo()
		{
		}

		public OrganizationInfo(Organization model)
		{
			ID = model.ID;
			Code = model.Code;
			Description = model.Description;


			TypeID = model.TypeID;
			AdditionalValue01 = model.AdditionalValue01;
			AdditionalValue02 = model.AdditionalValue02;
			AdditionalValue03 = model.AdditionalValue03;
			AdditionalValue04 = model.AdditionalValue04;
			AdditionalValue05 = model.AdditionalValue05;
			AdditionalValue06 = model.AdditionalValue06;
			AdditionalValue07 = model.AdditionalValue07;
			AdditionalValue08 = model.AdditionalValue08;
			AdditionalValue09 = model.AdditionalValue09;
			AdditionalValue10 = model.AdditionalValue10;

			ParentID = model.ParentID;
		}

		public int ID { get; set; }
		public string Code { get; set; }
		public string Description { get; set; }

		public int TypeID { get; set; }
		public string AdditionalValue01 { get; set; }
		public string AdditionalValue02 { get; set; }
		public string AdditionalValue03 { get; set; }
		public string AdditionalValue04 { get; set; }
		public string AdditionalValue05 { get; set; }
		public string AdditionalValue06 { get; set; }
		public string AdditionalValue07 { get; set; }
		public string AdditionalValue08 { get; set; }
		public string AdditionalValue09 { get; set; }
		public string AdditionalValue10 { get; set; }

		public Nullable<int> ParentID { get; set; }

	}


	public class OrganizationTypeInfo
	{
		public OrganizationTypeInfo()
		{
		}

		public OrganizationTypeInfo(OrganizationType model)
		{
			ID = model.ID;
			Code = model.Code;
			Description = model.Description;

			AdditionalAttribute01 = model.AdditionalAttribute01;
			AdditionalAttribute02 = model.AdditionalAttribute02;
			AdditionalAttribute03 = model.AdditionalAttribute03;
			AdditionalAttribute04 = model.AdditionalAttribute04;
			AdditionalAttribute05 = model.AdditionalAttribute05;
			AdditionalAttribute06 = model.AdditionalAttribute06;
			AdditionalAttribute07 = model.AdditionalAttribute07;
			AdditionalAttribute08 = model.AdditionalAttribute08;
			AdditionalAttribute09 = model.AdditionalAttribute09;
			AdditionalAttribute10 = model.AdditionalAttribute10;

		}

		public int ID { get; set; }
		public string Code { get; set; }
		public string Description { get; set; }

		public string AdditionalAttribute01 { get; set; }
		public string AdditionalAttribute02 { get; set; }
		public string AdditionalAttribute03 { get; set; }
		public string AdditionalAttribute04 { get; set; }
		public string AdditionalAttribute05 { get; set; }
		public string AdditionalAttribute06 { get; set; }
		public string AdditionalAttribute07 { get; set; }
		public string AdditionalAttribute08 { get; set; }
		public string AdditionalAttribute09 { get; set; }
		public string AdditionalAttribute10 { get; set; }
	}
}
