﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.DTO
{
	public class RoleMemberViewModel
	{
		public int MemberId { get; set; }
		public string MemberName { get; set; }
		public string bExist { get; set; }
		public int RoleId { get; set; }
	}
}
