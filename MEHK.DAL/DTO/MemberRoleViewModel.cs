﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.DTO
{
	public class MemberRoleViewModel
	{
		public int RoletId { get; set; }
		public string RoleName { get; set; }
		public string bExist { get; set; }
		public int MemberId { get; set; }
	}
}
