﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.DTO
{
	public class RightGroupViewModel
	{
		public int ID { get; set; }
		public string GroupName { get; set; }
		public string bExist { get; set; }
		public int RightID { get; set; }
	}
}
