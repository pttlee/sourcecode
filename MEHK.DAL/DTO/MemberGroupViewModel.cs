﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.DTO
{
	public class MemberGroupViewModel
	{
		public int ID { get; set; }
		public string Description { get; set; }
		public string bExist { get; set; }
		public int MemberId { get; set; }
	}
}
