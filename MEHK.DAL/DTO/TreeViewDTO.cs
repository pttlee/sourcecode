﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEHK.DAL.Model;
namespace MEHK.DAL.DTO
{
	public class TreeViewDto
	{
        public int id { get; set; }
        public string Name { get; set; }
        //public bool hasChildren { get; set; }
        public bool hasChildren { get {
                using (var GetDB = new Entities())
                {
                    return GetDB.Units.Where(x => !x.IsDeleted && (x.ParentUnitId.HasValue && x.ParentUnitId == id)).Count() > 0;
                } 

            } }
        public string spriteCssClass
		{
			get
			{
				return "folder";
			}
		}
	}
}
