﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.DTO
{
	public class SiteMenuRightViewModel
	{
		public int ID { get; set; }
		public string RightName { get; set; }
		public string bExist { get; set; }
		public int SiteMenuID { get; set; }
	}
}
