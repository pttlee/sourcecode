﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.DTO
{
	public class ProcessMenu
	{
		public int ProcessID { get; set; }
		public string ProcessName { get; set; }
		public string ProcessDisplayName { get; set; }
		public string IsDefault { get; set; }
		public string SPANav { get; set; }
	}
}
