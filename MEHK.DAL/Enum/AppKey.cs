﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.Enum
{
	public enum AppKey
	{
		UploadTempFolder,
        UploadTempFolderESign,
        LinkDBRemark,
        K2DomainName,
        k2Password,
        K2Port,
        K2Server,
        k2UserID,
        k2WindowsDomain,
        loginDomain,


        MobileAllowanceProcessName,
        DutyMileageLocalTravelProcessName,
		LocalTravelMileageMobileAdjustment,
		PettyCashProcessName,
        PettyCashSurpriseCashCountFormProcessName,
        OverseasTravellingProcessName,
        OverseasTravellingExpenseClaimProcessName,
        SummaryReportProcessName,
        StaffWelfareProcessName,

        NonBudgetCAPEXProcessName,
        ESignProcessName,

        SRSystemAdmin,


        CapexReportTemplate,
        //StaffWelfareProcessName,
        StaffWelfareJobClass,
    }
}
