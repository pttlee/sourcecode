﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.Enum
{
 
    public enum UnitCode
    {
        Company,
        Division,
        Department,
        DepartmentOrDivision,
        Section,
        Others,
    }
}
