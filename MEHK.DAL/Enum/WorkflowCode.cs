﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.Enum
{
	public enum WorkflowCode
	{

		//MobileAllowance,
		MA,
		CAF,
		DMLT,
		SR,
		PC,
        NonBudgetCAPEX,
        NonBudgetCAPEXBatch,
        DAF,
        STW
    }
}
