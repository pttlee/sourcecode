﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.Enum
{
	public enum RuleCode
	{
		Requester,
		CostCenter,

		CAF_2,
		CAF_3,
		CAF_4,
		CAF_5,
		CAF_6a,
		CAF_6b,
		CAF_7a,
		CAF_7a_AEI,
		CAF_7b,
		CAF_7b_AEI,
		CAF_8a,
		CAF_8b,
		CAF_9a,
		CAF_9b,
		CAF_10a,
		CAF_10b,
		CAF_11,
		CAF_12a,
		CAF_12b,
		CAF_13a,
		CAF_13b,
		CAF_13c,
		CAF_13d,

	}
}
