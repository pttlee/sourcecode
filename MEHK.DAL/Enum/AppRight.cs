﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.Enum
{
    public enum AppRight
    {
        SearchAllCapexProfile,

        SearchAllNonBudgetCapex_Project,
        SearchMyDeptNonBudgetCapex_Project,

        SearchAllNonBudgetCapex_NonProject,
        SearchMyDeptNonBudgetCapex_NonProject,
    }
}
