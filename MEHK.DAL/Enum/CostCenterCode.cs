﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.Enum
{
	public enum CostCenterCode
	{	
		CostCenter01,
		CostCenter02,
		CostCenter03,
		CostCenter04,
		CostCenter05,
		CostCenter06,
		CostCenter07,
		CostCenter08,
		CostCenter09,
		CostCenter10,
	}
}
