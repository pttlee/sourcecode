﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.Enum
{
    public enum UserAdditional
    {
        //MobileAllowanceNumber,

        PrivatePhoneNo,
        VehicleRegNo,
        VehicleType,
        ResidentialDistrict,
		//5 license plate numbers and types---Ren
		VehicleRegNo1,
		VehicleType1,
		VehicleRegNo2,
		VehicleType2,
		VehicleRegNo3,
		VehicleType3,
		VehicleRegNo4,
		VehicleType4,
		VehicleRegNo5,
		VehicleType5,
	}
}
