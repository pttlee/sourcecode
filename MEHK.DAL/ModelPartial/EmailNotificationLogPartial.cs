﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEHK.DAL.DTO;

namespace MEHK.DAL.Model
{
	public partial class EmailNotificationLog
	{

		public EmailNotificationLog()
		{

		}


		public EmailNotificationLog(EmailNotificationLogDTO dto)
		{
			this.EmailNotificationProfileID = dto.EmailNotificationProfileID;
			this.EmailNotificationTasksID = dto.EmailNotificationTasksID;
			this.EmailTemplateID = dto.EmailTemplateID;
			this.ErrorMessage = dto.ErrorMessage;
			this.LoggedOn = dto.LoggedOn;
			this.Recipient = dto.Recipient;
		}
	}
}
