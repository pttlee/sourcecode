//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MEHK.DAL.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class ApprovalMartix
    {
        public int ApprovalMartixId { get; set; }
        public string ApprovalMartixName { get; set; }
        public int ProcessId { get; set; }
        public Nullable<int> UnitId { get; set; }
        public Nullable<int> RoleId { get; set; }
        public decimal Priority { get; set; }
        public Nullable<int> CopyDetailFrom { get; set; }
        public string ApprovalMartixOptionCode { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
    }
}
