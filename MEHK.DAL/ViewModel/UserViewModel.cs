﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEHK.DAL.Model;

namespace MEHK.DAL.ViewModel
{
	public class UserViewModel
	{

		public UserViewModel()
		{

		}

		public UserViewModel(User model)
		{
			UserID = model.UserID;
			StaffID = model.StaffID;
			DisplayName = model.DisplayName;
			SAMAccount = model.SAMAccount;
			JobTitle = model.JobTitle;
			//ReportToUserID = model.ReportToUserID;
			JobClass = model.JobClass;
			IsDeleted = model.IsDeleted;

			CreatedDate = model.CreatedDate;
			CreatedBy = model.CreatedBy;
			ModifiedDate = model.ModifiedDate;
			ModifiedBy = model.ModifiedBy;

			Email = model.Email;
			ContactNumber = model.ContactNumber;
			Department = model.Department;

		}

		public int UserID { get; set; }
		[Required(ErrorMessage = "*")]
		public string StaffID { get; set; }
		[Required(ErrorMessage = "*")]
		public string DisplayName { get; set; }
		[Required(ErrorMessage = "*")]
		public string SAMAccount { get; set; }
		public string JobTitle { get; set; }
		public Nullable<int> ReportToUserID { get; set; }
		public string JobClass { get; set; }
		public bool IsDeleted { get; set; }
		public Nullable<System.DateTime> CreatedDate { get; set; }
		public string CreatedBy { get; set; }
		public Nullable<System.DateTime> ModifiedDate { get; set; }
		public string ModifiedBy { get; set; }

		public bool InGroup { get; set; }


		public string Email { set; get; }
		public string ContactNumber { set; get; }
		public string Department { set; get; }
	}
}
