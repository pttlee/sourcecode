﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEHK.DAL.Model;

namespace MEHK.DAL.ViewModel
{
	public class OrganizationTypeViewModel
	{
		public OrganizationTypeViewModel()
		{

		}

		public OrganizationTypeViewModel(OrganizationType model)
		{
			ID = model.ID;
			Code = model.Code;
			Description = model.Description;

			AdditionalAttribute01 = model.AdditionalAttribute01;
			AdditionalAttribute02 = model.AdditionalAttribute02;
			AdditionalAttribute03 = model.AdditionalAttribute03;
			AdditionalAttribute04 = model.AdditionalAttribute04;
			AdditionalAttribute05 = model.AdditionalAttribute05;
			AdditionalAttribute06 = model.AdditionalAttribute06;
			AdditionalAttribute07 = model.AdditionalAttribute07;
			AdditionalAttribute08 = model.AdditionalAttribute08;
			AdditionalAttribute09 = model.AdditionalAttribute09;
			AdditionalAttribute10 = model.AdditionalAttribute10;

			IsDeleted = model.IsDeleted;
		}

		public int ID { get; set; }
		public string Code { get; set; }
		public string Description { get; set; }
		public string AdditionalAttribute01 { get; set; }
		public string AdditionalAttribute02 { get; set; }
		public string AdditionalAttribute03 { get; set; }
		public string AdditionalAttribute04 { get; set; }
		public string AdditionalAttribute05 { get; set; }
		public string AdditionalAttribute06 { get; set; }
		public string AdditionalAttribute07 { get; set; }
		public string AdditionalAttribute08 { get; set; }
		public string AdditionalAttribute09 { get; set; }
		public string AdditionalAttribute10 { get; set; }
		[ScaffoldColumn(true)]
		public bool IsDeleted { get; set; }
		public Nullable<System.DateTime> CreatedDate { get; set; }
		public string CreatedBy { get; set; }
		public Nullable<System.DateTime> ModifiedDate { get; set; }
		public string ModifiedBy { get; set; }

	}
}

