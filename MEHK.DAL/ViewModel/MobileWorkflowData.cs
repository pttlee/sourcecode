﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.ViewModel
{
   public class WorkflowData
    {
		public int ProcInstID { get; set; }


		public string Action { get; set; }
        public string ActivityName { get; set; }
        public decimal Amount { get; set; }
        public string AppHisID { get; set; }
     
        public string ApproveURL { get; set; }
        public string CentralRecordNum { get; set; }
        public string Comment { get; set; }
        public string ConditionalURL { get; set; }
        public string EventName { get; set; }
        public string ID { get; set; }
     
        public bool IsConditionalApprove { get; set; }
        public bool IsExistConditional { get; set; }
        public string K2DomainName { get; set; }
        public string K2Server { get; set; }
        public string K2WorkflowInstance { get; set; }
        public string K2WorkflowInstanceUser { get; set; }
        public int Levl { get; set; }
        public string LoginName { get; set; }
        public string LoginNameID { get; set; }
        public string ProcessName { get; set; }
        public string QueryURL { get; set; }
        public string QueryUser { get; set; }
        public string Requester { get; set; }
        public string ReSubmitURL { get; set; }
        public string SerialNumber { get; set; }
        public string SN { get; set; }
        public DateTime StartDate { get; set; }
        public string StatusCode { get; set; }
        public string URL { get; set; }
    }
}
