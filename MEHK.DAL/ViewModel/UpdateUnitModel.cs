﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEHK.DAL.Model;

namespace MEHK.DAL.ViewModel
{
	public class UpdateUnitModel
	{
		public UpdateUnitModel()
		{
		}

		public UpdateUnitModel(Unit model)
		{
            UnitId = model.UnitId;
            UnitName = model.UnitName;
			Description = model.Description;


            UnitTypeId = model.UnitTypeId;


            ParentUnitId = model.ParentUnitId;
			IsDeleted = model.IsDeleted;
		}


		public int UnitId { get; set; }
		[Required(ErrorMessage = "*")]
		public string UnitName { get; set; }
		[Required(ErrorMessage = "*")]
		public string Description { get; set; }

		public int UnitTypeId { get; set; }
		
		public Nullable<int> ParentUnitId { get; set; }

		public bool IsDeleted { get; set; }
        public List<int> CostCenters { get; set; }
        public string ModifiedBy { get; set; }
    }
}
