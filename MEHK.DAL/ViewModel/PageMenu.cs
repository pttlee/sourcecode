﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.ViewModel
{
	public class PageMenu
	{
		public string MenuName { get; set; }

		public string MenuDisplayName { get; set; }
		public string PageNameDisplayName_CN { get; set; }

		public int nID { get; set; }
		public string URL { get; set; }

		public List<PageMenu> Pages { get; set; }


	}

	//public class PageMenu
	//{
	//	public string MenuName { get; set; }

	//	public int nID { get; set; }
	//	public string URL { get; set; }

	//	public List<SubPage> Pages { get; set; }


	//}
	public class SubPage
	{
		public string PageName { get; set; }
		public int ParentID { get; set; }
		public string URL { get; set; }

		public List<SubPage> Pages {get;set;}
	}
}
