﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.ViewModel
{
	public class FileInfoViewModel
	{
		//public int ID { get; set; }
		public Guid FileID { get; set; }

		public double? FileSize { get; set; }

		//public int AppID { get; set; 

		public bool HasUpload { get; set; }

		public string Title { get; set; }

		public string DocumentName { get; set; }

		public string Remark { get; set; }

		public bool EnableDelete { get; set; }

		public int UploadedByUserID { get; set; }

		public string UploadedBy { get; set; }

		public string UploadedDate { get; set; }

        public bool UploadAfterApproved { get; set; }

        private string _FileType;
		public string FileType
		{
			get
			{
				return !String.IsNullOrEmpty(_FileType) ? _FileType : "";
			}
			set
			{
				_FileType = value;
			}
		}

		private string _Section;
		public string Section
		{
			get
			{
				return !String.IsNullOrEmpty(_Section) ? _Section : "";
			}
			set
			{
				_Section = value;
			}
		}
		
		private string _CatName;
		public string CatName
		{
			get
			{
				return !String.IsNullOrEmpty(_CatName) ? _CatName : "";
			}
			set
			{
				_CatName = value;
			}
		}
        private string _TempPath;
        public string TempPath
        {
            get
            {
                return !String.IsNullOrEmpty(_TempPath) ? _TempPath : "";
            }
            set
            {
                _TempPath = value;
            }
        }

    }
}
