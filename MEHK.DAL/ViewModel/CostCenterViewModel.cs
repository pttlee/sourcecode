﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEHK.DAL.Model;

namespace MEHK.DAL.ViewModel
{
	public class CostCenterViewModel
	{
		public CostCenterViewModel()
		{

		}
		public CostCenterViewModel(CostCenter model)
		{
			Code = model.Code;
			Description = String.Format("{0} - {1}", model.Code, model.Description);
			PIC = model.PIC;
			CurrencyCode = model.CurrencyCode;
			IsDeleted = model.IsDeleted;
		}


		public string Code { get; set; }
		public string Description { get; set; }
		public string PIC { get; set; }
		public string CurrencyCode { get; set; }
		public bool IsDeleted { get; set; }

		public bool IsClone { get; set; }
	}
}
