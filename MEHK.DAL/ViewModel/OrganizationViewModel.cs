﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.ViewModel
{
	public class OrganizationViewModel
    {
        public UpdateUnitModel UpdateUnitModel { get; set; }
        public int CurrentUserID { get; set; }

    }
}
