﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MEHK.DAL.ViewModel
{
    public class MemberUnit
    {

        //[Required]
        //[DisplayName("Member")]
        //[UIHint("GridForeignKey")]
            //public int Id { get; set; }

            public int MemberId { get; set; }
           
            //[DisplayName("Member Name")]
            public string MemberName { get; set; }
            //[ScaffoldColumn(false)]
            public DateTime? CreatedDate { get; set; }

            //[ScaffoldColumn(false)]
            public int UnitId { get; set; }

            public string CreatedDateString
            {
                get
                {
                    return CreatedDate.HasValue ? CreatedDate.Value.ToString("yyyy/MM/dd") : "";
                }
            }
        
    }
}