﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.ViewModel
{
	public class RightViewModel
	{

		public int ID { get; set; }
		[Required(ErrorMessage = "*")]
		public string RightName { get; set; }
		public string Description { get; set; }
		[Required(ErrorMessage = "*")]
		public int AppID { get; set; }
		public bool IsDeleted { get; set; }
		public Nullable<System.DateTime> CreatedDate { get; set; }
		public string CreatedBy { get; set; }
		public Nullable<System.DateTime> ModifiedDate { get; set; }
		public string ModifiedBy { get; set; }

	}
}
