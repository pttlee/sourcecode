﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.ViewModel
{
    public partial class SiteMenuModel
    {
        public int ID { get; set; }
        public string PageDisplayName { get; set; }
		public string PageNameDisplayName_CN { get; set; }
		public string PageName { get; set; }
        public Nullable<int> ParentID { get; set; }
        public string URL { get; set; }
        public Nullable<int> DisplayOrder { get; set; }
        public Nullable<int> ProcessID { get; set; }
    }
}
