﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.ViewModel
{
	public class MatchCostCenterViewModel
	{
		public string sDisplayName { get; set; }
		public bool bExist { get; set; }

		public int nTargetID { get; set; }
		public string sCode { get; set; }

	}
}
