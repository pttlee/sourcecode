﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEHK.DAL.Model;

namespace MEHK.DAL.ViewModel
{
	public class FileList
	{
		public FileList()
		{

		}


		public FileList(FileAttachment model)
		{
			ID = model.ID;
			Name = model.FileName;
			FileOwnerID = model.FileOwner.HasValue ? model.FileOwner.Value : -1;
			FileSize = model.FileSize.HasValue ? (int)model.FileSize.Value : 0;
			UploadedDate = model.UploadedDate;
		}

		public Guid ID { get; set; }
		//public string Name { get; set; }
		public int FileOwnerID { get; set; }
		public string Name { get; set; }
		public int FileSize { get; set; }
		public DateTime UploadedDate { get; set; }
	}
}
