﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.ViewModel
{
	public class OrganizationDetailViewModel
	{
		public int ID { get; set; }

		public int TypeID { get; set; }
		public string Type { get; set; }
		public string Label { get; set; }
		public string Value { get; set; }


		public string Key { get; set; }
	}
}
