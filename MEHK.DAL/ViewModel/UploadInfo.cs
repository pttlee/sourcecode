﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MEHK.DAL.ViewModel
{
	public class UploadInfo
	{
		public string refid { get; set; }
		public string process { get; set; }
		public string remark { get; set; }
		public int userID { get; set; }
		public string attachmentType { get; set; }

	}
}
