﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.ViewModel
{
	public class OrganizationCostCenterViewModel
	{
		[Required]
		[DisplayName("Cost Center")]
		[UIHint("GridForeignKey")]
		public string CostCenterCode { get; set; }

		[ScaffoldColumn(false)]
		public DateTime? CreatedDate { get; set; }

		[ScaffoldColumn(false)]
		public int OrganizationID { get; set; }

		public string CreatedDateString
		{
			get
			{
				return CreatedDate.HasValue ? CreatedDate.Value.ToString("yyyy/MM/dd") : "";
			}
		}
	}
}
