﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.ViewModel
{
	public class MatchUserViewModel
	{
		public string DisplayName { get; set; }
		public bool Exist { get; set; }

		public int TargetID { get; set; }
		public int UserID { get; set; }

		public string TargetCode { get; set; }
	}
}
