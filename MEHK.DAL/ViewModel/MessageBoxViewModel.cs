﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEHK.DAL.ViewModel
{
	public class MessageBoxViewModel
	{
		public bool Success { get; set; }

		public string Title { get; set; }

		public string Text { get; set; }

		public int ID { get; set; }
	}
}
